package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Units;

public interface UnitDAO {
	List<Units> getAllUsageUnits();
	List<Units> getAllSKUUnits();
	List<Units> findAll();
	Units find(String unitCode);
	int insert(Units unit);
	int delete(String unitCode);
	int update(Units unit);
}
