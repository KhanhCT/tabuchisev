package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.LineDAO;
import com.daisyit.backend.model.ProdLine;
@Repository("lineDAO")
@Transactional
public class LineDAOImpl implements LineDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(ProdLine line) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT INTO ProdLine(FactoryID, LineTagCode, LineDesc, WPlaceQty, Disabled) VALUES"
				+ "(:FactoryID, :LineTagCode, :LineDesc, :WPlaceQty, :Disabled)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("FactoryID", line.getId().getFactoryId());
		query.setParameter("LineTagCode", line.getLineTagCode());
		query.setParameter("LineDesc", line.getLineDesc());
		query.setParameter("WPlaceQty", line.getWplaceQty());
		query.setParameter("Disabled", false);
		return query.executeUpdate();
	}

	@Override
	public int update(ProdLine line) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(line);
		return 1;
	}

	@Override
	public ProdLine find(int lineId, int factoryId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM ProdLine WHERE id.lineId= :lineId AND id.factoryId = :factoryId AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("lineId", lineId);
		query.setParameter("factoryId", factoryId);
		query.setParameter("disabled", false);
		return (ProdLine) query.uniqueResult();
	}
	@Override
	public ProdLine find(String lineCode, int factoryId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM ProdLine WHERE lineTagCode= :lineCode AND id.factoryId = :factoryId AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("lineCode", lineCode);
		query.setParameter("factoryId", factoryId);
		query.setParameter("disabled", false);
		return (ProdLine) query.uniqueResult();
	}

	@Override
	public int delete(int factoryId, Integer lineId) {
		Session session = this.sessionFactory.getCurrentSession();
		if(lineId == null) {
			String sqlQuery = "DELETE ProdLine WHERE id.factoryId = :factoryId";
			Query query = session.createQuery(sqlQuery);
			query.setInteger("factoryId", factoryId);
			return query.executeUpdate();
		}else {
			String sqlQuery = "DELETE ProdLine WHERE LineID= :lineId AND FactoryID= :factoryId";
			Query query = session.createSQLQuery(sqlQuery);
			query.setInteger("lineId", lineId);
			query.setInteger("factoryId", factoryId);
			return query.executeUpdate();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProdLine> findAll(int factoryId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM ProdLine WHERE id.factoryId = :factoryId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("factoryId", factoryId);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProdLine> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM ProdLine WHERE  Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setBoolean("disabled", false);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAll(int factoryId, int lineId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_FindAllProdLine :factoryId, :lineId";
		Query query = session.createSQLQuery(sqlQuery);
		query.setInteger("lineId", lineId);
		query.setInteger("factoryId", factoryId);
		return query.list();
	}

	@Override
	public int saveOrUpdate(ProdLine line) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(line);
		return 1;
	}

	@Override
	public int insert(String json) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertProdLine :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("json", json);
		return query.executeUpdate();
	}

	

}
