package com.daisyit.backend.model;
// Generated Jan 10, 2019 10:12:43 AM by Hibernate Tools 3.6.0


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * WorkPlace generated by hbm2java
 */
@Entity
@Table(name="WorkPlace"
    ,schema="dbo"
    ,catalog="Tabuchi"
)
public class WorkPlace  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WorkPlaceId id;
     private String description;
     private int processId;
     private String ipv4Tab;
     private String ipv4Cam;
     private Boolean disabled;

    public WorkPlace() {
    }

	
    public WorkPlace(WorkPlaceId id, int processId, String ipv4Cam) {
        this.id = id;
        this.processId = processId;
        this.ipv4Cam = ipv4Cam;
    }
    public WorkPlace(WorkPlaceId id, String description, int processId, String ipv4Tab, String ipv4Cam, Boolean disabled) {
       this.id = id;
       this.description = description;
       this.processId = processId;
       this.ipv4Tab = ipv4Tab;
       this.ipv4Cam = ipv4Cam;
       this.disabled = disabled;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="factoryId", column=@Column(name="FactoryID", nullable=false) ), 
        @AttributeOverride(name="lineId", column=@Column(name="LineID", nullable=false) ), 
        @AttributeOverride(name="wplaceId", column=@Column(name="WPlaceID", nullable=false) ) } )
    public WorkPlaceId getId() {
        return this.id;
    }
    
    public void setId(WorkPlaceId id) {
        this.id = id;
    }

    
    @Column(name="Description", length=50)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="ProcessID", nullable=false)
    public int getProcessId() {
        return this.processId;
    }
    
    public void setProcessId(int processId) {
        this.processId = processId;
    }

    
    @Column(name="IPV4_Tab", length=50)
    public String getIpv4Tab() {
        return this.ipv4Tab;
    }
    
    public void setIpv4Tab(String ipv4Tab) {
        this.ipv4Tab = ipv4Tab;
    }

    
    @Column(name="IPV4_Cam", nullable=false, length=50)
    public String getIpv4Cam() {
        return this.ipv4Cam;
    }
    
    public void setIpv4Cam(String ipv4Cam) {
        this.ipv4Cam = ipv4Cam;
    }

    
    @Column(name="Disabled")
    public Boolean getDisabled() {
        return this.disabled;
    }
    
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }




}


