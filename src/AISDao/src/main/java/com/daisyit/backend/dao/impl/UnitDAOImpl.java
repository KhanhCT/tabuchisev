package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.UnitDAO;
import com.daisyit.backend.model.Units;
@Repository("unitDAO")
@Transactional
public class UnitDAOImpl implements UnitDAO {
	@Autowired
	protected SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	@Override
	public List<Units> getAllUsageUnits() {
		String sqlQuery ="FROM Units WHERE UnitUsage= :usageUnit AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setBoolean("usageUnit", true);
		query.setBoolean("disabled", false);
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Units> getAllSKUUnits() {
		String sqlQuery ="FROM Units WHERE UnitSKU= :unitSKU AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setBoolean("unitSKU", true);
		query.setBoolean("disabled", false);
		
		return query.list();
	}
	

	@Override
	public Units find(String unitCode) {
		String sqlQuery ="FROM Units WHERE id.unitCode= :unitCode AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setString("unitCode", unitCode);
		query.setBoolean("disabled", false);
		return (Units) query.uniqueResult();
	}

	@Override
	public int insert(Units unit) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT INTO Units(UnitCode, UnitName, UnitUsage, UnitSKU, Disabled) VALUES(:unitCode, :unitName, :unitUsage, :unitSku, :disabled)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("unitCode", unit.getId().getUnitCode());
		query.setString("unitName", unit.getUnitName());
		query.setBoolean("unitUsage", unit.getUnitUsage());
		query.setBoolean("unitSku", unit.getUnitSku());
		query.setBoolean("disabled", unit.getDisabled());
		return query.executeUpdate();
	}

	@Override
	public int delete(String unitCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE Units WHERE UnitCode= :unitCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("unitCode", unitCode);
		return query.executeUpdate();
	}

	@Override
	public int update(Units unit) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Units SET UnitName= :unitName WHERE UnitCode= :unitCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("unitCode", unit.getId().getUnitCode());
		query.setString("unitName", unit.getUnitName());
		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Units> findAll() {
		String sqlQuery ="FROM Units WHERE Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setBoolean("disabled", false);	
		return query.list();
	}

}
