package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.UserRoles;

public interface UserRoleDAO {
	String getUserRoleName(int userRoleId);
	List<UserRoles> getAllUserRoles();
}
