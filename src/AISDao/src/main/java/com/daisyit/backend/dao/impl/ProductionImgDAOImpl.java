package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProductionImgDAO;

@Repository("productionImgDAO")
@Transactional
public class ProductionImgDAOImpl implements ProductionImgDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> find(Date workingDate, int factoryId, int lineId, int shiftId, int position) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Object[]> list = new ArrayList<>();
		//String sqlQuery = "SELECT CaptureDate, CaptureTime, ImgLink FROM ProductionImg_20180816 WHERE WorkingDate = :workingDate AND FactoryID=:factoryId AND LineID=:lineId AND ShiftID=:shiftId";
		String sqlQuery = "exec sp_GetImages  :lineId, :shiftId, :factoryId, :workingDate, :position";
		Query query = session.createSQLQuery(sqlQuery);
		query.setDate("workingDate", workingDate);
		query.setInteger("factoryId", factoryId);
		query.setInteger("lineId", lineId);
		query.setInteger("shiftId", shiftId);
		query.setInteger("position", position);
		list = query.list();
		return list;
	}

}
