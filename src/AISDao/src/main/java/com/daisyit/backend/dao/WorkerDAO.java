package com.daisyit.backend.dao;


import java.util.List;

import com.daisyit.backend.model.Worker;

public interface WorkerDAO {
	public int insert(Worker worker);
	public int insert(String json);
	public int update(Worker worker);
	public int delete(int workerId);
	public int delete(String workerCode);
	public Worker find(String workerCode);
	List<Worker> findAll();

}
