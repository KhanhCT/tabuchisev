package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.UserRoleDAO;
import com.daisyit.backend.model.UserRoles;

@Repository("userRoleDAO")
@Transactional
public class UserRoleDAOImpl implements UserRoleDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public String getUserRoleName(int userRoleId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("SELECT c.roleDesc FROM UserRoles c WHERE c.id.roleId = :roleId");
		query.setInteger("roleId", userRoleId);
		String roleName = (String) query.uniqueResult();
		return roleName;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<UserRoles> getAllUserRoles() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM UserRoles");
		return query.list();
	}


}
