package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.LineProdProcess;

public interface LineProcessDAO {
	public int insert(LineProdProcess process);
	public int update(LineProdProcess process);
	public LineProdProcess find(int lineProdId, int processId);
	public int delete(int lineProdId, Integer processId);
	public int saveOrUpdate(LineProdProcess process);
	public List<Object[]> findAll(int lineProdId, int processId);
}
