package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.WorkPlace;

public interface WorkPlaceDAO {
	public int insert(WorkPlace wPlace);
	public int update(WorkPlace wPlace);
	public int insert(String wPlaceJson);
	public int delete(int factoryId, Integer lineId, int wPlaceId);
	public int saveOrUpdate(WorkPlace wPlace);
	public List<Object[]> find(int factoryId, Integer lineID,  Integer wPlaceId);
}
