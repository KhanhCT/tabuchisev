package com.daisyit.backend.dao.impl;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.backend.model.Sysvar;

@Transactional
@Repository(value="sysvarDAO")
public class SysvarDAOImpl implements SysvarDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public String getImageDir(String varname) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("SELECT VarValue FROM Sysvar WHERE VarName = :varname");
		query.setString("varname", varname);
		return (String) query.uniqueResult();
	}
	@Override
	public String getValueFromName(String varname) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("SELECT VarValue FROM Sysvar WHERE VarName = :varname");
		query.setString("varname", varname);
		return (String) query.uniqueResult();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Sysvar> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Sysvar");
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Sysvar> findAll(String type) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Sysvar WHERE valueType=:type");
		query.setParameter("type", type);
		return query.list();
	}

}
