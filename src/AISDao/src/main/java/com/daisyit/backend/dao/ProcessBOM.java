package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.ProcessProdBom;

public interface ProcessBOM {
	public int insert(ProcessProdBom lineProdBom);
	public int update(ProcessProdBom lineProdBom);
	public List<Object[]> find(int lineProdId, int processId);
	public List<Object[]> findAll();
	public List<ProcessProdBom> findAllProcessBOM();
	public int delete(Integer lineProdId, Integer materialId, Integer processId);
	public List<Object[]> exportBOM();
}
