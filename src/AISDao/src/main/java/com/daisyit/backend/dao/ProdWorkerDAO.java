package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.ProductionWorker;

public interface ProdWorkerDAO {
	public int insert(ProductionWorker worker);
	public int update(ProductionWorker worker);
	public int insert(String wPlaceJson);
	public int delete(int factoryID, int lineID, int workerID);
	public int saveOrUpdate(ProductionWorker worker);
	public List<Object[]> find(int factoryID, Integer lineID,  Integer workerID);
}
