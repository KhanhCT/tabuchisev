package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ShiftDAO;
import com.daisyit.backend.model.Shift;
@Repository("shiftDAO")
@Transactional
public class ShiftDAOImpl implements ShiftDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	@Override
	public List<Shift> findAll(int factoryID) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Shift WHERE id.factoryId=:factoryId AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("factoryId", factoryID);
		query.setParameter("disabled", false);
		return query.list();
	}
	@Override
	public Shift find(int factoryID, String shiftCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Shift WHERE id.factoryId=:factoryId AND shiftCode=:shiftCode AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("factoryId", factoryID);
		query.setParameter("shiftCode", shiftCode);
		query.setParameter("disabled", false);
		return (Shift) query.uniqueResult();
	}
}
