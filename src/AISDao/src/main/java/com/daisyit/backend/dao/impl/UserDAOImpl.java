package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.UserDAO;
import com.daisyit.backend.model.Account;

@Repository("userDAO")
@Transactional
public class UserDAOImpl  implements UserDAO{
	
	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public Account getUserByName(String userName) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Account c WHERE c.id.userName = :userName ");
		query.setParameter("userName", userName);
		Account user = (Account) query.uniqueResult();
		return user;
	}
	
	@Override
	public Account getUser(String username, String password) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Account c WHERE c.id.userName = :userName AND c.password = :password  AND Disable= :disabled");
		query.setParameter("userName", username);
		query.setParameter("password", password);
		query.setBoolean("disabled", false);
		Account user = (Account) query.uniqueResult();
		return user;
	}
	
	@Override
	public boolean deleteUserById(int userId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("DELETE Account c WHERE c.id.userId= :userId");
		query.setInteger("userId", userId);
		if(query.executeUpdate() > 0)
			return true;
		else
			return false;
	}


	@Override
	public int addUser(Account user) {
		Session session = sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT INTO Account(UserName, Password, UserRoleId, Disable) VALUES(:userName, :password, :userRoleId, :disable)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("userName",user.getId().getUserName());
		query.setString("password",user.getPassword());
		query.setInteger("userRoleId",user.getUserRoleId());
		query.setBoolean("disable", false);
		return query.executeUpdate();
	}
	
	
	@Override
	public Account getUserById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Account c WHERE c.id.userId = :userId");
		query.setParameter("userId", id);
		@SuppressWarnings("unchecked")
		List<Account> list  = query.list();
		if(list != null && ! list.isEmpty()){
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Account> getAllUser() {
		List<Account> lst = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Account WHERE Disable= :disabled");
		query.setBoolean("disabled", false);
		lst =  query.list();
		return lst;
	}
}
