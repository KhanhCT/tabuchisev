package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.LineProcessDAO;
import com.daisyit.backend.model.LineProdProcess;
@Repository("lineProcessDAO")
@Transactional
public class LineProcessDAOImpl implements LineProcessDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(LineProdProcess process) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(process);
		return 1;
	}

	@Override
	public int update(LineProdProcess process) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(process);
		return 0;
	}

	@Override
	public LineProdProcess find(int lineProdId, int processId) {
		String sqlQuery ="FROM LineProdProcess WHERE id.lineProdId= :lineProdId AND id.processId= :processId AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setParameter("lineProdId", lineProdId);
		query.setParameter("processId", processId);
		query.setParameter("disabled", false);
		return (LineProdProcess) query.uniqueResult();
	}

	@Override
	public int delete(int lineProdId, Integer processId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery;
		Query query;
		if(processId == null) {
			sqlQuery = "DELETE LineProdProcess WHERE LineProdID= :lineProdId";
			query = session.createSQLQuery(sqlQuery);
			query.setParameter("lineProdId", lineProdId);
			return query.executeUpdate();
		}else {
			sqlQuery = "DELETE LineProdProcess WHERE LineProdID= :lineProdId AND ProcessID= :processId";
			query = session.createSQLQuery(sqlQuery);
			query.setParameter("lineProdId", lineProdId);
			query.setParameter("processId", processId);
			return query.executeUpdate();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAll(int lineProdId, int processId) {		
		String sqlQuery ="exec sp_FindAllProcess :lineProdId , :processId";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("lineProdId", lineProdId);
		query.setParameter("processId", processId);
		return query.list();
	}

	@Override
	public int saveOrUpdate(LineProdProcess process) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(process);
		return 1;
	}


}
