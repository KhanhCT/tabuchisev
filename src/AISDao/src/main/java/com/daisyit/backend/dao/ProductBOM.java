package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.LineProdBom;

public interface ProductBOM {
	public int insert(LineProdBom lineProdBom);
	public int update(LineProdBom lineProdBom);
	public LineProdBom find(int lineProdId, int materialId);
	public List<Object[]> findAll();
	public int delete(Integer lineProdId, Integer materialId);
}
