package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Sysvar;

public interface SysvarDAO {
	public String getImageDir(String varname);
	public String getValueFromName(String varname);
	public List<Sysvar> findAll();
	public List<Sysvar> findAll(String type);
}
