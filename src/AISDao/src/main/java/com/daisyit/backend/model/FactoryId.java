package com.daisyit.backend.model;
// Generated Jan 10, 2019 10:12:43 AM by Hibernate Tools 3.6.0


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * FactoryId generated by hbm2java
 */
@Embeddable
public class FactoryId  implements java.io.Serializable {


     private int factoryId;
     private String factoryCode;

    public FactoryId() {
    }

    public FactoryId(int factoryId, String factoryCode) {
       this.factoryId = factoryId;
       this.factoryCode = factoryCode;
    }
   


    @Column(name="FactoryID", nullable=false)
    public int getFactoryId() {
        return this.factoryId;
    }
    
    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }


    @Column(name="FactoryCode", nullable=false, length=2)
    public String getFactoryCode() {
        return this.factoryCode;
    }
    
    public void setFactoryCode(String factoryCode) {
        this.factoryCode = factoryCode;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof FactoryId) ) return false;
		 FactoryId castOther = ( FactoryId ) other; 
         
		 return (this.getFactoryId()==castOther.getFactoryId())
 && ( (this.getFactoryCode()==castOther.getFactoryCode()) || ( this.getFactoryCode()!=null && castOther.getFactoryCode()!=null && this.getFactoryCode().equals(castOther.getFactoryCode()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getFactoryId();
         result = 37 * result + ( getFactoryCode() == null ? 0 : this.getFactoryCode().hashCode() );
         return result;
   }   


}


