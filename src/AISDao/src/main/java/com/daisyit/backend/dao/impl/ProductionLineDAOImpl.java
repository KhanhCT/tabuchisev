package com.daisyit.backend.dao.impl;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProductionLineDAO;
import com.daisyit.backend.model.ProductionLine;
@Repository("productionLineDAO")
@Transactional
public class ProductionLineDAOImpl implements ProductionLineDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(String json) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertProductionPlan :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("json", json);
		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> find(String workingDate) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_FindAllProdPlan :workingDate";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("workingDate", workingDate);
		return query.list();
	}

	@Override
	public int save(ProductionLine plan) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.saveOrUpdate(plan);
			return 1;
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public int delete(int factoryId, int lineId, String workingDate, int shiftI) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE ProductionLine  WHERE FactoryID=:FactoryID AND LineID=:LineID AND ShiftID=:ShiftID AND WorkingDate=:WorkingDate";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("FactoryID", factoryId);
		query.setParameter("LineID", lineId);
		query.setParameter("ShiftID", shiftI);
		query.setParameter("WorkingDate", workingDate);
		return query.executeUpdate();
	}

	@Override
	public int update(ProductionLine plan) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE ProductionLine SET OrderedQty=:OrderedQty, LineProdID=:LineProdID , Disabled=:Disabled WHERE FactoryID=:FactoryID AND LineID=:LineID AND ShiftID=:ShiftID AND WorkingDate=:WorkingDate";
		Query query = session.createSQLQuery(sqlQuery);
		String workingDate = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			workingDate= sdf.format(plan.getId().getWorkingDate()).toString();
		}catch(Exception ex){
			return 0;
		}
		query.setParameter("FactoryID", plan.getId().getFactoryId());
		query.setParameter("LineID", plan.getId().getLineId());
		query.setParameter("ShiftID", plan.getId().getShiftId());
		query.setParameter("WorkingDate", workingDate);

		query.setParameter("OrderedQty", plan.getOrderedQty());
		query.setParameter("LineProdID", plan.getLineProdId());
		query.setParameter("Disabled", plan.getDisabled());

		return query.executeUpdate();
	}

}
