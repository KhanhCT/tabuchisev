package com.daisyit.backend.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProdLineDetailDAO;

@Repository("prodLineDetailDAO")
@Transactional
public class ProdLineDetailDAOImpl implements  ProdLineDetailDAO{
	@Autowired
	protected SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getMonthlyLineData(int factoryId, int lineId, Date fromDate, Date toDate, int type) {
		Session session = this.sessionFactory.getCurrentSession();
		String queryString = "exec sp_GetMonthlyLineData :factoryId, :lineId, :type, :fromDate, :toDate";
		Query query = session.createSQLQuery(queryString);
		query.setInteger("factoryId", factoryId);
		query.setInteger("lineId", lineId);
		query.setInteger("type", type);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]>  getFactoryData(int factoryId, Date curDate) {
		Session session = this.sessionFactory.getCurrentSession();
		String queryString = "exec sp_GetFactoryData :factoryId, :workingDate";
		Query query = session.createSQLQuery(queryString);
		query.setParameter("factoryId", factoryId);
		query.setParameter("workingDate", curDate);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getMonthlyWorkerData(int workerId, Date workingDate) {
		Session session = this.sessionFactory.getCurrentSession();
		String queryString = "exec sp_GetMonthlyWorkerData :workerId, :workingDate";
		Query query = session.createSQLQuery(queryString);
		query.setInteger("workerId", workerId);
		query.setParameter("workingDate", workingDate);
		return query.list();
	}
	

}
