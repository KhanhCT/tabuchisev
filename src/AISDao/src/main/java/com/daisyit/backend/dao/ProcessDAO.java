package com.daisyit.backend.dao;

import java.util.List;


public interface ProcessDAO {
	public int insert(com.daisyit.backend.model.Process process);
	public int update(com.daisyit.backend.model.Process process);
	public List<com.daisyit.backend.model.Process> find(String processCode);
	public List<com.daisyit.backend.model.Process> findAll();
	public int delete(String processCode);
}
