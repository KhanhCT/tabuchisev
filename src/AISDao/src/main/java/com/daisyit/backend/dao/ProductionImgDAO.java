package com.daisyit.backend.dao;

import java.util.Date;
import java.util.List;

public interface ProductionImgDAO {
	List<Object[]> find(Date workingDate, int factoryId, int lineId, int shiftId, int position);
}
