package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.WorkPlaceDAO;
import com.daisyit.backend.model.WorkPlace;
@Repository("workPlaceDAO")
@Transactional
public class WorkPlaceDAOImpl implements WorkPlaceDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(String wPlaceJson) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertWorkPlaces :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("json", wPlaceJson);
		return query.executeUpdate();
	}

	@Override
	public int insert(WorkPlace wPlace) {
		Session session = this.sessionFactory.getCurrentSession();
		try{
			session.saveOrUpdate(wPlace);
			return 1;
		}catch(Exception e) {
			return 0;
		}
	}

	@Override
	public int update(WorkPlace wPlace) {
		Session session = this.sessionFactory.getCurrentSession();
		try{
			session.update(wPlace);
			return 1;
		}catch(Exception e) {
			return 0;
		}
	}

//	@Override
//	public WorkPlace find(int lineID, int factoryID, int wPlaceID) {
//		Session session = this.sessionFactory.getCurrentSession();
//		String sqlQuery = "FROM WorkPlace WHERE FactoryID= :factoryID AND LineID=:lineID AND WPlaceID=:wPlaceID";
//		Query query = session.createSQLQuery(sqlQuery);
//		query.setParameter("factoryID", factoryID);
//		query.setParameter("lineID", lineID);
//		query.setParameter("wPlaceID", wPlaceID);
//		WorkPlace wPlace = (WorkPlace) query.uniqueResult();
//		return wPlace;
//	}

	@Override
	public int delete(int factoryID, Integer lineID, int wPlaceID) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE WorkPlace  WHERE FactoryID= :factoryID AND LineID=:lineID AND WPlaceID=:wPlaceID";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("factoryID", factoryID);
		query.setParameter("lineID", lineID);
		query.setParameter("wPlaceID", wPlaceID);

		return query.executeUpdate();
	}

	@Override
	public int saveOrUpdate(WorkPlace wPlace) {
		Session session = this.sessionFactory.getCurrentSession();
		try{
			session.saveOrUpdate(wPlace);
			return 1;
		}catch(Exception e) {
			return 0;
		}	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> find(int factoryID, Integer lineID,  Integer wPlaceID) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_FindAllWorkPlace :factoryID, :lineID, :wPlaceID";
		
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("factoryID", factoryID);
		query.setParameter("lineID", lineID);
		query.setParameter("wPlaceID", wPlaceID);
		return query.list();
	}

}
