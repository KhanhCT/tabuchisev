package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.MaterialDAO;
import com.daisyit.backend.model.Material;
@Repository("materialDAO")
@Transactional
public class MaterialDAOImpl implements MaterialDAO {
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(Material material) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery;
		if(material.getIsLineProd())
			sqlQuery = "INSERT INTO Material(MaterialCode, Barcode, MaterialName, UsageUnitID, SKUUnitID, SKUUsageQty,IsLineProd, LineProdID, Disabled) VALUES"
				+ "(:MaterialCode, :Barcode, :MaterialName, :UsageUnitID, :SKUUnitID, :SKUUsageQty, :IsLineProd, :LineProdID, :Disabled)";
		else
			sqlQuery = "INSERT INTO Material(MaterialCode, Barcode, MaterialName, UsageUnitID, SKUUnitID, SKUUsageQty,IsLineProd, Disabled) VALUES"
					+ "(:MaterialCode, :Barcode, :MaterialName, :UsageUnitID, :SKUUnitID, :SKUUsageQty, :IsLineProd, :Disabled)";

		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("MaterialCode", material.getId().getMaterialCode());
		query.setParameter("Barcode", material.getId().getBarcode());
		query.setParameter("MaterialName", material.getMaterialName());
		query.setParameter("UsageUnitID", material.getUsageUnitId());
		query.setParameter("SKUUnitID", material.getSkuunitId());
		query.setParameter("SKUUsageQty",material.getSkuusageQty());
		query.setParameter("IsLineProd", material.getIsLineProd());
		if(material.getIsLineProd())
		{
			if(material.getLineProdId() !=null)
				query.setParameter("LineProdID", material.getLineProdId());
		}
		query.setBoolean("Disabled", material.getDisabled());
		return query.executeUpdate();
	}
	@Override
	public int update(Material material) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Material SET  MaterialName= :MaterialName, UsageUnitID= :UsageUnitID,SKUUnitID= :SKUUnitID,SKUUsageQty= :SKUUsageQty,IsLineProd= :IsLineProd,LineProdID= :LineProdID, Disabled=:Disabled"
				+ " WHERE MaterialCode= :MaterialCode AND Barcode= :Barcode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("MaterialCode", material.getId().getMaterialCode());
		query.setParameter("Barcode", material.getId().getBarcode());
		query.setParameter("MaterialName", material.getMaterialName());
		query.setParameter("UsageUnitID", material.getUsageUnitId());
		query.setParameter("SKUUnitID", material.getSkuunitId());
		query.setParameter("SKUUsageQty", material.getSkuusageQty());
		
		query.setParameter("IsLineProd", material.getIsLineProd());
		query.setParameter("LineProdID", material.getLineProdId());
		query.setBoolean("Disabled", material.getDisabled());
		return query.executeUpdate();
	}
	@Override
	public Material find(String materialCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Material WHERE id.materialCode= :materialCode AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setString("materialCode", materialCode);
		query.setBoolean("disabled", false);
		return (Material) query.uniqueResult();
	}
	@Override
	public int delete(String materialCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE Material WHERE MaterialCode= :materialCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("materialCode", materialCode);
		return query.executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_GetAllMaterial";
		Query query = session.createSQLQuery(sqlQuery);
		return query.list();
	}
	@Override
	public int insert(String json) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertMaterial :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("json", json);
		return query.executeUpdate();
	}

}
