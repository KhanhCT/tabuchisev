package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Material;

public interface MaterialDAO {
	
	public int insert(Material material);
	public int update(Material material);
	public Material find(String materialCode);
	public int delete(String materialCode);
	public int insert(String json);
	public List<Object[]> findAll();
}
