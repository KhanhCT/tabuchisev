package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProductBOM;
import com.daisyit.backend.model.LineProdBom;

@Repository("productBOM")
@Transactional
public class ProductBOMImpl implements ProductBOM {
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(LineProdBom lineProdBom) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(lineProdBom);
		}catch (Exception e) {		
			return 0;
		}
		return 1;
	}

	@Override
	public int update(LineProdBom lineProdBom) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.update(lineProdBom);
		} catch (Exception ex) {
			return 0;
		}
		return 1;
	}

	@Override
	public LineProdBom find(int lineProdId, int materialId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM LineProdBom WHERE id.lineProdId= :lineProdId AND id.materialId= :materialId";
		Query query = session.createQuery(sqlQuery);
		query.setInteger("materialId", materialId);
		query.setInteger("lineProdId", lineProdId);
		return (LineProdBom) query.uniqueResult();
	}

	@Override
	public int delete(Integer lineProdId, Integer materialId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ;
		Query query;
		if(lineProdId == null) {
			sqlQuery = "DELETE LineProdBom WHERE Material_ID= :MaterialId";
			query = session.createSQLQuery(sqlQuery);
			query.setInteger("MaterialId", materialId);
			return query.executeUpdate();
		}else if (materialId ==null ) {
			sqlQuery = "DELETE LineProdBom WHERE LineProd_ID= :LineProdId";
			query = session.createSQLQuery(sqlQuery);
			query.setInteger("LineProdId", lineProdId);
			return query.executeUpdate();
		}else {
			sqlQuery = "DELETE LineProdBom WHERE LineProd_ID= :LineProdId AND Material_ID= :MaterialId";
			query = session.createSQLQuery(sqlQuery);
			query.setInteger("MaterialId", materialId);
			query.setInteger("LineProdId", lineProdId);
			return query.executeUpdate();
		}		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_GetAllProdBOM";
		Query query = session.createSQLQuery(sqlQuery);
		return query.list();
	}

}
