package com.daisyit.backend.dao;

import com.daisyit.backend.model.Shift;

import java.util.List;

public interface ShiftDAO {
	List<Shift> findAll(int factoryID);
	Shift find(int factoryID, String shiftCode) ;
}
