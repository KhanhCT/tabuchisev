package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProdWorkerDAO;
import com.daisyit.backend.model.ProductionWorker;

@Repository("prodWorkerDAO")
@Transactional
public class ProdWorkerDAOImpl implements ProdWorkerDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(ProductionWorker worker) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(ProductionWorker worker) {
		
		return 0;
	}

	@Override
	public int insert(String workerJson) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertWorkPlaces :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("json", workerJson);
		return query.executeUpdate();
	}

	@Override
	public int delete(int factoryID, int lineID, int workerID) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE ProcductionWorker SET Disabled=:disabled WHERE FactoryID= :factoryID AND LineID=:lineID AND WorkerID=:workerID";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("factoryID", factoryID);
		query.setParameter("lineID", lineID);
		query.setParameter("workerID", workerID);
		query.setParameter("disabled", true);
		return query.executeUpdate();
	}

	@Override
	public int saveOrUpdate(ProductionWorker worker) {
		Session session = this.sessionFactory.getCurrentSession();
		try{
			session.saveOrUpdate(worker);
			return 1;
		}catch(Exception e) {
			return 0;
		}	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> find(int factoryID, Integer shiftID, Integer workerID) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery;
		if(shiftID ==  null)
			sqlQuery = "SELECT t1.*, t2.FactoryCode, t2.FactoryName, t3.LineTagCode, t4.ShiftCode, t4.ShiftName, t5.WorkerCode, t5.workerName FROM ProductionWorker as t1 JOIN Factory as t2 ON t1.FactoryID=t2.FactoryID "
					+ "JOIN ProdLine as t3 ON t1.FactoryID=t3.FactoryID AND t1.LineID=t3.LineID "
					+ "JOIN Shift as t4 ON t1.ShiftID=t4.ShiftID "
					+ "JOIN Worker as t5 ON t1.WorkerID=t5.WorkerID "
					+ "WHERE t1.FactoryID= :factoryID";
		else {
			if(workerID == null) {
				sqlQuery = "SELECT t1.*, t2.FactoryCode, t2.FactoryName, t3.LineTagCode, t4.ShiftCode, t4.ShiftName, t5.WorkerCode, t5.workerName FROM ProductionWorker as t1 JOIN Factory as t2 ON t1.FactoryID=t2.FactoryID "
						+ "JOIN ProdLine as t3 ON t1.FactoryID=t3.FactoryID AND t1.LineID=t3.LineID "
						+ "JOIN Shift as t4 ON t1.ShiftID=t4.ShiftID "
						+ "JOIN Worker as t5 ON t1.WorkerID=t5.WorkerID "
						+ "WHERE t1.FactoryID= :factoryID AND t1.LineID=:lineID";
			}else {
				sqlQuery = "SELECT t1.*, t2.FactoryCode, t2.FactoryName, t3.LineTagCode, t4.ShiftCode, t4.ShiftName, t5.WorkerCode, t5.workerName FROM ProductionWorker as t1 JOIN Factory as t2 ON t1.FactoryID=t2.FactoryID "
						+ "JOIN ProdLine as t3 ON t1.FactoryID=t3.FactoryID AND t1.LineID=t3.LineID "
						+ "JOIN Shift as t4 ON t1.ShiftID=t4.ShiftID "
						+ "JOIN Worker as t5 ON t1.WorkerID=t5.WorkerID "
						+ "WHERE t1.FactoryID= :factoryID AND t1.LineID=:lineID AND t1.ShiftID=:shiftID";
			}
		}
		Query query = session.createSQLQuery(sqlQuery);
		if(shiftID ==  null) {
			query.setParameter("factoryID", factoryID);
		}else {
			if(workerID == null) {
				query.setParameter("factoryID", factoryID);
				query.setParameter("shiftID", shiftID);
			}else {
				query.setParameter("factoryID", factoryID);
				query.setParameter("shiftID", shiftID);
				query.setParameter("workerID", workerID);
			}
		}
		return query.list();
	}

}
