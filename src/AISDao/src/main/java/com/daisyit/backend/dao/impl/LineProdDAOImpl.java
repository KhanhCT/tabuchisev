package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.LineProdDAO;
import com.daisyit.backend.model.LineProds;
@Repository("lineProdDAO")
@Transactional
public class LineProdDAOImpl implements LineProdDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public LineProds getLineProductByCode(String productCode) {
		String sqlQuery ="FROM LineProds WHERE id.lineProdCode= :productCode AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setParameter("productCode", productCode);
		query.setParameter("disabled", false);
		return (LineProds) query.uniqueResult();
	}
	@Override
	public LineProds getLineProductById(int lineProdId) {
		String sqlQuery ="FROM LineProds WHERE id.lineProdId= :lineProdId AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setParameter("lineProdId", lineProdId);
		query.setParameter("disabled", false);
		return (LineProds) query.uniqueResult();
	}
	@Override
	public int insert(LineProds lineProds) {
		
		Session session = this.sessionFactory.getCurrentSession();
		
		String sqlQuery = "INSERT INTO LineProds(LineProdCode, Barcode, LineProdName,  SKUUnitID, ProcessQty,WPlaceQty,Disabled) VALUES"
				+ "(:LineProdCode, :Barcode, :LineProdName, :SKUUnitID, :ProcessQty, :WPlaceQty, :Disabled)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("LineProdCode", lineProds.getId().getLineProdCode());
		query.setParameter("Barcode", lineProds.getBarcode());
		query.setParameter("LineProdName", lineProds.getLineProdName());
		query.setParameter("SKUUnitID", lineProds.getSkuunitId());
		query.setParameter("ProcessQty", lineProds.getProcessQty());
		query.setParameter("WPlaceQty", lineProds.getWplaceQty());
		query.setParameter("Disabled", false);
		return query.executeUpdate();
	}
	@Override
	public int update(LineProds lineProds) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE LineProds "
				+ " SET Barcode= :Barcode, LineProdName= :LineProdName,SKUUnitID= :SKUUnitID,ProcessQty= :ProcessQty,WPlaceQty= :WPlaceQty WHERE LineProdCode= :LineProdCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("LineProdCode", lineProds.getId().getLineProdCode());
		query.setParameter("Barcode", lineProds.getBarcode());
		query.setParameter("LineProdName", lineProds.getLineProdName());
		query.setParameter("SKUUnitID", lineProds.getSkuunitId());
		query.setParameter("ProcessQty", lineProds.getProcessQty());
		query.setParameter("WPlaceQty", lineProds.getWplaceQty());
		return query.executeUpdate();
	}
	@Override
	public LineProds find(String productCode) {
		String sqlQuery ="FROM LineProds WHERE id.lineProdCode= :lineProdCode AND Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setParameter("lineProdCode", productCode);
		query.setParameter("disabled", false);
		return (LineProds) query.uniqueResult();
	}
	@Override
	public int delete(String productCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE LineProds WHERE LineProdCode= :lineProdCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("lineProdCode", productCode);
		return query.executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<LineProds> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ="FROM LineProds WHERE Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setParameter("disabled", false);
		return query.list();
	}

}
