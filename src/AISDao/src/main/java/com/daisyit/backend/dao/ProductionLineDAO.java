package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.ProductionLine;
import com.daisyit.backend.model.ProductionLineId;

public interface ProductionLineDAO {
	int insert(String json);
	List<Object[]> find(String workingDate);
	int save(ProductionLine plan);
	int update(ProductionLine plan);
	int delete(int factoryId, int lineId, String workingDate, int shiftId);
}
