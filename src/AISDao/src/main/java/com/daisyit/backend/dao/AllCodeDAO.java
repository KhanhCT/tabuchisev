package com.daisyit.backend.dao;

import java.util.List;
import java.util.Map;

import com.daisyit.backend.model.AllCode;

public interface AllCodeDAO {
	public List<AllCode> getAllCodeByType(String codeType);
	public AllCode getCodeById(String codeName);
	//public int updateEnv(AllCode code);
	public int saveOrUpdate(AllCode code);
	Map<String, String> getCodeMap(String codeType);
}
