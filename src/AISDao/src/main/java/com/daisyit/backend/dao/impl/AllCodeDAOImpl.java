package com.daisyit.backend.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.AllCodeDAO;
import com.daisyit.backend.model.AllCode;

@Repository("allCodeDAO")
@Transactional
public class AllCodeDAOImpl implements AllCodeDAO {
	@Autowired
	protected SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<AllCode> getAllCodeByType(String codeType) {
		Session session = this.sessionFactory.getCurrentSession();
		List<AllCode> list = new ArrayList<>();
		String sqlQuery = "FROM AllCode WHERE type = :codeType";
		Query query = session.createQuery(sqlQuery);
		query.setString("codeType", codeType);
		list = query.list();
		return list;
	}

	@SuppressWarnings("unused")
	@Override
	public AllCode getCodeById(String codeName) {
		Session session = this.sessionFactory.getCurrentSession();
		List<AllCode> list = new ArrayList<>();
		String sqlQuery = "FROM AllCode WHERE id.codeName = :codeName";
		Query query = session.createQuery(sqlQuery);
		query.setString("codeName", codeName);
		return (AllCode) query.uniqueResult();
	}


	@Override
	public int saveOrUpdate(AllCode code) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE AllCode SET codeVal = :codeVal WHERE codeName = :codeName";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("codeName", code.getId().getCodeName());
		query.setString("codeVal", code.getCodeVal());
		if(query.executeUpdate() >0)
			return query.executeUpdate();
		sqlQuery = "INSERT INTO AllCode(CodeName, Type, CodeIdx, CodeVal, Modify) VALUES(:codeName, :type, :codeIdx, :codeVal, :modify)";	
		query = session.createSQLQuery(sqlQuery);
		query.setString("codeName", code.getId().getCodeName());
		query.setString("type", code.getType());
		query.setInteger("codeIdx", code.getCodeIdx());
		query.setString("codeVal", code.getCodeVal());
		query.setBoolean("modify", code.getModify());
		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getCodeMap(String codeType) {
		Map<String, String> map = new HashMap<>();
		Session session = this.sessionFactory.getCurrentSession();
		List<AllCode> list = new ArrayList<>();
		String sqlQuery = "FROM AllCode WHERE type = :codeType";
		Query query = session.createQuery(sqlQuery);
		query.setString("codeType", codeType);
		list = query.list();
		for(AllCode code : list) {
			map.put(code.getId().getCodeName(), code.getCodeVal());
		}
		return map;
	}

	

}
