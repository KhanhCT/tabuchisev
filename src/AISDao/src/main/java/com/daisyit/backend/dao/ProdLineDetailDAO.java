package com.daisyit.backend.dao;

import java.util.Date;
import java.util.List;

public interface ProdLineDetailDAO {
	List<Object[]> getMonthlyLineData(int factoryId, int lineId, Date fromDate, Date toDate, int type);
	List<Object[]> getFactoryData(int factoryId, Date curDate);
	List<Object[]> getMonthlyWorkerData(int workerId, Date workingDate);
}
