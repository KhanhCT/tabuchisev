package com.daisyit.backend.model;
// Generated Jan 10, 2019 10:12:43 AM by Hibernate Tools 3.6.0


import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Account generated by hbm2java
 */
@Entity
@Table(name="Account"
    ,schema="dbo"
    ,catalog="Tabuchi"
)
public class Account  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AccountId id;
     private String password;
     private Integer userRoleId;
     private Date lastLogin;
     private String token;
     private Boolean disable;

    public Account() {
    }

    public Account(Account account, String password, Integer userRoleId, Date lastLogin, String token, Boolean disable) {
       this.password = password;
       this.userRoleId = userRoleId;
       this.lastLogin = lastLogin;
       this.token = token;
       this.disable = disable;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="userId", column=@Column(name="UserId", nullable=false) ), 
        @AttributeOverride(name="userName", column=@Column(name="UserName", nullable=false, length=100) ) } )
    public AccountId getId() {
        return this.id;
    }
    
    public void setId(AccountId id) {
        this.id = id;
    }


    
    @Column(name="Password", length=100)
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    
    @Column(name="UserRoleId")
    public Integer getUserRoleId() {
        return this.userRoleId;
    }
    
    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="LastLogin", length=10)
    public Date getLastLogin() {
        return this.lastLogin;
    }
    
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    
    @Column(name="Token")
    public String getToken() {
        return this.token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }

    
    @Column(name="Disable")
    public Boolean getDisable() {
        return this.disable;
    }
    
    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

}


