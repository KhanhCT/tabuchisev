package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.LineProds;

public interface LineProdDAO {
	LineProds getLineProductByCode(String productCode);
	LineProds getLineProductById(int lineProdId);
	public int insert(LineProds lineProds);
	public int update(LineProds lineProds);
	public LineProds find(String productCode);
	public int delete(String productCode);
	
	List<LineProds> findAll();
}
