package com.daisyit.backend.dao.impl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseDAO<T> {
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	public abstract void insert(T data);
	public abstract void delete(T data);
	public abstract void update(T data);
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
