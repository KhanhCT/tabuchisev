package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.Factory;

public interface FactoryDAO {
	List<Factory> findAll();
	Factory find(String factoryCode);
}
