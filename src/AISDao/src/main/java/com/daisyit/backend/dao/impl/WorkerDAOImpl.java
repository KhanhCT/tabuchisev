package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.WorkerDAO;
import com.daisyit.backend.model.Worker;
@Repository("workerDAO")
@Transactional
public class WorkerDAOImpl implements WorkerDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(Worker worker) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT INTO Worker(WorkerCode, WorkerName, Barcode, Disabled) VALUES(:WorkerCode, :WorkerName, :Barcode, :Disabled)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("WorkerCode", worker.getId().getWorkerCode());
		query.setParameter("WorkerName", worker.getWorkerName());
		query.setParameter("Barcode", worker.getBarcode());
		query.setParameter("Disabled", worker.getDisabled());
		return query.executeUpdate();
	}

	@Override
	public int update(Worker worker) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Worker SET WorkerName=:WorkerName,Barcode =:Barcode, Disabled=:Disabled WHERE WorkerCode =:WorkerCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("WorkerCode", worker.getId().getWorkerCode());
		query.setParameter("WorkerName", worker.getWorkerName());
		query.setParameter("Barcode", worker.getBarcode());
		query.setParameter("Disabled", worker.getDisabled());
		return query.executeUpdate();
	}

	@Override
	public Worker find(String workerCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Worker WHERE id.workerCode= :workerCode";
		Query query = session.createQuery(sqlQuery);
		query.setString("workerCode", workerCode);
		return (Worker) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Worker> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Worker";
		Query query = session.createQuery(sqlQuery);
		return query.list();
	}

	@Override
	public int delete(int workerId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Worker SET Disabled= :disabled WHERE WorkerId= :workerId ";
		Query query = session.createSQLQuery(sqlQuery);
		query.setInteger("workerId", workerId);
		return query.executeUpdate();
	}
	@Override
	public int delete(String workerCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE Worker WHERE WorkerCode= :workerCode ";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("workerCode", workerCode);
		return query.executeUpdate();
	}

	@Override
	public int insert(String json) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_InsertWorker :json";
		Query query = session.createSQLQuery(sqlQuery);
		query.setString("json", json);
		return query.executeUpdate();
	}

}
