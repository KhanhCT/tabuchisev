package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.FactoryDAO;
import com.daisyit.backend.model.Factory;
@Repository("factoryDAO")
@Transactional
public class FactoryDAOImpl implements FactoryDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	@Override
	public List<Factory> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ="FROM Factory WHERE Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setBoolean("disabled", false);
		return query.list();
	}
	@Override
	public Factory find(String factoryCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ="FROM Factory WHERE id.factoryCode= :factoryCode AND Disabled= :disabled";
		Query query = session.createQuery(sqlQuery);
		query.setString("factoryCode", factoryCode);
		query.setBoolean("disabled", false);
		return (Factory) query.uniqueResult();
	}

}
