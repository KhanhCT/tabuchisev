package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProcessBOM;
import com.daisyit.backend.model.ProcessProdBom;
@Repository("processBOM")
@Transactional
public class ProcessBOMImpl implements ProcessBOM{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(ProcessProdBom lineProdBom) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(lineProdBom);
		}catch (Exception e) {		
			return 0;
		}
		return 1;
	}

	@Override
	public int update(ProcessProdBom lineProdBom) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.update(lineProdBom);
		} catch (Exception ex) {
			return 0;
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> find(int lineProdId,  int processId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = null;
		if(processId == 0 && lineProdId !=0) {
			sqlQuery = "SELECT t1.*, t2.LineProdCode, t3.MaterialCode FROM ProcessProdBOM as t1 inner join LineProds as t2 on t1.LineProd_ID = t2.LineProdID inner join Material as t3 on t1.Material_ID=t3.MaterialID"
					+ " WHERE  t1.LineProd_ID=" + String.valueOf(lineProdId);
		}else if(processId != 0 && lineProdId !=0) {
			sqlQuery = "SELECT t1.*, t2.LineProdCode, t3.MaterialCode FROM ProcessProdBOM as t1 inner join LineProds as t2 on t1.LineProd_ID = t2.LineProdID inner join Material as t3 on t1.Material_ID=t3.MaterialID"
					+ " WHERE  t1.LineProd_ID=" + String.valueOf(lineProdId) + " AND t1.Process_IDX=" + String.valueOf(processId);
		}
		
		Query query = session.createSQLQuery(sqlQuery);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "exec sp_GetAllProcessProdBOM 0, 0";
		Query query = session.createSQLQuery(sqlQuery);
		return query.list();
	}

	@Override
	public int delete(Integer lineProdId, Integer materialId, Integer processId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery ;
		Query query;
		if(lineProdId == null) {
			sqlQuery = "DELETE ProcessProdBom WHERE Material_ID= :MaterialId";
			query = session.createSQLQuery(sqlQuery);
			query.setParameter("MaterialId", materialId);
			return query.executeUpdate();
		}else if (materialId ==null ) {
			sqlQuery = "DELETE ProcessProdBom WHERE LineProd_ID= :LineProdId";
			query = session.createSQLQuery(sqlQuery);
			query.setParameter("LineProdId", lineProdId);
			return query.executeUpdate();
		}else {
			sqlQuery = "DELETE ProcessProdBom WHERE LineProd_ID= :LineProdId AND Material_ID= :MaterialId AND Process_IDX= :processIdx";
			query = session.createSQLQuery(sqlQuery);
			query.setParameter("MaterialId", materialId);
			query.setParameter("LineProdId", lineProdId);
			query.setParameter("processIdx", processId);
			return query.executeUpdate();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> exportBOM() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "select t2.Barcode as B1, t1.Process_IDX, t3.Barcode as B2 from ProcessProdBOM as t1 inner join LineProds as t2 on t1.LineProd_ID=t2.LineProdID inner join Material as t3 on t1.Material_ID=t3.MaterialID";
		Query query = session.createSQLQuery(sqlQuery);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessProdBom> findAllProcessBOM() {
		String sqlQuery = "FROM ProcessProdBOM WHERE Disabled= :disabled";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(sqlQuery);
		query.setParameter("disabled", false);
		return query.list();
	}

}
