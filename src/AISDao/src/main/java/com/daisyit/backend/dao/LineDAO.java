package com.daisyit.backend.dao;

import java.util.List;

import com.daisyit.backend.model.ProdLine;

public interface LineDAO {
	public int insert(ProdLine line);
	public int insert(String json);
	public int update(ProdLine line);
	public ProdLine find(int lineID, int factoryId);
	public ProdLine find(String lineCode, int factoryId);
	public int delete(int factoryId, Integer lineId);
	public int saveOrUpdate(ProdLine line);
	public List<ProdLine> findAll(int factoryId);
	public List<Object[]> findAll(int factoryId, int lineId);
	public List<ProdLine> findAll();
}
