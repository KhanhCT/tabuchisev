package com.daisyit.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daisyit.backend.dao.ProcessDAO;
import com.daisyit.backend.model.Process;
@Repository("processDAO")
@Transactional
public class ProcessDAOImpl implements ProcessDAO{
	@Autowired
	protected SessionFactory sessionFactory;
	@Override
	public int insert(Process process) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "INSERT INTO Process(ProcessCode, ProcessName, ProcessDoc,  Disabled) VALUES(:ProcessCode, :ProcessName, :ProcessDoc, :disabled)";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("ProcessCode", process.getId().getProcessCode());
		query.setParameter("ProcessName", process.getProcessName());
		query.setParameter("ProcessDoc", process.getProcessDoc());
		query.setParameter("disabled", process.getDisabled());
		return query.executeUpdate();
	}

	@Override
	public int update(Process process) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "UPDATE Process SET ProcessCode=:ProcessCode , ProcessName= :ProcessName, ProcessDoc= :ProcessDoc, Disabled=:disabled WHERE ProcessID=:processId";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("processId", process.getId().getProcessId());
		query.setParameter("ProcessCode", process.getId().getProcessCode());
		query.setParameter("ProcessName", process.getProcessName());
		query.setParameter("ProcessDoc", process.getProcessDoc());
		query.setParameter("disabled", process.getDisabled());
		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<com.daisyit.backend.model.Process> find(String processCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Process WHERE id.processCode= :processCode";
		if(processCode == null || processCode.equals("N/G")) {
			sqlQuery = "FROM Process";
		}
		Query query = session.createQuery(sqlQuery);
		if(processCode != null && !processCode.equals("N/G"))
			query.setParameter("processCode", processCode);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<com.daisyit.backend.model.Process> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "FROM Process";
		Query query = session.createQuery(sqlQuery);
		return query.list();
	}

	@Override
	public int delete(String processCode) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqlQuery = "DELETE Process Where ProcessCode= :processCode";
		Query query = session.createSQLQuery(sqlQuery);
		query.setParameter("processCode", processCode);
		return query.executeUpdate(); 
	}

}
