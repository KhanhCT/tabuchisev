myApp.controller('productionPlanCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.skuUnitList = [];
    $scope.epcLineList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    
    var nodeWPlace = '----[ALL]----';
    $scope.selectedWPlaceID = undefined;
    
    $scope.productList = [];
    $scope.selectedProduct = undefined;
    
    $scope.shiftList = [];
    $scope.selectedShist = undefined;
    $scope.selectedDate = moment(new Date(), 'DD/MM/YYYY').format('DD/MM/YYYY');
    $scope.planList = []
    $scope.listKeyTable = [{
        code : 'factoryName',
        name : 'Factory Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineDesc',
        name : 'Line Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'shiftName',
        name : 'Shift Name',
        iClass : "text-center"
    },
    {
        code : 'lineProdName',
        name : 'Product',
        iClass : "text-center"
    },{
        code : 'workingDate',
        name : 'Working Date',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'orderedQty',
        name : 'Ordered Qty',
        isSortFilter : true,
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    
	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
	var API_FIND_ALL_PRODUCT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAll';
	var API_FIND_ALL_SHIFT = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllShifts';
	var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadProductionPlan';
	
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };
    
    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;		
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.planList = resp.data
				showMessage("Success");
			}else{
				$scope.planList = [];
				showMessage("Failed");
			}
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	$scope.pkVisibility = {
        		wPlaceId: false,
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			lineId: 0,
    			factoryId: 0,
    			shiftId: 0,
    			lineProdId:  0,
    			workingDate: '',
    			orderedQty: 0,
    			disabled: false
    	}

    	$q.all([
        	$http({
    			    method: 'GET',
    			    url: API_FIND_ALL_FACTORY,
    			    headers: {'Content-Type': 'application/json; charset=utf-8'}
    			}).then(function successCallback(response) {
    			    if(response.data){
    			    	$scope.factoryList = response.data;
    			    	$scope.selectedFactory = $scope.factoryList[0];
    		    		$http({
    					    method: 'GET',
    					    url: API_FIND_ALL_SHIFT,
    					    headers: {'Content-Type': 'application/json; charset=utf-8'},
    					    params: {factoryId: $scope.selectedFactory.factoryId}
    					}).then(function successCallback(response) {
    					    if(response.data){
    					    	$scope.shiftList = response.data;
    					    	$scope.selectedShift= $scope.shiftList[0];
    					    }
    					}, function errorCallback(response) {
    					    console.log('ERROR CALL : [findAllFactory]');
    					});
    			    	$http({
    					    method: 'GET',
    					    url: API_FIND_ALL_LINE,
    					    headers: {'Content-Type': 'application/json; charset=utf-8'},
    					    params: {factoryId: $scope.selectedFactory.factoryId}
    					}).then(function successCallback(response) {
    					    if(response.data){
    					    	$scope.lineListFilter = response.data;
    					    	$scope.selectedLine = $scope.lineListFilter[0];
    					    }
    					}, function errorCallback(response) {
    					    console.log('ERROR CALL : [findAllLine]');
    					})
    			    }
    			}, function errorCallback(response) {
    			    console.log('ERROR CALL : [findAllFactory]');
    		}),
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_PRODUCT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.productList = response.data;
			    	$scope.selectedProduct = $scope.productList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllLine]');
			})
        ]).then (function () {
        	
        });
        $scope.refresh();
        
    };
    $scope.refresh = function(){
    	var API_FIND_ALL_PLAN = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProductionPlan'
    	$http({
		    method: 'GET',
		    url: API_FIND_ALL_PLAN,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {workingDate: 'N/G'}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.planList = response.data;
		    }else{
		    	$scope.planList = []
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllEpcLinePos]');
		})
    }
    
    $scope.factoryChanged = function(){
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
    }
    $scope.clean = function(){
    	$scope.pkVisibility = {
        		wPlaceId: false,
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			lineId: 0,
    			factoryId: 0,
    			shiftId: 0,
    			lineProdId:  0,
    			workingDate: '',
    			orderedQty: 0,
    			disabled: false
    	}
    };
    $scope.findEpcLinePos = function(){
    	var API_FIND_ALL_PLAN = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProductionPlan'
    	var curDate = moment($scope.selectedDate, 'DD/MM/YYYY').format('YYYY-MM-DD')	
    	$http({
		    method: 'GET',
		    url: API_FIND_ALL_PLAN,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {workingDate: curDate}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.planList = response.data;
		    }else{
		    	$scope.planList = []
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllEpcLinePos]');
		})
    }
    $scope.mofidyEpcLinePos = function(){
    	if($scope.selectedFactory && $scope.selectedLine){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
    		$scope.itemObj.shiftId = $scope.selectedShift.shiftId;
    		$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    		var curDate = moment($scope.selectedDate, 'DD/MM/YYYY').format('YYYY-MM-DD')
    		$scope.itemObj.workingDate = curDate;
        	var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'updateProductionPlan';
    		$http({
    		    method: 'POST',
    		    url: API_UPDATE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	$scope.refresh();
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Failed!");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [updateEpcLinePos]');
    		})
    	}
    }
    
    $scope.showEdit = function(row){
	   	$scope.btnVisibility = {
		        newBtn: true,
		        modifyBtn: false
		 };
	   	 $scope.pkVisibility = {
	   			wPlaceId: true,
		     }
	   	$scope.itemObj = row;
	   	$scope.selectedDate = moment(new Date(row.workingDate), 'DD/MM/YYYY').format('DD/MM/YYYY');
		for(var i =0; i<$scope.factoryList.length; i++){
    		if($scope.factoryList[i].factoryCode === row.factoryCode){
    			$scope.selectedFactory = $scope.factoryList[i];
    			break;
    		}		
    	}
		for(var i =0; i<$scope.productList.length; i++){
    		if($scope.productList[i].lineProdId === row.lineProdId){
    			$scope.selectedProduct = $scope.productList[i];
    			break;
    		}		
    	}
		for(var i =0; i<$scope.shiftList.length; i++){
    		if($scope.shiftList[i].shiftId === row.shiftId){
    			$scope.selectedShift = $scope.shiftList[i];
    			break;
    		}		
    	}
		for(var i =0; i<$scope.lineListFilter.length; i++){
    		if($scope.lineListFilter[i].lineId === row.lineId){
    			$scope.selecteLine = $scope.lineListFilter[i];
    			break;
    		}		
    	}
		$http({
		    method: 'GET',
		    url: API_FIND_ALL_LINE,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {factoryId: $scope.selectedFactory.factoryId}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.lineListFilter = response.data;
		    	for(var i =0; i<$scope.lineListFilter.length; i++){
		    		if($scope.lineListFilter[i].lineId === row.lineId){
		    			$scope.selectedLine = $scope.lineListFilter[i];
		    			break;
		    		}		
		    	}
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllFactory]');
		})
		
   }
    $scope.deleteEpcLinePos = function(){
    	$scope.itemObj.shiftId = $scope.selectedShift.shiftId;
		var curDate = moment($scope.selectedDate, 'DD/MM/YYYY').format('YYYY-MM-DD')
		$scope.itemObj.workingDate = curDate;
    	var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'deleteProductionPlan/' +$scope.selectedFactory.factoryId + "/" + $scope.selectedLine.lineId  +"/" + $scope.itemObj.shiftId +"/" + $scope.itemObj.workingDate;
    	$http({
		    method: 'DELETE',
		    url: API_DELETE_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {

		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	$scope.refresh();
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    $scope.saveEpcLinePos = function(){
    	if($scope.selectedFactory && $scope.selectedLine){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
    		$scope.itemObj.shiftId = $scope.selectedShift.shiftId;
    		$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    		var curDate = moment($scope.selectedDate, 'DD/MM/YYYY').format('YYYY-MM-DD')
    		$scope.itemObj.workingDate = curDate;
        	var API_NEW_PLAN = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'newProductionPlan';
    		$http({
    		    method: 'POST',
    		    url: API_NEW_PLAN,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	$scope.refresh();
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Line Pos existed.");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newEpcLine]');
    		})
    	}else{
    		showMessage("Failed");
    	}
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});