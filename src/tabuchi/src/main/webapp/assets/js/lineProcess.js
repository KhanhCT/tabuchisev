myApp.controller('lineProcessCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.skuUnitList = [];
    $scope.lineProcessList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    $scope.productList = [];
    $scope.selectedProduct = undefined;
    $scope.pkVisibility = {
    		lineProdCode: false,
    		materialCode: false
    }
    $scope.btnVisibility = {
        	newBtn: false,
        	modifyBtn: true
    };
    $scope.listKeyTable = [{
        code : 'processId',
        name : 'Process ID',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'processName',
        name : 'Process Name',
        iClass : "text-center"
    },{
        code : 'lineProdCode',
        name : 'Product Code',
        isSortFilter : true,
        iClass : "text-center"
    },
    {
        code : 'lineProdName',
        name : 'Product Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'minSt',
        name : 'Min ST',
        iClass : "text-center"
    },{
        code : 'maxSt',
        name : 'Max ST',
        iClass : "text-center"
    },{
        code : 'standardVideoLink',
        name : 'ST Video Link',
        iClass : "text-center"
    },{
        code : 'workVideoLink',
        name : 'Work Video Link',
        iClass : "text-center"
    },{
        code : 'wplaceQty',
        name : 'Work Place Qty',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.processIdList = [];
    $scope.selectedProcess = undefined;
    var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadProcessList';
    var API_FIND_ALL_PRODUCT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAll';
    var API_FIND_ALL_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProcess';
    var UPDATE_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'updateProcess';
    var noneProcessId = '-----[None]-----';
    //$scope.processIdList.unshift(noneProcessId);
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.lineProcessList = resp.data;
			}
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	$scope.itemObj = {  
        		lineProdId: 0,
    			lineProdCode: '',
    			processId: $scope.processIdList[0],
    			processName: '',
    			minSt: 0,
    			maxSt: 0,
    			standardVideoLink : '',
    			workVideoLink: '',
    			wplaceQty: 1,
    			disabled: false
        };
    	var API_GET_ALL_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';
       $q.all([
    	   $http({
			    method: 'GET',
			    url: API_GET_ALL_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode :'N/G'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.processIdList = response.data;
			    	$scope.selectedProcess = $scope.processIdList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_FIND_ALL_PRODUCT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.productList = response.data;
			    	$scope.selectedProduct = $scope.productList[0];
			    	$http({
					    method: 'GET',
					    url: API_FIND_ALL_PROCESS,
					    headers: {'Content-Type': 'application/json; charset=utf-8'},
					    params:{lineProdId: $scope.selectedProduct.lineProdId, processId: 0}
					}).then(function successCallback(response) {
					    if(response.data){
					    	$scope.lineProcessList = response.data;
					    }
					}, function errorCallback(response) {
					    console.log('ERROR CALL : [findAll]');
					})
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAll]');
			}),
			
			
        ]).then (function () {
        	
        })
    };
    $scope.clean = function(){
    	$scope.selectedProcess = $scope.processIdList[0];
    	$scope.itemObj = {  
        		lineProdId: 0,
    			lineProdCode: '',
    			processId: $scope.selectedProcess.processIdx,
    			processName: '',
    			minSt: 0,
    			maxSt: 0,
    			standardVideoLink : '',
    			workVideoLink: '',
    			wplaceQty: 1,
    			disabled: false
        }

    	$scope.pkVisibility = {
        		lineProdCode: false,
        		processId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    };
    
    $scope.showEdit = function(obj){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	
    	$scope.pkVisibility.lineProdCode = true;
    	$scope.pkVisibility.processId= true;
    	
    	$scope.itemObj.lineProdCode = obj.lineProdCode;
		$scope.itemObj.processId = obj.processId;
		$scope.itemObj.processName = obj.processName;
		$scope.itemObj.minSt = obj.minSt;
		$scope.itemObj.maxSt = obj.maxSt;
		$scope.itemObj.standardVideoLink = obj.standardVideoLink;
		$scope.itemObj.workVideoLink = obj.workVideoLink;
		$scope.itemObj.wplaceQty = obj.wplaceQty;
		$scope.itemObj.disabled = obj.disabled;
    		
    	for(var i =0; i< $scope.processIdList.length; i++){
			if($scope.processIdList[i].processIdx == $scope.itemObj.processId)
			{
				$scope.selectedProcess = $scope.processIdList[i];
				break;
			}
    	}
    }

    $scope.deleteProcess = function(){
    	$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    	$scope.itemObj.lineProdCode = $scope.selectedProduct.lineProdCode;
    	var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'deleteProcess/' +  $scope.itemObj.lineProdCode + "/" +  $scope.selectedProcess.processIdx;
    	if($scope.itemObj.lineProdCode!='')
		{  		
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    case 1:{
    			    	for(var i = 0; i< $scope.lineProcessList.length; i++)
    			    	{
    		    			
    			    		if($scope.lineProcessList[i].lineProdCode == $scope.itemObj.lineProdCode && $scope.lineProcessList[i].processId == $scope.selectedProcess.processIdx ){
    			    			$scope.lineProcessList.splice(i, 1); 
    			    			break;
    			    		}
    			    	}
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteLineProduct]');
    		})
		}else{
			showMessage("Line Product Code Or Material Code is not null");
		}
    }
    $scope.findProcess = function(){
    	
    	$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    	$scope.itemObj.lineProdCode = $scope.selectedProduct.lineProdCode;
    	if($scope.selectedProcess.processIdx == noneProcessId){
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params:{lineProdId:$scope.selectedProduct.lineProdId, processId: 0}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.processId= true;
			    	$scope.lineProcessList = response.data;
			    }else{
			    	showMessage("Not found Code: " + $scope.itemObj.lineProdCode );
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllProcess]');
			})
    	}else{
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params:{lineProdId:$scope.selectedProduct.lineProdId, processId: $scope.selectedProcess.processIdx}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.processId= true;
			    	$scope.lineProcessList = response.data;
			    }else{
			    	showMessage("Not found Code: " + $scope.itemObj.lineProdCode );
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllProcess]');
			})
    	}
    }
  
    $scope.modifyProcess = function(){  	
    	$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    	$scope.itemObj.lineProdCode = $scope.selectedProduct.lineProdCode;
    	$scope.itemObj.processId = $scope.selectedProcess.processIdx;
    	$scope.itemObj.processName= $scope.selectedProcess.processName;
    	$http({
		    method: 'POST',
		    url: UPDATE_NEW_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed");
			    }break;
			    case 1:{
			    	for(var i =0; i< $scope.lineProcessList.length; i++){
			    		if($scope.lineProcessList[i].lineProdCode == $scope.itemObj.lineProdCode && $scope.lineProcessList[i].processId == $scope.itemObj.processId ){
			    			$scope.lineProcessList[i].processId= $scope.itemObj.processId;
			    			$scope.lineProcessList[i].processName= $scope.itemObj.processName;
			    			$scope.lineProcessList[i].minSt= $scope.itemObj.minSt;
			    			$scope.lineProcessList[i].maxSt= $scope.itemObj.maxSt;	
			    			$scope.lineProcessList[i].standardVideoLink= $scope.itemObj.standardVideoLink;
			    			$scope.lineProcessList[i].workVideoLink= $scope.itemObj.workVideoLink;
			    			$scope.lineProcessList[i].wplaceQty= $scope.itemObj.wplaceQty;
			    			$scope.lineProcessList[i].disabled= $scope.itemObj.disabled;
			    			break;
			    		}
			    	}
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [updateProcess]');
		})
    }
       
    $scope.saveProcess = function(){
    	var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'newProcess';
    	$scope.itemObj.lineProdId = $scope.selectedProduct.lineProdId;
    	$scope.itemObj.lineProdCode = $scope.selectedProduct.lineProdCode;
    	$scope.itemObj.processId =  $scope.selectedProcess.processIdx
		$http({
		    method: 'POST',
		    url: API_NEW_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	var obj = {		
			    			lineProdCode: $scope.selectedProduct.lineProdCode,
			    			lineProdName: $scope.selectedProduct.lineProdName,
			    			processId: $scope.selectedProcess.processIdx,
			    			processName: $scope.selectedProcess.processName,
			    			minSt: $scope.itemObj.processId,
			    			maxSt:$scope.itemObj.maxSt,
			    			standardVideoLink: $scope.itemObj.standardVideoLink,
			    			workVideoLink: $scope.itemObj.workVideoLink,
			    			wplaceQty:$scope.itemObj.wplaceQty,
			    			disabled: $scope.itemObj.disabled
			        };
			    	$scope.clean();
			    	$scope.lineProcessList.push(obj);
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});