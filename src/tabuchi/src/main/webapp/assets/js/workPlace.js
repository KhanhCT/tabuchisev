myApp.controller('workPlaceCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.skuUnitList = [];
    $scope.epcLineList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    
    var nodeWPlace = '----[ALL]----';
    $scope.wPlaceIDList = [nodeWPlace, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    $scope.selectedWPlaceID = undefined;
    
    
    $scope.processList = [];
    $scope.selectedProcess = undefined;
    
    $scope.linePosList = [];
    
    $scope.listKeyTable = [{
        code : 'factoryName',
        name : 'Factory Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineDesc',
        name : 'Line Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'wPlaceId',
        name : 'Work Place ID',
        iClass : "text-center"
    },{
        code : 'processName',
        name : 'Process',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'ipv4Cam',
        name : 'Camera Ip',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'ipv4Tab',
        name : 'Tablet IP',
        isSortFilter : true,
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    
	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
	var API_FIND_ALL_EPC_POS = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllWorkPlaces';
	var API_FIND_ALL_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';
	var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadWorkPlace';
	
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };
    
    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;		
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.linePosList = resp.data
			}			
			$timeout(function(){
				$scope.showLoaderAllPage = false;
				
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	$scope.pkVisibility = {
        		wPlaceId: false,
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			wPlaceId : $scope.wPlaceIDList[0],
    			lineId: 0,
    			factoryId: 0,
    			ipv4Cam: '',
    			processId: 0,
    			ipv4Tab: '',
    			description: '',
    			disabled: false
    	}

    	$q.all([
        	$http({
    			    method: 'GET',
    			    url: API_FIND_ALL_FACTORY,
    			    headers: {'Content-Type': 'application/json; charset=utf-8'}
    			}).then(function successCallback(response) {
    			    if(response.data){
    			    	$scope.factoryList = response.data;
    			    	$scope.selectedFactory = $scope.factoryList[0];
    			    	$http({
    					    method: 'GET',
    					    url: API_FIND_ALL_LINE,
    					    headers: {'Content-Type': 'application/json; charset=utf-8'},
    					    params: {factoryId: $scope.selectedFactory.factoryId}
    					}).then(function successCallback(response) {
    					    if(response.data){
    					    	$scope.lineListFilter = response.data;
    					    	$scope.selectedLine = $scope.lineListFilter[0];
    					    	$http({
    	    					    method: 'GET',
    	    					    url: API_FIND_ALL_EPC_POS,
    	    					    headers: {'Content-Type': 'application/json; charset=utf-8'},
    	    					    params: {wPlaceId: 0, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
    	    					}).then(function successCallback(response) {
    	    					    if(response.data){
    	    					    	$scope.linePosList = response.data;
    	    					    }
    	    					}, function errorCallback(response) {
    	    					    console.log('ERROR CALL : [findAllEpcLinePos]');
    	    					})
    					    }
    					}, function errorCallback(response) {
    					    console.log('ERROR CALL : [findAllLine]');
    					})
    			    }
    			}, function errorCallback(response) {
    			    console.log('ERROR CALL : [findAllFactory]');
    		}),
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode: 'N/G'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.processList = response.data;
			    	$scope.selectedProcess = $scope.processList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
		}),
        ]).then (function () {
        	
        })
        
    };
    $scope.refresh = function(){
    	$http({
		    method: 'GET',
		    url: API_FIND_ALL_EPC_POS,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {wPlaceId: 0, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.linePosList = response.data;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllEpcLinePos]');
		})
    }
    
    $scope.factoryChanged = function(){
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
    }
    $scope.clean = function(){
    	$scope.pkVisibility = {
        		wPlaceId: false,
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			wPlaceId : $scope.wPlaceIDList[0],
    			lineId: 0,
    			factoryId: 0,
    			ipv4Cam: '',
    			processId: 0,
    			ipv4Tab: '',
    			description: '',
    			disabled: false
    	}
    };
    $scope.findEpcLinePos = function(){
    	if($scope.selectedFactory && $scope.selectedLine && $scope.itemObj.wPlaceId == nodeWPlace){
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_EPC_POS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {wPlaceId: 0, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	if(response.data.length >0){
			    		$scope.linePosList = response.data;
			    	}else{
			    		showMessage("Not found!");
			    		$scope.linePosList = []
			    	}
			    }else{
			    	showMessage("Not found!");
			    	$scope.linePosList = []
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllEpcLinePos]');
			})
	
    	}else{
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_EPC_POS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {wPlaceId: $scope.itemObj.wPlaceId, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	if(response.data.length >0){
			    		$scope.linePosList = response.data;
			    	}else{
			    		showMessage("Not found!");
			    	}
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllEpcLinePos]');
			})
    	}
    }
    $scope.mofidyEpcLinePos = function(){
    	if($scope.selectedFactory && $scope.selectedLine){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
    		$scope.itemObj.processId = $scope.selectedProcess.processIdx;
        	var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'updateWorkPlace';
    		$http({
    		    method: 'POST',
    		    url: API_UPDATE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	$scope.refresh();
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Failed!");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [updateEpcLinePos]');
    		})
    	}
    }
    
    $scope.showEdit = function(row){
	   	$scope.btnVisibility = {
		        newBtn: true,
		        modifyBtn: false
		 };
	   	 $scope.pkVisibility = {
	   			wPlaceId: true,
		     }
	   	$scope.itemObj.wPlaceId = row.wPlaceId;
		$scope.itemObj.workerCode = row.workerCode;
		$scope.itemObj.ipv4Cam = row.ipv4Cam;
		$scope.itemObj.ipv4Tab = row.ipv4Tab;
		$scope.selectedWPlaceID= row.wPlaceId;
		$scope.itemObj.disabled= row.disabled;
		$scope.itemObj.description= row.description;
		for(var i =0; i<$scope.factoryList.length; i++){
    		if($scope.factoryList[i].factoryCode === row.factoryCode){
    			$scope.selectedFactory = $scope.factoryList[i];
    			break;
    		}		
    	}
		$http({
		    method: 'GET',
		    url: API_FIND_ALL_LINE,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {factoryId: $scope.selectedFactory.factoryId}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.lineListFilter = response.data;
		    	for(var i =0; i<$scope.lineListFilter.length; i++){
		    		if($scope.lineListFilter[i].lineId === row.lineId){
		    			$scope.selectedLine = $scope.lineListFilter[i];
		    			break;
		    		}		
		    	}
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllFactory]');
		})
		
		
		for(var i =0; i<$scope.processList.length; i++){
    		if($scope.processList[i].processIdx === row.processId){
    			$scope.selectedProcess = $scope.processList[i];
    			break;
    		}		
    	}
   }
    $scope.deleteEpcLinePos = function(){
    	var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'deleteWorkPlace/' +$scope.selectedFactory.factoryId + "/" + $scope.selectedLine.lineId  +"/" + $scope.itemObj.wPlaceId;
    	$http({
		    method: 'DELETE',
		    url: API_DELETE_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {

		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	$scope.refresh();
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    $scope.saveEpcLinePos = function(){
    	if($scope.selectedFactory && $scope.selectedLine && $scope.itemObj.wPlaceId && $scope.selectedProcess){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
    		$scope.itemObj.processId = $scope.selectedProcess.processIdx;
        	var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'newWorkPlace';
        	
    		$http({
    		    method: 'POST',
    		    url: API_NEW_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	$scope.refresh();
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Line Pos existed.");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newEpcLine]');
    		})
    	}else{
    		showMessage("Failed");
    	}
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});