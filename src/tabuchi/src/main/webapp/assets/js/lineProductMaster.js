myApp.controller('productMasterCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', 'Upload', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, Upload, $http,$timeout, $q) {
    $scope.itemObj = {
    		lineProdCode: '',
    		barcode: '',
    		lineProdName: '',
    		skuUnitID: 0,
    		processQty: 1,
    		wplaceQty:1 	
    };
    $scope.showTable = true;
    
    $scope.skuUnitList = [];
    $scope.selectedSKUUnit = undefined;
    
    $scope.productList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    $scope.pkVisibility = {
    		lineProdCode: false,
    		barcode: false
    }
    $scope.btnVisibility = {
        	newBtn: false,
        	modifyBtn: true
        };
    
    $scope.listKeyTable = [{
        code : 'lineProdCode',
        name : 'Product Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'barcode',
        name : 'Barcode',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'lineProdName',
        name : 'Product Name',
        iClass : "text-center"
    },{
        code : 'skuUnitName',
        name : 'SKU Unit',
        iClass : "text-center"
    },{
        code : 'processQty',
        name : 'Process Qty',
        iClass : "text-center"
    },{
        code : 'wplaceQty',
        name : 'Work Place Qty',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadProductList';
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.productList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	var API_GET_SKU_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllSkuUnits';
    	var API_GET_LINE_PRODUCT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAll';
    	$q.all([
    		$http({
			    method: 'GET',
			    url: API_GET_SKU_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.skuUnitList  = response.data;
			        $scope.selectedSKUUnit = $scope.skuUnitList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_LINE_PRODUCT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	for(var j = 0; j<response.data.length; j++){
			    		var itemObj = response.data[j]
			    		itemObj.skuUnitName = '';
			    		for(var i =0; i< $scope.skuUnitList.length; i++){
				    		if($scope.skuUnitList[i].unitId == itemObj.skuUnitID)
				    		{
				    			itemObj.skuUnitName = $scope.skuUnitList[i].unitName		    			
				    			break;
				    		}
				    	}
			    		$scope.productList.push(itemObj);
			    	}
			    	
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			})
			
        ]).then (function () {
        	
        })
    };
    $scope.deleteProduct = function(){
    	if($scope.itemObj.lineProdCode !='')
		{  		
    		var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'deleteLineProduct/' +  $scope.itemObj.lineProdCode;
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    case 1:{
    			    	showMessage("Success!");
    			    	for(var i = 0; i< $scope.productList.length; i++)
    			    	{
    			    		if($scope.productList[i].lineProdCode === $scope.itemObj.lineProdCode){
    			    			$scope.productList.splice(i, 1); 
    			    			break;
    			    		}
    			    	}
    			    	$scope.clean();
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteLineProduct]');
    		})
		}else{
			showMessage("Line Product Code is not null");
		}
    };
    $scope.modifyProduct = function(){
    	if($scope.itemObj.lineProdCode!='' && $scope.itemObj.barcode!=''){
    		var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'updateLineProduct';
    		$scope.itemObj.skuUnitID = $scope.selectedSKUUnit.unitId;
    		$http({
			    method: 'POST',
			    url: API_UPDATE_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	for(var i = 0; i< $scope.productList.length; i++)
    			    	{
    			    		if($scope.productList[i].lineProdCode === $scope.itemObj.lineProdCode){
    			    			$scope.productList[i].barcode = $scope.itemObj.barcode;
    			    			$scope.productList[i].lineProdName = $scope.itemObj.lineProdName;
    			    			$scope.productList[i].skuUnitID = $scope.itemObj.skuUnitID;
    			    			$scope.productList[i].skuUnitName =  $scope.selectedSKUUnit.unitName;
    			    			$scope.productList[i].processQty = $scope.itemObj.processQty;
    			    			$scope.productList[i].wplaceQty = $scope.itemObj.wplaceQty;	
    			    			break;
    			    		}
    			    	}
				    	$scope.clean();
				    	showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [updateLineProduct]');
			})
    	}else{
			showMessage("Line Product Code is not null");
		}
    };
    $scope.findProduct = function(){
    	if($scope.itemObj.lineProdCode!=''){
    		var API_FIND_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findLineProduct';
    		$http({
			    method: 'GET',
			    url: API_FIND_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {lineProdCode: $scope.itemObj.lineProdCode}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.barcode= true;
			    	$scope.itemObj = response.data;
			    	for(var i =0; i< $scope.skuUnitList.length; i++){
			    		if($scope.skuUnitList[i].unitId == $scope.itemObj.skuUnitID)
			    		{
			    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
			    			break;
			    		}
			    	}
			    }else{
			    	showMessage("Not found Product Code: " + $scope.itemObj.lineProdCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findMaterial]');
			})
    	}else{
			showMessage("Line Product Code is not null");
		}
    };
    $scope.clean = function(){
    	$scope.btnVisibility.newBtn = false;
    	$scope.btnVisibility.modifyBtn = true;
    	$scope.itemObj = {
        		lineProdCode: '',
        		barcode: '',
        		lineProdName: '',
        		skuUnitID: 0,
        		processQty: 1,
        		wplaceQty:1 	
        };
    	$scope.pkVisibility.lineProdCode = false;
    	$scope.pkVisibility.barcode= false;
    };
    $scope.showEdit = function(obj){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.lineProdCode = true;
    	$scope.pkVisibility.barcode= false;
		$scope.itemObj.lineProdCode = obj.lineProdCode;
		$scope.itemObj.barcode= obj.barcode;
		$scope.itemObj.lineProdName= obj.lineProdName;
		$scope.itemObj.skuUnitID= obj.skuUnitID;
		$scope.itemObj.processQty= obj.processQty;
		$scope.itemObj.wplaceQty = obj.wplaceQty;
		for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == obj.skuUnitID)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    }
    $scope.showDelete = function(obj){
    	$scope.itemObj.lineProdCode = obj.lineProdCode;
		$scope.itemObj.barcode= obj.barcode;
		$scope.itemObj.lineProdName= obj.lineProdName;
		$scope.itemObj.skuUnitID= obj.skuUnitID;
		$scope.itemObj.processQty= obj.processQty;
		$scope.itemObj.wplaceQty = obj.wplaceQty;
		for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == row.skuUnitID)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    }
    $scope.saveProduct = function(){
    	if($scope.itemObj.lineProdCode!='' && $scope.itemObj.barcode!=''){
    		var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'newLineProduct';
    		$scope.itemObj.skuUnitID = $scope.selectedSKUUnit.unitId;
    		$http({
			    method: 'POST',
			    url: API_NEW_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	var obj = {
				    			lineProdCode: $scope.itemObj.lineProdCode,
				        		barcode: $scope.itemObj.barcode,
				        		lineProdName: $scope.itemObj.lineProdName,
				        		skuUnitID: $scope.skuUnitID,
				        		skuUnitName: $scope.selectedSKUUnit.unitName,
				        		processQty : $scope.itemObj.processQty,
				        		wplaceQty: $scope.itemObj.wplaceQty,
				        };
				        $scope.itemObj = {
				        		lineProdCode: '',
				        		barcode: '',
				        		lineProdName: '',
				        		skuUnitID: 0,
				        		processQty: 1,
				        		wplaceQty:1 	
				        };
				    	$scope.selectedSKUUnit = $scope.skuUnitList[0];
				    	$scope.productList.push(obj);
				    	showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newLineProduct]');
			})
    	}else{
    		showMessage("Failed!");
    	}
    };
    
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});