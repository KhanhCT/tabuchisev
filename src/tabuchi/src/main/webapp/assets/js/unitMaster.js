myApp.controller('unitMasterCrtl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
    $scope.itemObj = {};
    $scope.showTable = true;
    
    $scope.skuUnitList = [];
    $scope.selectedSKUUnit = undefined;
    
    $scope.usageUnitList = [];
    $scope.selectedUsageUnit = undefined;
    
    $scope.unitList = [];
    
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.deleteMaterial = undefined;
    
    $scope.btnVisibility = {
    	newBtn: false,
    	modifyBtn: true
    };
    $scope.pkVisibility = {
    	unitCode: false
    }
    
    $scope.listKeyTable = [{
        code : 'unitCode',
        name : 'Unit Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'unitName',
        name : 'Unit Name',
        isSortFilter : true,
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		var URI_UPLOAD_FILE = 'uploadCustomerList';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.customerList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	};
    $scope.clean = function(){
    	$scope.btnVisibility.newBtn = false;
    	$scope.btnVisibility.modifyBtn = true;
    	$scope.itemObj = {
    			unitCode: '',
        		unitName: '',
        		unitUsage: false,
        		unitSKU: true,
        	};
    	$scope.pkVisibility.unitCode = false;
    }
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.itemObj = {
    		unitCode: '',
    		unitName: '',
    		unitUsage: false,
    		unitSKU: true,
    	}
    	showMessage('Initalizing.....');
    	var API_GET_SKU_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllUnits';
    	$q.all([
			$http({
			    method: 'GET',
			    url: API_GET_SKU_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.unitList  = response.data;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
        ]).then (function () {
        	
        })
        
    };
    
    $scope.showEditUnit = function(row){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.unitCode = true;
    	
		$scope.itemObj.unitCode = row.unitCode;
		$scope.itemObj.unitName= row.unitName;
		$scope.itemObj.unitUsage= row.unitUsage;
		$scope.itemObj.unitSKU= row.unitSKU;
    };
    
    $scope.showDeleteUnit = function(row){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.unitCode = true;
    	
		$scope.itemObj.unitCode = row.unitCode;
		$scope.itemObj.unitName= row.unitName;
		$scope.itemObj.unitUsage= row.unitUsage;
		$scope.itemObj.unitSKU= row.unitSKU;
    };
    $scope.deleteUnit = function(){
    	if($scope.itemObj.unitCode !='')
		{  		
    		var API_DELETE_UNIT= contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'deleteUnit/' +  $scope.itemObj.unitCode;
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_UNIT,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    case 1:{
    			    	showMessage("Success!");
    			    	for(var i = 0; i< $scope.unitList.length; i++)
    			    	{
    			    		if($scope.unitList[i].unitCode === $scope.itemObj.unitCode){
    			    			$scope.unitList.splice(i, 1); 
    			    			break;
    			    		}
    			    	}
    			    	$scope.clean();
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteMaterial]');
    		})
		}else{
			showMessage("Material Code or Barcode is not null");
		}
    }
    
    $scope.findUnit = function(){
    	if($scope.itemObj.unitCode!=''){
    		var API_FIND_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findUnit';
    		$http({
			    method: 'GET',
			    url: API_FIND_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {unitCode: $scope.itemObj.unitCode}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.itemObj = response.data;
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.unitCode = true;			    	
			    }else{
			    	showMessage("Not found Unit Code: " + $scope.itemObj.materialCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findMaterial]');
			})
    	}else{
    		showMessage("Unit Code is not null");
		}
    }
    $scope.modifyUnit = function(){
    	if($scope.itemObj.unitCode!=''){
    		var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'updateUnit';
    		$http({
			    method: 'POST',
			    url: API_UPDATE_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	for(var i = 0; i< $scope.unitList.length; i++)
    			    	{
    			    		if($scope.unitList[i].unitCode === $scope.itemObj.unitCode){
    			    			$scope.unitList[i].unitName =  $scope.itemObj.unitName;
    			    			$scope.unitList[i].unitUsage = $scope.itemObj.unitUsage;
    			    			$scope.unitList[i].unitSKU = $scope.itemObj.unitSKU;
    			    			break;
    			    		}
    			    	}
				    	 $scope.clean();			    	
				    	 showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newMaterial]');
			})
    	}else{
    		showMessage("Unit Code is not null");
		}
    };
    $scope.saveUnit = function(){
    	if($scope.itemObj.unitCode!='' ){
    		var API_NEW_UNIT= contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'newUnit';
    		$http({
			    method: 'POST',
			    url: API_NEW_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	var obj = {
				    			unitCode: $scope.itemObj.unitCode,
				    			unitName: $scope.itemObj.unitName,
				    			unitUsage: $scope.itemObj.unitUsage,
				    			unitSKU: $scope.itemObj.unitSKU,
				        };
				    	$scope.clean();
				    	$scope.unitList.push(obj);
				    	showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newMaterial]');
			})
    	}else{
			showMessage("Unit Code is not null");
		}
    };
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    };
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});