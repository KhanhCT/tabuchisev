myApp.controller('processMasterCrtl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
    $scope.itemObj = {};
    $scope.showTable = true;
    
    $scope.unitList = [];
    
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.deleteMaterial = undefined;
    
    $scope.btnVisibility = {
    	newBtn: false,
    	modifyBtn: true
    };
    $scope.pkVisibility = {
    	unitCode: false
    }
    
    $scope.listKeyTable = [{
        code : 'processCode',
        name : 'Process Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'processName',
        name : 'Process Name',
        isSortFilter : true,
        iClass : "text-center"
    },
    {
        code : 'processDoc',
        name : 'Description',
        isSortFilter : true,
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		var URI_UPLOAD_FILE = 'uploadCustomerList';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.customerList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	};
    $scope.clean = function(){
    	$scope.btnVisibility.newBtn = false;
    	$scope.btnVisibility.modifyBtn = true;
    	$scope.itemObj = {
    			processIdx: 0,
    			processCode: '',
        		processName: '',
        		processDoc: '',
        		disabled: false,
        	};
    	$scope.pkVisibility.unitCode = false;
    }
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.itemObj = {
    		processIdx: 0,
    		processCode: '',
        	processName: '',
        	processDoc: '',
        	disabled: false,
    	}
    	showMessage('Initalizing.....');
    	var API_GET_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';
    	$q.all([
			$http({
			    method: 'GET',
			    url: API_GET_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode: 'N/G'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.unitList  = response.data;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
        ]).then (function () {
        	
        })
        
    };
    $scope.refresh = function() {
    	var API_GET_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';
    	$q.all([
			$http({
			    method: 'GET',
			    url: API_GET_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode: 'N/G'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.unitList  = response.data;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
        ]).then (function () {
        	
        })
	}
    
    $scope.showUnitInfo = function(row){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.unitCode = true;
    	
    	$scope.itemObj.processIdx = row.processIdx;
    	$scope.itemObj.processCode = row.processCode;
		$scope.itemObj.processName= row.processName;
		$scope.itemObj.processDoc= row.processDoc;
		$scope.itemObj.disabled = row.disabled;
    };
    $scope.deleteUnit = function(){
    	if($scope.itemObj.processCode !='')
		{  		
    		var API_DELETE_UNIT= contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'deleteProcess/' +  $scope.itemObj.processCode;
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_UNIT,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    default:{
    			    	showMessage("Success!");
    			    	$scope.refresh();
    			    	$scope.clean();
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteProcess]');
    		})
		}else{
			showMessage("Process code is not null");
		}
    }
    
    $scope.findUnit = function(){
    	if($scope.itemObj.processCode!=''){
    		var API_FIND_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';
    		$http({
			    method: 'GET',
			    url: API_FIND_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode: $scope.itemObj.processCode}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.itemObj = response.data;
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.unitCode = true;			    	
			    }else{
			    	showMessage("Not found Unit Code: " + $scope.itemObj.materialCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findProcess]');
			})
    	}else{
    		showMessage("Process Code is not null");
		}
    }
    $scope.modifyUnit = function(){
    	if($scope.itemObj.processCode!=''){
    		var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'updateProcess';
    		$http({
			    method: 'POST',
			    url: API_UPDATE_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    default: {
				    	 $scope.refresh();
				    	 $scope.clean();			    	
				    	 showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newProcess]');
			})
    	}else{
    		showMessage("Process Code is not null");
		}
    };
    $scope.saveUnit = function(){
    	if($scope.itemObj.unitCode!='' ){
    		var API_NEW_UNIT= contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'newProcess';
    		$http({
			    method: 'POST',
			    url: API_NEW_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	$scope.refresh();
				    	$scope.clean();
				    	showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newProcess]');
			})
    	}else{
			showMessage("Process Code is not null");
		}
    };
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    };
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});