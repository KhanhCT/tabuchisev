myApp.controller('productMasterCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    
	$scope.itemObj = {
		workerId: 0,
    	workerCode: '',
    	workerName: '',
    	barcode: '',
    	disabled: false,
    };

    $scope.showTable = true;
    $scope.skuUnitList = [];
    $scope.productList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    $scope.listKeyTable = [{
        code : 'workerCode',
        name : 'Worker Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'workerName',
        name : 'Worker Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'barcode',
        name : 'Barcode',
        iClass : "text-center"
    }];
    
    $scope.workerList = [];
    $scope.selectedWorker = undefined;
       
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
    	
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		var URI_UPLOAD_FILE = 'uploadWorkerList';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.workerList = resp.data;
			}
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
	var API_FIND_ALL_WORKERS = contextPath + CONTSTANT_SERVICE_URI.URI_WORKER + 'findAllWorkers';
	var API_FIND_ALL_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProcess';
	var API_FIND_ALL_PRODUCT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAll';
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	
        $q.all([
        	$http({
			    method: 'GET',
			    url: API_FIND_ALL_WORKERS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.workerList = response.data;
			    	$scope.selectedWorker = $scope.workerList[0];			    	
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
         ]).then (function () {
         	
         })
        
    };
    $scope.refresh = function(){
    	$http({
		    method: 'GET',
		    url: API_FIND_ALL_WORKERS,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.workerList = response.data;
		    	$scope.selectedWorker = $scope.workerList[0];			    	
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllFactory]');
		})
    }
    
    $scope.showEdit = function(obj){
    	$scope.itemObj= obj;
    }
    $scope.saveWorker = function(){ 	
    	var API_NEW_WORKER = contextPath + CONTSTANT_SERVICE_URI.URI_WORKER + 'newWorker'
    	$http({
			    method: 'POST',
			    url: API_NEW_WORKER,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
				if(response.data){
					if(response.data >0){
						showMessage("Success");
						$scope.refresh();
					}else{
						showMessage("Failed");
					}
				}else{
					showMessage("Failed");
				}
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [deleteWorker]');
			})
    }
    $scope.findWorker = function(){
    	var API_FIND_WORKER = contextPath + CONTSTANT_SERVICE_URI.URI_WORKER + 'findWorker';
    	if($scope.itemObj.workerCode ==undefined){
    		showMessage("Failed");
    		return;
    	}
    	$http({
			    method: 'GET',
			    url: API_FIND_WORKER,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {workerCode: $scope.itemObj.workerCode}
			}).then(function successCallback(response) {
				if(response.data){
					if(response.data){		
						$scope.itemObj = response.data;
						showMessage("Success");
					}else{
						showMessage("Failed");
					}
				}else{
					showMessage("Failed");
				}
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [deleteWorker]');
			})
    }
    $scope.deleteWorker = function(){
    	var API_DELETE_WORKER = contextPath + CONTSTANT_SERVICE_URI.URI_WORKER + 'deleteWorkerCode/' + $scope.itemObj.workerCode ;
    	$http({
			    method: 'DELETE',
			    url: API_DELETE_WORKER,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			}).then(function successCallback(response) {
				if(response.data){
					if(response.data >0){
						showMessage("Success");
						$scope.refresh();
					}else{
						showMessage("Failed");
					}
				}else{
					showMessage("Failed");
				}
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [deleteWorker]');
			})
    }
    $scope.updateWorker = function(){
    	var API_UPDATE_WORKER = contextPath + CONTSTANT_SERVICE_URI.URI_WORKER + 'updateWorker'
    	$http({
		    method: 'POST',
		    url: API_UPDATE_WORKER,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
			if(response.data){
				if(response.data >0){
					showMessage("Success");
					$scope.refresh();
				}else{
					showMessage("Failed");
				}
			}else{
				showMessage("Failed");
			}
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [deleteWorker]');
		})
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});