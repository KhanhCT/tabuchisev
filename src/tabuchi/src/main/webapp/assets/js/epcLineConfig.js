myApp.controller('epcLineConfigCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = [];
    $scope.showTable = true;
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    var noneEpc = '-----[None]-----';
    $scope.epcIDList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43,43,44,45,46,47,48,49];
    $scope.epcIDList.unshift(noneEpc);
    $scope.selectedEPCID = undefined;
    var noneLine = {lineDesc: '-----[None]-----'};
    $scope.epcLineList = [];
    
    $scope.listKeyTable = [{
        code : 'epcid',
        name : 'EPC ID',
        iClass : "text-center"
    },{
        code : 'epcname',
        name : 'EPC Name',
        iClass : "text-center"
    },{
        code : 'factoryCode',
        name : 'Factory Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'factoryName',
        name : 'Factory Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineDesc',
        name : 'Line Name',
        isSortFilter : true,
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };
    var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory'; 	
	var API_FIND_ALL_EPC = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllEpcLine';
	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
	var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadEPCConfig';
	var API_FIND_ALL_EPC = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllEpcLine';
	var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'updateEpcLine';
    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.epcLineList = resp.data;
			}
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	$scope.pkVisibility = {
        		epcId: false,
        		factoryId: false,
        		lineId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			epcid:$scope.epcIDList[0],
    			factoryId: 0,
    			lineId :0,
    			epcname: ''	
    	}
    	
    	
    	$q.all([
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_FACTORY,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.factoryList = response.data;
			    	$scope.selectedFactory = $scope.factoryList[0];
			    	if($scope.selectedFactory !=undefined){
			    		$http({
						    method: 'GET',
						    url: API_FIND_ALL_LINE,
						    headers: {'Content-Type': 'application/json; charset=utf-8'},
						    params: {factoryId: $scope.selectedFactory.factoryId}
						}).then(function successCallback(response) {
						    if(response.data){
						    	$scope.lineListFilter = response.data;
						    	$scope.lineListFilter.unshift(noneLine);
						    	$scope.selectedLine = $scope.lineListFilter[0];
						    	$http({
								    method: 'GET',
								    url: API_FIND_ALL_EPC,
								    headers: {'Content-Type': 'application/json; charset=utf-8'},
									params: {factoryId: $scope.selectedFactory.factoryId, lineId: 0, epcId : 0}
								}).then(function successCallback(response) {
								    if(response.data){
								    	$scope.epcLineList = response.data;
								    }
								}, function errorCallback(response) {
								    console.log('ERROR CALL : [findAllEpcLine]');
								})
						    }
						}, function errorCallback(response) {
						    console.log('ERROR CALL : [findAllFactory]');
						})
			    		
			    	}
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			}),
			
         ]).then (function () {
         	
         })	
    };
    
    $scope.factoryChanged = function(){
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){	
			    	if(response.data.length >0)
			    	{
			    		$scope.lineListFilter = response.data;
			    		$scope.lineListFilter.unshift(noneLine);
				    	$scope.selectedLine = $scope.lineListFilter[0];				    	
			    	}else{
			    		$scope.lineListFilter = []
			    		$scope.lineListFilter.push(noneLine);
			    		$scope.selectedLine = $scope.lineListFilter[0];
			    	}
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
		})   	
    }
    
    $scope.clean = function(){
    	$scope.pkVisibility = {
        		epcId: false,
        		factoryId: false,
        		lineId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	$scope.itemObj = {
    			epcid:$scope.epcIDList[0],
    			factoryId: 0,
    			lineId : 0,
    			epcname: ''	
    	}
    }
    $scope.findEpcLine = function(){
    	
    	if($scope.selectedLine.lineDesc == noneLine.lineDesc){
    		$http({
    		    method: 'GET',
    		    url: API_FIND_ALL_EPC,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    params: {epcId: 0, factoryId: $scope.selectedFactory.factoryId, lineId: 0}
    		}).then(function successCallback(response) {
    		    if(response.data){
    		    	$scope.epcLineList = response.data;
    		    	$scope.btnVisibility = {
			             	newBtn: true,
			             	modifyBtn: false
			         };
			    	 $scope.pkVisibility = {
			         		epcId: false,
			         		factoryId: false,
			         		lineId: false
			         }
    		    }else{
    		    	showMessage("Not found!");
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newProcess]');
    		})
    	} else if($scope.itemObj.epcid == noneEpc){
    		$http({
    		    method: 'GET',
    		    url: API_FIND_ALL_EPC,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    params: {epcId: 0, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
    		}).then(function successCallback(response) {
    		    if(response.data){
    		    	$scope.epcLineList = response.data;
			    	showMessage("Success!");
			    	 
    		    }else{
    		    	showMessage("Not found!");
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newProcess]');
    		})
    	}else {
    		$http({
    		    method: 'GET',
    		    url: API_FIND_ALL_EPC,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    params: {epcId: $scope.itemObj.epcid, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId}
    		}).then(function successCallback(response) {
    		    if(response.data){
    		    	$scope.epcLineList = response.data;
    		    	$scope.btnVisibility = {
			             	newBtn: true,
			             	modifyBtn: false
			         };
			    	 $scope.pkVisibility = {
			         		epcId: true,
			         		factoryId: true,
			         		lineId: true
			         }
    		    }else{
    		    	showMessage("Not found!");
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newProcess]');
    		})
    	}
    }
    
    $scope.mofidyEpcLine = function(){
    	if($scope.selectedFactory && $scope.selectedLine){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
        	
    		$http({
    		    method: 'POST',
    		    url: API_UPDATE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	for(var i =0; i<$scope.epcLineList.length; i++){
    			    		if($scope.epcLineList[i].factoryCode ==$scope.selectedFactory.factoryCode && $scope.itemObj.epcid == $scope.epcLineList[i].epcid && $scope.selectedLine.lineId ===  $scope.epcLineList[i].lineId){
    			    			$scope.epcLineList[i].epcname = $scope.itemObj.epcname;
    			    			break;
    			    		}		
    			    	}
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Failed!");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newProcess]');
    		})
    	}
    }
    $scope.showEdit = function(row){
    	 $scope.btnVisibility = {
	             	newBtn: true,
	             	modifyBtn: false
	         };
    	 $scope.pkVisibility = {
	         		epcId: true,
	         		factoryId: true,
	         		lineId: true
	     }
    	$scope.itemObj.epcid =row.epcid;
		$scope.itemObj.epcname = row.epcname;
		for(var i =0; i<$scope.factoryList.length; i++){
    		if($scope.factoryList[i].factoryCode === row.factoryCode){
    			$scope.selectedFactory = $scope.factoryList[i];
    			break;
    		}		
    	}
		for(var i =0; i<$scope.lineListFilter.length; i++){
    		if($scope.lineListFilter[i].lineId === row.lineId){
    			$scope.selectedLine = $scope.lineListFilter[i];
    			break;
    		}		
    	}
    }
    $scope.deleteEpcLine = function(){
    	var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'deleteEpcLine/'+$scope.itemObj.epcid + "/" +$scope.selectedFactory.factoryId + "/" + $scope.selectedLine.lineId ;
    	$http({
		    method: 'DELETE',
		    url: API_DELETE_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {

		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
					
			    	for(var i =0; i<$scope.epcLineList.length; i++){
			    		if($scope.epcLineList[i].factoryCode === $scope.selectedFactory.factoryCode && $scope.itemObj.epcid == $scope.epcLineList[i].epcid && $scope.selectedLine.lineId ===  $scope.epcLineList[i].lineId){
			    			$scope.epcLineList.splice(i, 1);
			    			break;
			    		}		
			    	}
			    	$scope.clean();
			    	$scope.factoryChanged();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    
    $scope.saveEpcLine = function(){
    	if($scope.selectedFactory && $scope.selectedLine){
    		$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
    		$scope.itemObj.lineId = $scope.selectedLine.lineId;
        	var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'newEpcLine';
    		$http({
    		    method: 'POST',
    		    url: API_NEW_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'},
    		    data: $scope.itemObj
    		}).then(function successCallback(response) {
    		    switch(response.data){
    		    
    			    case 0:{
    			    	showMessage("Failed!");
    			    }break;
    			    case 1:{
    			    	var obj = {		
    			    			epcid: $scope.itemObj.epcid,
    			    			epcname: $scope.itemObj.epcname,
    			    			factoryCode: $scope.selectedFactory.factoryCode,
    			    			factoryName: $scope.selectedFactory.factoryName,
    			    			lineId: $scope.selectedLine.lineId,
    			    			lineDesc: $scope.selectedLine.lineDesc
    			        };
    			    	$scope.clean();
    			    	$scope.epcLineList.push(obj);
    			    	showMessage("Success!");
    			    }break;
    			    case 2:{
    			    	showMessage("Epc Line existed.");
    			    }break;
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [newEpcLine]');
    		})
    	}
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});