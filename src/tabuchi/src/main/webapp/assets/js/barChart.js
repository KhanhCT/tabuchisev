// Directive for generic chart, pass in chart options
myApp.directive('ngSbsBarchart', function () {
    return {
        restrict: 'EA',
        template: '<div style="width:100%" ng-scope-element="sbsChart"></div>',
        scope: {
            columnData: '='
        },
        link: function (scope, element, attrs) {
        	angular.element(element).ready(function () {
        		var categoriesXAxis = [],
                seriesData = [],yAxisLst = [],
                okData = [], ngData = [];
        		var a = []
        		var	okDataObj ,	ngDataObj , chart;
        		scope.$watchCollection('columnData', function(newVal, oldVal){
        			if(newVal != undefined){
        				categoriesXAxis = [];
                        seriesData = [];
                        yAxisLst = [];
                        okData = [];
                        ngData = [];
                        okDataObj = newVal.okData.data;
                		ngDataObj = newVal.ngData.data;
                		
                		if(okDataObj.length > 0){
                			 console.log(ngDataObj.length)
                			 for(var i = 0; i < ngDataObj.length; i++){
                                 categoriesXAxis.push(okDataObj[i].line);
                                 okData.push(okDataObj[i].value);
                                 ngData.push(ngDataObj[i].value);
                             }
                		}
                                              
                        yAxisLst = [{
                            labels: {
                                style: {
                                    color: 'rgb(166,166,166)'
                                }
                            },
                            title: {
                                text: scope.columnData.ytitle,
                                style: {
                                    color: 'rgb(166,166,166)'
                                }
                            },
                            tickAmount: 8,
                            endOnTick: true,
                            maxPadding: 0,
                            minPadding: 0,
                            min: 0
                        }];
                       
                        seriesData = [{
                             name: newVal.okData.okName,
                             data: okData,
                             _colorIndex: 0

                         },{
                             name: newVal.ngData.ngName,
                             data: ngData,
                             _colorIndex: 1
 
                         }];
                        
                     
                        console.log(seriesData)
                		draw_chart();
        			}
        		});
        		function draw_chart() {
        			 chart = new Highcharts.Chart({
        				 chart: {
                             renderTo: scope.sbsChart[0],
                             type: 'column',
                             style: {
                                 fontFamily: 'Montserrat'
                             },
                             alignTicks: true,
                         },
                         title: {
                             text: scope.columnData.title
                         },
                         xAxis: {
                        	 categories: categoriesXAxis,
                             minorTickLength: 0,
                             tickLength: 0,
                             crosshair: true
                         },
                         yAxis: yAxisLst,
                         credits: {
                             enabled: false
                         },
                         tooltip: {
                        	 headerFormat: '<span style="font-size:10px">{point.key}</span><table style="width:250px">',
                             pointFormat: '<tr><td style="color:{series.color};padding:2px">{series.name}: </td>' +
                                 '<td style="padding:0"><b>{point.y}</b></td></tr>',
                             footerFormat: '</table>',
                             useHTML: true
                         },
                         plotOptions: {                   	
                             column: {
                                 pointPadding: 2,
                                 borderWidth: 0,
                                 pointWidth: 50,
//                                 zones: [{
//                                     color: 'red' // ... have the color blue.
//                                 }]
                             }
                         },
                         legend: {
                             enabled: false
                         },
                         series: seriesData
        			 });
        		}
        	})
        }
    };
});

myApp.directive('ngBarchart', function ($window) {
    return {
        restrict: 'EA',
        scope: {
            columnDataObject: '='
        },
        template: '<div class="container-highchart" ng-scope-element="columnGraph" style="width:100%"></div>',
        replace: true,
        link: function (scope, element, attrs) {
            angular.element(element).ready(function () {
                // =========== VARIABLE ===========
                var cloneToolTip = null,
                    chart = undefined,
                    redirectToPage = 'newPageAction',
                    SRC_IMG_CLOSE = 'closePopup.png',
                    SRC_IMG_PLUS = 'addNewPlus.png',
                    categoriesXAxis = [],
                    seriesData = [],
                    yAxisLst = [],
                    dataUser = [];
                // =========== INIT FAKE DATA ===========
                var	dataObject ;
                // =========== END INIT FAKE DATA ===========

                // =========== EFFECT INIT ===========
                effectEventOnChart();
                scope.$watchCollection('columnDataObject', function(newVal, oldVal){
                	if(newVal != undefined){
                		dataObject = newVal;
                    	categoriesXAxis = [],
                        seriesData = [],
                        yAxisLst = [],
                        dataUser = [];
                    	for(var i = 0; i < dataObject.data.length; i++){
                             categoriesXAxis.push(dataObject.data[i].time);
                             dataUser.push({
                                 y: dataObject.data[i].value,
                                 xCateId: dataObject.data[i].time,
                                 color: dataObject.data[i].color
                             })
                         }

                    	 yAxisLst = [{
                             labels: {
                                 style: {
                                     color: 'rgb(166,166,166)'
                                 }
                             },
                             title: {
                                 text: dataObject.ytitle,
                                 style: {
                                     color: 'rgb(166,166,166)'
                                 }
                             },
                             tickAmount: 8,
                             endOnTick: true,
                             maxPadding: 0,
                             minPadding: 0,
                             min: 0
                         }];
                    	 seriesData = [{
                             id: dataObject.name.toString().toLowerCase(),
                             name: dataObject.name,
                             type: 'column',
                             tooltip: {
                                 valueSuffix:' '+ dataObject.dataSuffix
                             },
                             data: dataUser,
                         }];
            			draw_chart();
                	}
        		});
                // ======= END VARIABLE AFTER INIT DATA =======

                // ======= CHART DRAW =======
                function draw_chart() {
                    chart = new Highcharts.Chart({
                        chart: {
                            renderTo: scope.columnGraph[0],
                            style: {
                                fontFamily: 'Montserrat'
                            },
                            alignTicks: true,
                        },
                        title: {
                            text: dataObject.title
                        },
                        xAxis: {
                            categories: categoriesXAxis,
                            type: 'datetime',
                            labels: {
                                format: '{value:%d/%m/%Y}',
                                style:{
                                    color: 'rgb(166,166,166)',
                                },
                            },
                            minorTickLength: 0,
                            tickLength: 0
                        },
                        yAxis: yAxisLst,
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            formatter: function() {
                                var htmlTemp = '<div class="name-tooltip-chart auto-effect"><span>'+Highcharts.dateFormat('%d/%m/%Y',new Date(this.x))+'</span></div>';
                                // THIS POINT INFORMATION
                                htmlTemp += '<div class="row-serie-chart">'
                                    + '<div class="circle-serie-color" style="background:'+ this.point.color +'"></div>'
                                    + '<div class="name-serie-chart"><span>'+ this.y + " products";                
                                return htmlTemp;
                            },
                            positioner: function(boxWidth, boxHeight, point){
                                var chart = this.chart,
                                    distance = this.distance,
                                    ret = {},
                                    h = point.h || 0,
                                    swapped,
                                    first = ['y', chart.chartHeight, boxHeight,
                                        point.plotY + chart.plotTop, chart.plotTop,
                                        chart.plotTop + chart.plotHeight
                                    ],
                                    second = ['x', chart.chartWidth, boxWidth,
                                        point.plotX + chart.plotLeft, chart.plotLeft,
                                        chart.plotLeft + chart.plotWidth
                                    ],
                                    preferFarSide = !this.followPointer && pick(point.ttBelow, !chart.inverted === !!point.negative),
                                    // NGANG
                                    firstDimension = function (dim,outerSize,innerSize,point,min,max) {
                                        var roomLeft = innerSize < point - distance,
                                            roomRight = point + distance + innerSize < outerSize,
                                            alignedLeft = point - distance - innerSize,
                                            alignedRight = point + distance;
                                        if (preferFarSide && roomRight) {
                                            ret[dim] = alignedRight;
                                        } else if (!preferFarSide && roomRight) {
                                            ret[dim] = alignedRight;
                                        } else if (roomLeft) {
                                            ret[dim] = Math.min(max - innerSize,alignedLeft - h < 0 ? alignedLeft : alignedLeft - h);
                                        } else if (roomRight) {
                                            ret[dim] = Math.max(min,alignedRight + h + innerSize > outerSize ?alignedRight :alignedRight + h);
                                        } else {
                                            return false;
                                        }
                                    },
                                    // CAO
                                    secondDimension = function (dim, outerSize, innerSize, point) {
                                        var retVal;
                                        if (point < distance || point > outerSize - distance) {
                                            retVal = false;
                                        } else if (point < innerSize) {
                                            ret[dim] = 1;
                                        } else if (point > outerSize - innerSize) {
                                            ret[dim] = outerSize - innerSize - 2;
                                        } else {
                                            ret[dim] = point - innerSize;
                                        }
                                        return retVal;
                                    },
                                    //Swap the dimensions chu y neu la cao thi chuyen thanh ngang
                                    swap = function (count) {
                                        var temp = first;
                                        first = second;
                                        second = temp;
                                        swapped = count;
                                    },
                                    run = function () {
                                        if (firstDimension.apply(0, second) !== false) {
                                            if (secondDimension.apply(0, first) === false && !swapped) {
                                                swap(true);
                                                run();
                                            }
                                        } else if (!swapped) {
                                            swap(true);
                                            run();
                                        } else {
                                            ret.x = ret.y = 0;
                                        }
                                    };
                                // Under these conditions, prefer the tooltip on the side of the point
                                if (chart.inverted || this.len > 1) {
                                    swap();
                                }
                                run();
                                return ret;
                            },
                            backgroundColor: 'rgba(255,255,255,0.97)',
                            shared: false,
                            shadow: true,
                            useHTML: true,
                            borderWidth: 1,
                            padding: 10,
                            borderColor: 'rgb(217,217,217)',
                            borderRadius: 0,
                            enabled: true,
                            followPointer: true,
                            snap: 0
                        },
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                pointPadding: 0,
                                groupPadding: 0.1,
                                borderWidth: 0,
                                stickyTracking: false,
                                turboThreshold : 100000,
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        series: seriesData
                    });
                }
            })
        }
    };
});
function effectEventOnChart() {
    bindTextAnimationChart('.name-tooltip-chart', 'span');
    bindTextAnimationChart('.action-type-name', 'span');
}

function bindTextAnimationChart(parentSelector, childSelector, isTextAlignRight) {
    $(parentSelector).unbind('mouseenter');
    $(parentSelector).unbind('mouseleave');
    $(document).on('mouseenter', parentSelector, function () {
        var child = $(this).find(childSelector);
        var parentWidth = $(this).width();
        $(child).hide();
        var childWidth = $(child).width();
        $(child).show();
        if (childWidth > parentWidth) {
            if($(this).closest('.wrap-name-tooltip').length > 0){
                $('.wrap-name-tooltip').width($('.wrap-name-tooltip').width())
            }
            if($(this).closest('.action-type-wrap').length > 0){
                $('.action-type-wrap').width($('.action-type-wrap').width())
            }
            repeatTextAnimation(child, childWidth, parentWidth);
        }
    });

    $(document).on('mouseleave', parentSelector, function () {
        var child = $(this).find(childSelector);
        $(child).clearQueue().stop();
        $(child).css('margin-left', 0);
        if (isTextAlignRight) {
            $(this).css({
                'text-align': 'right'
            });
        }
    });
}

function repeatTextAnimation(child, childWidth, parentWidth) {
    $(child).animate({
        marginLeft: (-childWidth) + 'px'
    }, 6000, "linear");

    $(child).animate({
        marginLeft: parentWidth + 'px'
    }, 0, function () {
        repeatTextAnimation(child, childWidth, parentWidth);
    });
}