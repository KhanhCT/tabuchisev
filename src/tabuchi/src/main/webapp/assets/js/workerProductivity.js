myApp.controller('workerCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
   $scope.columnDataObject = {
        dataSuffix: 'Unit Test',
        name: 'Monthly Factory Productivity',
        title: 'Line Productivity Chart',
        xtitle: '',
        ytitle: 'Number of ok products',
        data:[]
   };
   
   $scope.mesObj = undefined;
   function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
       }, 2000);
	}
   $scope.factoryList = [];
   $scope.selectedFactory = undefined;
   
   $scope.shiftList = [];
   $scope.selectedShift = undefined;

   
   $scope.selectedDate = moment(new Date(), 'DD/MM/YYYY').format('DD/MM/YYYY')
   $scope.lineData = [];
   $scope.today = new Date()
   $scope.curDate = new Date()
   
   $scope.lineListFilter = [];
   $scope.selectedLine = undefined;
   $scope.sbsData = {
		    title: 'Factory Productivity Chart',
	        xtitle: 'Line',
	        ytitle: 'Number of products',
	        okData:{
	        	okName: 'OK Products',
	 	        data:[]
	        },
	        ngData:{
	        	ngName: 'NG Products',
	        	data:[]
	        }
  }; 
   $scope.factoryChanged = function(){
   	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
   	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
   }
   
    $scope.initializeData = function(){
    	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
    	var API_FIND_ALL_SHIFT = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllShifts';
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	var API_GET_FAC_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyFactoryProductivity';
    	var momentFrom = moment($scope.today, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_FACTORY,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.factoryList = response.data;
			    	$scope.selectedFactory = $scope.factoryList[0];
			    	if($scope.selectedFactory !=undefined){	
			    		$http({
						    method: 'GET',
						    url: API_FIND_ALL_LINE,
						    headers: {'Content-Type': 'application/json; charset=utf-8'},
						    params: {factoryId: $scope.selectedFactory.factoryId}
						}).then(function successCallback(response) {
						    if(response.data){
						    	$scope.lineListFilter = response.data;
						    	$scope.selectedLine = $scope.lineListFilter[0];
						    	$http({
						            method: 'GET',
						            url: API_GET_LINE_DATA,
						            headers: {'Content-Type': 'application/json; charset=utf-8'},
						            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId: $scope.selectedFactory.factoryId}
						        }).then(function successCallback(response) {	
						        	 if(response.data && response.data !=''){
						             	var _lineData = response.data;
						             	$scope.columnDataObject.data = [];
						             	for(var i =0; i< response.data.length; i++){
						 	           		$scope.columnDataObject.data.push({
						 	                       time: _lineData[i].key ,
						 	                       color: '#91b6f2',
						 	                       value: _lineData[i].value,
						 	                   });
						             	}
						             }else{
						             	$scope.columnDataObject.data = [];
						             }
						        }, function errorCallback(response) {
									$scope.showLoaderAllPage = false;
						            console.log( + ' [ERROR Call getMonthyLineProductivity]');
						        });
						    	$http({
						            method: 'GET',
						            url: API_GET_FAC_DATA,
						            headers: {'Content-Type': 'application/json; charset=utf-8'},
						            params: {date: momentFrom,  factoryId: $scope.selectedFactory.factoryId}
						        }).then(function successCallback(response) {	
						            if(response.data && response.data.length > 0){
						            	var facData = response.data;
						            	$scope.sbsData = {
						            		    title: 'Line Productivity Chart',
						            	        xtitle: 'Line',
						            	        ytitle: 'Number of products',
						            	        okData:{
						            	        	okName: 'OK Products',
						            	 	        data:[]
						            	        },
						            	        ngData:{
						            	        	ngName: 'NG Products',
						            	        	data:[]
						            	        }
						            	}; 
						            	for(var i =0; i<facData.length; i++){
						                    $scope.sbsData.okData.data.push({
						                    	line: facData[i].key,
						             		   	color: '#91b6f2',
						             		   	value: facData[i].value
						                    });
						                	$scope.sbsData.ngData.data.push({
						                		line: facData[i].key,
						             		   	color: '#01b6f2',
						             		   	value: facData[i].otherValue
						                    });
						            	}   	
						            }else{
						            	$scope.sbsData.okData.data = [];
						            	$scope.sbsData.ngData.data = [];
						            }        
						        }, function errorCallback(response) {
									$scope.showLoaderAllPage = false;
						            console.log( + ' [ERROR Call getMonthyFactoryProductivity]');
						        });
						    }
						}, function errorCallback(response) {
						    console.log('ERROR CALL : [findAllFactory]');
						})
			    	}
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
		});
    }
    
    $scope.filter = function(){
    	if(!$scope.selectedLine) {
    		showMessage("Please select Line!");
    		return;
    	}
    	var momentFrom = moment($scope.selectedDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_FAC_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyFactoryProductivity';
    	$http({
            method: 'GET',
            url: API_GET_FAC_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom,  factoryId: $scope.selectedFactory.factoryId}
        }).then(function successCallback(response) {	
            if(response.data && response.data.length > 0){
            	var facData = response.data;
            	$scope.sbsData = {
            		    title: 'Factory Productivity Chart',
            	        xtitle: 'Line',
            	        ytitle: 'Number of products',
            	        okData:{
            	        	okName: 'OK Products',
            	 	        data:[]
            	        },
            	        ngData:{
            	        	ngName: 'NG Products',
            	        	data:[]
            	        }
            	}; 
            	for(var i =0; i<facData.length; i++){
                    $scope.sbsData.okData.data.push({
                    	line: facData[i].key,
             		   	color: '#91b6f2',
             		   	value: facData[i].value
                    });
                	$scope.sbsData.ngData.data.push({
                		line: facData[i].key,
             		   	color: '#01b6f2',
             		   	value: facData[i].otherValue
                    });
            	}  
            }else{
            	$scope.sbsData.okData.data = [];
            	$scope.sbsData.ngData.data = [];
            }
            
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyFactoryProductivity]');
        });
    }
    
    $scope.nextMonth = function(){
    	if(!$scope.selectedLine) {
    		showMessage("Please select Line!");
    		return;
    	}
    	$scope.curDate.setMonth($scope.curDate.getMonth() + 1);
    	var momentFrom = moment($scope.curDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId:$scope.selectedFactory.factoryId }
        }).then(function successCallback(response) {	
            if(response.data && response.data !=''){
            	var _lineData = response.data;
            	$scope.columnDataObject.data = [];
            	for(var i =0; i< _lineData.length; i++){

	           		$scope.columnDataObject.data.push({
	                       time: _lineData[i].key ,
	                       color: '#91b6f2',
	                       value: _lineData[i].value,
	                   });
            	}
            }else{
            	$scope.columnDataObject.data = [];
            }
            
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    	
    }
    
    
    $scope.thisMonth = function(){
    	if(!$scope.selectedLine) {
    		showMessage("Please select Line!");
    		return;
    	}
    	$scope.lineData = []
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	var momentFrom = moment($scope.today, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId: $scope.selectedFactory.factoryId}
        }).then(function successCallback(response) {	
            if(response.data && response.data !=''){
            	var _lineData = response.data;
            	$scope.columnDataObject.data = [];
            	for(var i =0; i< _lineData.length; i++){

	           		$scope.columnDataObject.data.push({
	                       time: _lineData[i].key ,
	                       color: '#91b6f2',
	                       value: _lineData[i].value,
	                   });
            	}
            }else{
            	$scope.columnDataObject.data = [];
            }
           
        	
            
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    	
    }
    $scope.lastMonth = function(){
    	if(!$scope.selectedLine) {
    		showMessage("Please select Line!");
    		return;
    	}
    	var date = new Date();
    	date.setMonth(date.getMonth() - 1);
    	var momentFrom = moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId: $scope.selectedFactory.factoryId}
        }).then(function successCallback(response) {	
        	if(response.data && response.data !=''){
            	var _lineData = response.data;
            	$scope.columnDataObject.data = [];
            	for(var i =0; i< _lineData.length; i++){
	           		$scope.columnDataObject.data.push({
	                       time: _lineData[i].key ,
	                       color: '#91b6f2',
	                       value: _lineData[i].value,
	                   });
            	}
            }else{
            	$scope.columnDataObject.data = [];
            }
            
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    }
    $scope.backMonth = function(){
    	$scope.curDate.setMonth($scope.curDate.getMonth() - 1);
    	var momentFrom = moment($scope.curDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId: $scope.selectedFactory.factoryId}
        }).then(function successCallback(response) {	
        	
            if(response.data && response.data !=''){
            	var _lineData = response.data;
            	$scope.columnDataObject.data = [];
            	for(var i =0; i< _lineData.length; i++){

	           		$scope.columnDataObject.data.push({
	                       time: _lineData[i].key ,
	                       color: '#91b6f2',
	                       value: _lineData[i].value,
	                   });
            	}
            }else{
            	$scope.columnDataObject.data = [];
            }
            
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    }
}]);
