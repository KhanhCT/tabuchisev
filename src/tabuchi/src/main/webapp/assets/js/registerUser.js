myApp.controller('registerUserCtrl', ['$scope', '$http', '$q' ,'$timeout',
    function($scope, $http, $q, $timeout) {
	$scope.selectedRole = undefined;
    $scope.userReg = undefined;
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = true;
    $scope.selectedRole = undefined;
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    $scope.initializeData = function (){
        $scope.showLoaderAllPage = true;
        var URL_GET_ALL_ROLES = 'getAllUserRoles';
	  	var URL_API_GET_ALL_ROLES =  contextPath + CONTSTANT_SERVICE_URI.URI_USER + URL_GET_ALL_ROLES;
	  	$http({
            method: 'GET',
            url: URL_API_GET_ALL_ROLES,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        }).then(function successCallback(response) {	
            if(response.data){
            	$scope.userRolesList = response.data;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log(URL_API_GET_ALL_ROLES + ' [ERROR]');
        });
        $scope.userReg = {
            userName: '',
            password: '',
            customerId: '',
            userRoleId: 0,
            address: '',
            phoneNum: '',
            email: ''
        };
        
        $timeout(function () {
            $scope.showLoaderAllPage = false;
        }, 500);
    };

    $scope.registerUser = function() {
        $scope.showLoaderAllPage = true;
        $scope.userReg.userRoleId = $scope.selectedRole.userRoleId;
        var URL_ADD_USER = 'newUser';
        var URI_API_ADD_USER = contextPath + CONTSTANT_SERVICE_URI.URI_USER  + URL_ADD_USER;
        $http({
            method: 'POST',
            url: URI_API_ADD_USER,
            data:  $scope.userReg,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
        }).then(function successCallback(response) {
            switch(response.data){
            case 0:{
            	showMessage("Failed to register user :(. Try again!");
            }break;
            case 1:{
            	$scope.userReg = {
                        userName: '',
                        password: '',
                        customerId: '',
                        userRoleId: 0,
                        address: '',
                        phoneNum: '',
                        email: ''
                    };
            	showMessage("Register user successfully!");
            	
            }break;
            }
            $scope.showLoaderAllPage = false;
        }, function errorCallback(response) {
            alert('Register User Error');
            $scope.showLoaderAllPage = false;
        });
    }
}]);