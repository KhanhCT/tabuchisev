myApp.controller('factoryMasterCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.lineList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
   
    
    $scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    
    $scope.itemObj = {  
    	factoryId: 0,
    	factoryCode: '',
    	lineProdCode: '',
    	lineTagCode: '',
    	wplaceQty: 1
    }
    
    $scope.factoryKeyTable = [{
        code : 'factoryId',
        name : 'FactoryID',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'factoryCode',
        name : 'Factory Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'factoryName',
        name : 'Factory Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'shiftQty',
        name : 'Product Name',
        iClass : "text-center"
    },{
        code : 'status',
        name : 'Status',
        iClass : "text-center"
    }];
    
    $scope.lineKeyTable = [{
        code : 'factoryId',
        name : 'Factory ID',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineId',
        name : 'Line ID',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineProdId',
        name : 'Line Product ID',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineTagCode',
        name : 'Product Name',
        iClass : "text-center"
    },{
        code : 'lineDesc',
        name : 'Line Name',
        iClass : "text-center"
    },{
        code : 'wplaceQty',
        name : 'Work Place Qty',
        iClass : "text-center"
    },{
        code : 'status',
        name : 'Status',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.pkVisibility = {
        		factoryId: false,
        		processId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	showMessage('Initalizing.....');
    	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
        $q.all([
 			$http({
 			    method: 'GET',
 			    url: API_FIND_ALL_FACTORY,
 			    headers: {'Content-Type': 'application/json; charset=utf-8'}
 			}).then(function successCallback(response) {
 			    if(response.data){
 			    	$scope.factoryList = response.data;
 			    	$scope.selectedFactory = $scope.factoryList[0];
 			    	$http({
 					    method: 'GET',
 					    url: API_FIND_ALL_LINE,
 					    headers: {'Content-Type': 'application/json; charset=utf-8'},
 					    params: {factoryId: $scope.selectedFactory.factoryId}
 					}).then(function successCallback(response) {
 					    if(response.data){
 					    	$scope.lineListFilter = response.data;
 					    }
 					}, function errorCallback(response) {
 					    console.log('ERROR CALL : [findAllFactory]');
 					})
 			    }
 			}, function errorCallback(response) {
 			    console.log('ERROR CALL : [findAllFactory]');
 			})			
         ]).then (function () {
         	
         })
        
    };
    $scope.factoryChanged = function(){
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId }
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllLine]');
			})
    }
    $scope.clean = function(){
    	$scope.itemObj = {  
    	    	factoryId: 0,
    	    	factoryCode: '',
    	    	lineProdCode: '',
    	    	lineTagCode: '',
    	    	wplaceQty: 1
    	}
    	$scope.pkVisibility = {
    			factoryId: false,
        		processId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    }
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});