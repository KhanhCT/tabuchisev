myApp.controller('processBOMCtrl', ['$scope', 'NgTableParams',  'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel,  $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    
    $scope.skuUnitList = [];
    $scope.selectedSKUUnit = undefined;
    
    $scope.usageUnitList = [];
    $scope.selectedUsageUnit = undefined;
    
    $scope.processIdList = [];
    $scope.selectedProcess = undefined;
    
    $scope.productBOMList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.pkVisibility = {
    		lineProdCode: false,
    		materialCode: false
    }
    $scope.btnVisibility = {
        	newBtn: false,
        	modifyBtn: true
      };
    
    $scope.listKeyTable = [{
        code : 'lineProdCode',
        name : 'Product Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'processIdx',
        name : 'Process ID',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'materialCode',
        name : 'Material Code',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'lineSkuQty',
        name : 'SKU Qty',
        iClass : "text-center"
    },{
        code : 'lineSkuUnitName',
        name : 'SKU Unit',
        iClass : "text-center"
    },{
        code : 'mateUsageQty',
        name : 'Usage Qty',
        iClass : "text-center"
    },{
        code : 'mateUsageName',
        name : 'Usage Unit',
        iClass : "text-center"
    }, {
        code : 'mateSkuQty',
        name : 'Mate SKU Qty',
        iClass : "text-center"
    },];
    
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		var URI_UPLOAD_FILE = 'uploadProcessProdBOM';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.productBOMList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.itemObj = {
    		lineProdCode: '',
    		materialCode: '',
    		processIdx: $scope.processIdList[0],
    		materialName: '',
    		lineSkuunitId: 0,
    		lineSkuQty: 1,
    		mateUsageUnitId : 0,
    		mateUsageQty: 1,
    		mateSkuQty: 1
    	}
    	showMessage('Initalizing.....');
    	var API_GET_USAGE_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllUsageUnits';
    	var API_GET_SKU_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllSkuUnits';
    	var API_GET_ALL_BOM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllProcessProductBOM';
    	var API_GET_ALL_PROCESS = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcesses';

    	$q.all([
			$http({
			    method: 'GET',
			    url: API_GET_SKU_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.skuUnitList  = response.data;
			        $scope.selectedSKUUnit = $scope.skuUnitList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_USAGE_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.usageUnitList  = response.data;
			        $scope.selectedUsageUnit = $scope.usageUnitList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllUsageUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_ALL_PROCESS,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {processCode :'N/G'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.processIdList = response.data;
			    	$scope.selectedProcess = $scope.processIdList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_ALL_BOM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        var _data  = response.data;
			        for(var i=0; i< _data.length; i++){
			        	var itemObj = _data[i];
			        	var obj = {		
				    			lineProdCode: itemObj.lineProdCode,
				        		materialCode: itemObj.materialCode,
				        		processIdx: itemObj.processIdx,
				        		lineSkuQty: itemObj.lineSkuQty,
				        		lineSkuunitId: itemObj.lineSkuunitId,
				        		mateUsageQty: itemObj.mateUsageQty,
				        		mateUsageUnitId: itemObj.mateUsageUnitId,
				        		mateSkuQty: itemObj.mateSkuQty
				        };
			        	for(var j =0; j< $scope.skuUnitList.length; j++){
			        		if($scope.skuUnitList[j].unitId == itemObj.lineSkuunitId)
			        		{
			        			obj.lineSkuUnitName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}
			        	for(var j =0; j< $scope.usageUnitList.length; j++){
			        		if($scope.usageUnitList[j].unitId == itemObj.mateUsageUnitId)
			        		{
			        			obj.mateUsageName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}  
			        	for(var j =0; j< $scope.processIdList.length; j++){
			        		if($scope.processIdList[j].processIdx == itemObj.processIdx)
			        		{
			        			obj.processCode = $scope.processIdList[j].processCode; 
			        			break;
			        		}
			        	} 
			        	$scope.productBOMList.push(obj);
			        }
			        
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllUsageUnits]');
			}),
			
        ]).then (function () {
        	
        })
        
    };
    $scope.exportBOM = function(){
        window.location = contextPath + '/exportData/bom'
    	showMessage('Exporting.....');
    }
    $scope.showEdit = function(obj){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	
    	$scope.pkVisibility.lineProdCode = true;
    	$scope.pkVisibility.materialCode= true;
    	
    	$scope.itemObj.lineProdCode = obj.lineProdCode; 	
		$scope.itemObj.materialCode = obj.materialCode;
		$scope.itemObj.processIdx = obj.processIdx;
		$scope.itemObj.lineSkuQty = obj.lineSkuQty;
		$scope.itemObj.lineSkuunitId = obj.lineSkuunitId;
		$scope.itemObj.mateUsageQty = obj.mateUsageQty;
		$scope.itemObj.mateUsageUnitId = obj.mateUsageUnitId;
		$scope.itemObj.mateSkuQty = obj.mateSkuQty;
    	
    	for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == $scope.itemObj.lineSkuunitId)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    	for(var i =0; i< $scope.usageUnitList.length; i++){
    		if($scope.usageUnitList[i].unitId == $scope.itemObj.mateUsageUnitId)
    		{
    			$scope.selectedUsageUnit = $scope.usageUnitList[i]; 
    			break;
    		}
    	} 
    	for(var j =0; j< $scope.processIdList.length; j++){
    		if($scope.processIdList[j].processIdx == $scope.itemObj.processIdx)
    		{
    			$scope.selectedProcess = $scope.processIdList[j];
    			break;
    		}
    	} 
    }
    $scope.showDelete = function(obj){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	
    	$scope.pkVisibility.lineProdCode = true;
    	$scope.pkVisibility.materialCode= true;
    	
    	$scope.itemObj.lineProdCode = obj.lineProdCode;
		$scope.itemObj.materialCode = obj.materialCode;
		$scope.itemObj.processIdx = obj.processIdx;
		$scope.itemObj.lineSkuQty = obj.lineSkuQty;
		$scope.itemObj.lineSkuunitId = obj.lineSkuunitId;
		$scope.itemObj.mateUsageQty = obj.mateUsageQty;
		$scope.itemObj.mateUsageUnitId = obj.mateUsageUnitId;
		$scope.itemObj.mateSkuQty = obj.mateSkuQty;
    	
    	for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == $scope.itemObj.lineSkuunitId)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    	for(var i =0; i< $scope.usageUnitList.length; i++){
    		if($scope.usageUnitList[i].unitId == $scope.itemObj.mateUsageUnitId)
    		{
    			$scope.selectedUsageUnit = $scope.usageUnitList[i]; 
    			break;
    		}
    	} 
    	for(var j =0; j< $scope.processIdList.length; j++){
    		if($scope.processIdList[j].processIdx == $scope.itemObj.processIdx)
    		{
    			$scope.selectedProcess = $scope.processIdList[j];
    			break;
    		}
    	} 
    }
    
    $scope.deleteProductBOM = function(){
    	var API_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'deleteProcessProductBOM/' +  $scope.itemObj.lineProdCode + "/" +  $scope.itemObj.materialCode + "/" + $scope.selectedProcess.processIdx;
    	if($scope.itemObj.lineProdCode!='' && $scope.itemObj.materialCode!='' && $scope.itemObj.processIdx!='ALL')
		{  		
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_ITEM,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    case 1:{
    			    	for(var i = 0; i< $scope.productBOMList.length; i++)
    			    	{
    		    			
    			    		if($scope.productBOMList[i].lineProdCode == $scope.itemObj.lineProdCode && $scope.productBOMList[i].materialCode == $scope.itemObj.materialCode && $scope.productBOMList[i].processIdx == $scope.itemObj.processIdx ){
    			    			$scope.productBOMList.splice(i, 1); 
    			    			break;
    			    		}
    			    	}
    			    	$scope.clean();
    			    	showMessage("Success!");
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteLineProduct]');
    		})
		}else{
			showMessage("Line Product Code Or Material Code is not null");
		}
    }
    $scope.findProductBOM = function(){
    	if($scope.itemObj.lineProdCode!='' && $scope.itemObj.processIdx !='ALL'){
    		var API_FIND_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcessProductBOM';
    		$http({
			    method: 'GET',
			    url: API_FIND_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {lineProdCode: $scope.itemObj.lineProdCode, processIdx: $scope.selectedProcess.processIdx}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.materialCode= true;			    	
			    	var _data  = response.data;
			    	$scope.productBOMList = [];
			        for(var i=0; i< _data.length; i++){
			        	var itemObj = _data[i];
			        	var obj = {		
				    			lineProdCode: itemObj.lineProdCode,
				        		materialCode: itemObj.materialCode,
				        		processIdx: itemObj.processIdx,
				        		lineSkuQty: itemObj.lineSkuQty,
				        		lineSkuunitId: itemObj.lineSkuunitId,
				        		mateUsageQty: itemObj.mateUsageQty,
				        		mateUsageUnitId: itemObj.mateUsageUnitId,
				        		mateSkuQty: itemObj.mateSkuQty
				        };
			        	for(var j =0; j< $scope.skuUnitList.length; j++){
			        		if($scope.skuUnitList[j].unitId == itemObj.lineSkuunitId)
			        		{
			        			obj.lineSkuUnitName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}
			        	for(var j =0; j< $scope.usageUnitList.length; j++){
			        		if($scope.usageUnitList[j].unitId == itemObj.mateUsageUnitId)
			        		{
			        			obj.mateUsageName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}  	
			        	for(var j =0; j< $scope.processIdList.length; j++){
			        		if($scope.processIdList[j].processIdx == itemObj.processIdx)
			        		{
			        			obj.processCode = $scope.processIdList[j].processCode; 
			        			break;
			        		}
			        	} 
			        	$scope.productBOMList.push(obj);
			         }
			    }else{
			    	showMessage("Not found Code: " + $scope.itemObj.lineProdCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findLineProductBOM]');
			})
    	}else if ($scope.itemObj.lineProdCode!='' && $scope.itemObj.processIdx=='ALL'){
    		var API_FIND_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findProcessProductBOM';
    		$http({
			    method: 'GET',
			    url: API_FIND_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {lineProdCode: $scope.itemObj.lineProdCode, processIdx: 0}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.materialCode= true;			    	
			    	var _data  = response.data;
			    	$scope.productBOMList = [];
			        for(var i=0; i< _data.length; i++){
			        	var itemObj = _data[i];
			        	var obj = {		
				    			lineProdCode: itemObj.lineProdCode,
				        		materialCode: itemObj.materialCode,
				        		processIdx: itemObj.processIdx,
				        		lineSkuQty: itemObj.lineSkuQty,
				        		lineSkuunitId: itemObj.lineSkuunitId,
				        		mateUsageQty: itemObj.mateUsageQty,
				        		mateUsageUnitId: itemObj.mateUsageUnitId,
				        		mateSkuQty: itemObj.mateSkuQty
				        };
			        	for(var j =0; j< $scope.skuUnitList.length; j++){
			        		if($scope.skuUnitList[j].unitId == itemObj.lineSkuunitId)
			        		{
			        			obj.lineSkuUnitName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}
			        	for(var j =0; j< $scope.usageUnitList.length; j++){
			        		if($scope.usageUnitList[j].unitId == itemObj.mateUsageUnitId)
			        		{
			        			obj.mateUsageName = $scope.skuUnitList[j].unitName; 
			        			break;
			        		}
			        	}  	
			        	for(var j =0; j< $scope.processIdList.length; j++){
			        		if($scope.processIdList[j].processIdx == itemObj.processIdx)
			        		{
			        			obj.processCode = $scope.processIdList[j].processCode; 
			        			break;
			        		}
			        	} 
			        	$scope.productBOMList.push(obj);
			         }
			    }else{
			    	showMessage("Not found Code: " + $scope.itemObj.lineProdCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findLineProductBOM]');
			})
		}else{
			showMessage("Not found");
		}
    }
    $scope.modifyProductBOM = function(){
    	var UPDATE_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'updateProcessProducBOM';
    	$http({
		    method: 'POST',
		    url: UPDATE_NEW_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed");
			    }break;
			    case 1:{
			    	for(var i =0; i< $scope.productBOMList.length; i++){
			    		if($scope.productBOMList[i].lineProdCode == $scope.itemObj.lineProdCode && $scope.productBOMList[i].materialCode == $scope.itemObj.materialCode && $scope.productBOMList[i].processIdx == $scope.itemObj.processIdx){
			    			$scope.productBOMList[i].lineProdCode= $scope.itemObj.lineProdCode;
			    			$scope.productBOMList[i].materialCode= $scope.itemObj.materialCode;
			    			$scope.productBOMList[i].processCode= $scope.itemObj.processCode;
			    			$scope.productBOMList[i].processIdx= $scope.itemObj.processIdx;
			    			$scope.productBOMList[i].lineSkuQty= $scope.itemObj.lineSkuQty;
			    			$scope.productBOMList[i].lineSkuUnitName= $scope.selectedSKUUnit.unitName;
			    			$scope.productBOMList[i].lineSkuunitId=$scope.selectedSKUUnit.unitId;
			    			$scope.productBOMList[i].mateUsageQty= $scope.itemObj.mateUsageQty;
			    			$scope.productBOMList[i].mateUsageUniName= $scope.selectedUsageUnit.unitName;
			    			$scope.productBOMList[i].mateUsageUnitId= $scope.selectedUsageUnit.unitId;
			    			$scope.productBOMList[i].mateSkuQty= $scope.itemObj.mateSkuQty;
			    			break;
			    		}
			    	}
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newLineProduct]');
		})
    }
    
    $scope.saveProductBOM = function(){
    	if($scope.itemObj.lineProdCode!='' && $scope.itemObj.materialCode!=''){
    		var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'newProcessProductBOM';
    		$scope.itemObj.lineSkuunitId = $scope.selectedSKUUnit.unitId;
    		$scope.itemObj.mateUsageUnitId = $scope.selectedUsageUnit.unitId;
    		$scope.itemObj.processIdx = $scope.selectedProcess.processIdx;
    		$http({
			    method: 'POST',
			    url: API_NEW_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Line Product Code Or Material Code is not available!");
				    }break;
				    case 1:{
				    	var obj = {		
				    			lineProdCode: $scope.itemObj.lineProdCode,
				        		materialCode: $scope.itemObj.materialCode,
				        		processIdx: $scope.selectedProcess.processIdx, 
				        		processCode: $scope.selectedProcess.processCode,
				        		lineSkuQty: $scope.itemObj.lineSkuQty,
				        		processIdx: $scope.processIdList[0].processIdx,
				        		lineSkuUnitName: $scope.selectedSKUUnit.unitName,
				        		lineSkuunitId:$scope.selectedSKUUnit.unitId,
				        		mateUsageQty: $scope.itemObj.mateUsageQty,
				        		mateUsageName: $scope.selectedUsageUnit.unitName,
				        		mateUsageUnitId: $scope.selectedUsageUnit.unitId,
				        		mateSkuQty: $scope.itemObj.mateSkuQty
				        };
				    	$scope.productBOMList.push(obj);
			
				    	showMessage("Success!");
				    	$scope.clean();
				    }break;
				    case 2:{
				    	showMessage("Item existed.");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newLineProductBOM]');
			})
    	}else{
			showMessage("Line Product Code Or Material Code is not null");
		}
    };
    $scope.clean = function(){
    	$scope.itemObj = {
        		lineProdCode: '',
        		materialCode: '',
        		processIdx: 0,
        		materialName: '',
        		lineSkuunitId: 0,
        		lineSkuQty: 1,
        		mateUsageUnitId : 0,
        		mateUsageQty: 1,
        		mateSkuQty: 1
        };
    	$scope.selectedSKUUnit = $scope.skuUnitList[0];
    	$scope.selectedUsageUnit = $scope.usageUnitList[0];
    	$scope.selectedProcess = $scope.processIdList[0];
    	$scope.pkVisibility = {
    	    lineProdCode: false,
    	    materialCode: false
    	}
    	$scope.btnVisibility = {
    	    newBtn: false,
    	    modifyBtn: true
    	};
    };
    
    
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});