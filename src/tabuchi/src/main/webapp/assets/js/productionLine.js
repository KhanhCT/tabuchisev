myApp.controller('productionLineCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q','Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    $scope.lineList = [];
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
   
    
    $scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    
    var noneLine = {lineDesc: '----[None]----'};
    $scope.itemObj = {  
    	factoryId: 0,
    	lineId: 0,
    	factoryCode: '',
    	lineTagCode: '',
    	lineDesc: '',
    	wplaceQty: 1,
    	disabled: false
    }
    
    $scope.listKeyTable = [{
        code : 'factoryCode',
        name : 'Factory Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'factoryName',
        name : 'Factory Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'lineTagCode',
        name : 'Tag Code',
        iClass : "text-center"
    },{
        code : 'lineDesc',
        name : 'Line Name',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'wplaceQty',
        name : 'Work Place Qty',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + 'uploadProductionLine';
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				$scope.lineList = resp.data;
				showMessage("Success");
			}
			$timeout(function(){
				$scope.showLoaderAllPage = false;
				
				showMessage("Failed" );
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	}
    $scope.showEdit = function(obj){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.lineProdCode = true;
    	$scope.pkVisibility.processId= true;

    	for(var i=0; i<$scope.factoryList.length; i++){
    		if(obj.factoryId == $scope.factoryList[i].factoryId){
    			$scope.selectedFactory = $scope.factoryList[i];
    			break;
    		}
    	}
    	for(var i=0; i<$scope.lineListFilter.length; i++){
    		if(obj.lineId == $scope.lineListFilter[i].lineId){
    			$scope.selectedLine = $scope.lineListFilter[i];
    			break;
    		}
    	}
    	$scope.itemObj = obj;

    }
	var API_FIND_ALL_PROD_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProdLine';
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.pkVisibility = {
        		factoryId: false,
        		processId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    	showMessage('Initalizing.....');
    	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
        $q.all([
 			
 			$http({
 			    method: 'GET',
 			    url: API_FIND_ALL_FACTORY,
 			    headers: {'Content-Type': 'application/json; charset=utf-8'}
 			}).then(function successCallback(response) {
 			    if(response.data){
 			    	$scope.factoryList = response.data;
 			    	$scope.selectedFactory = $scope.factoryList[0];
 		    		$http({
 					    method: 'GET',
 					    url: API_FIND_ALL_PROD_LINE,
 					    headers: {'Content-Type': 'application/json; charset=utf-8'},
 					    params: {factoryId: $scope.selectedFactory.factoryId, lineId: 0}
 					}).then(function successCallback(response) {
 					    if(response.data){
 					    	$scope.lineList = response.data;
 					    	
 					    }else{
 					    	showMessage("Not found line: " + $scope.selectedLine.lineDesc );
 					    }
 					}, function errorCallback(response) {
 					    console.log('ERROR CALL : [findLine]');
 					})
 			    	$http({
 					    method: 'GET',
 					    url: API_FIND_ALL_LINE,
 					    headers: {'Content-Type': 'application/json; charset=utf-8'},
 					    params: {factoryId: $scope.selectedFactory.factoryId }
 					}).then(function successCallback(response) {
 					    if(response.data){
 					    	$scope.lineListFilter = response.data;
 					    	$scope.lineListFilter.unshift(noneLine);
 					    	$scope.selectedLine = $scope.lineListFilter[0];
 					    }
 					}, function errorCallback(response) {
 					    console.log('ERROR CALL : [findAllFactory]');
 					})
 			    }
 			}, function errorCallback(response) {
 			    console.log('ERROR CALL : [findAllFactory]');
 			})			
         ]).then (function () {
         	
         })
        
    };
    $scope.refresh = function(){
    	$http({
		    method: 'GET',
		    url: API_FIND_ALL_PROD_LINE,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    params: {factoryId: $scope.selectedFactory.factoryId, lineId: 0}
		}).then(function successCallback(response) {
		    if(response.data){
		    	$scope.lineList = response.data;
		    	
		    }else{
		    	showMessage("Not found line: " + $scope.selectedLine.lineDesc );
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findLine]');
		})
    }
    $scope.lineChanged = function(){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.factoryId = true;
    	
    	$scope.itemObj = $scope.selectedLine;
    }
    $scope.factoryChanged = function(){
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId }
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
    }
    $scope.clean = function(){
    	$scope.itemObj = {  
    			factoryId: 0,
    	    	lineId: 0,
    	    	factoryCode: '',
    	    	lineTagCode: '',
    	    	lineDesc: '',
    	    	wplaceQty: 1,
    	    	disabled: false
    	}
    	$scope.pkVisibility = {
    			factoryId: false,
        		processId: false
        }
        $scope.btnVisibility = {
            	newBtn: false,
            	modifyBtn: true
        };
    }
    
    $scope.findProdLine = function(){
    	$scope.clean();
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllProdLine';
    	if($scope.selectedLine.lineDesc == noneLine.lineDesc){
    		$http({
				    method: 'GET',
				    url: API_FIND_ALL_LINE,
				    headers: {'Content-Type': 'application/json; charset=utf-8'},
				    params: {factoryId: $scope.selectedFactory.factoryId, lineId:0}
				}).then(function successCallback(response) {
				    if(response.data){
				    	$scope.lineList = response.data;
				    	
				    }else{
				    	showMessage("Not found" );
				    }
				}, function errorCallback(response) {
				    console.log('ERROR CALL : [findLine]');
				})
    	}else{
    		
    		$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId, lineId:$scope.selectedLine.lineId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.lineProdCode = true;
			    	$scope.pkVisibility.processId= true;
			    	$scope.lineList = response.data;			    	
			    }else{
			    	showMessage("Not found line: " + $scope.selectedLine.lineDesc );
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findLine]');
			})
    	}
    }
    $scope.mofidyProdLine = function(){
    	var UPDATE_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'updateLine';
    	$http({
		    method: 'POST',
		    url: UPDATE_UPDATE_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	$scope.refresh();
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    $scope.deleteLine = function(){
    	var UPDATE_DELETE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'deleteLine/'+ $scope.selectedFactory.factoryId + "/" +$scope.selectedLine.lineId;
    	$http({
		    method: 'DELETE',
		    url: UPDATE_DELETE_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	$scope.refresh();
			    	$scope.clean();
			    	$scope.factoryChanged();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Failed");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    $scope.saveProdLine = function(){
    	var API_NEW_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'newLine';
    	$scope.itemObj.factoryId = $scope.selectedFactory.factoryId;
		$http({
		    method: 'POST',
		    url: API_NEW_ITEM,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
		    data: $scope.itemObj
		}).then(function successCallback(response) {
		    switch(response.data){
			    case 0:{
			    	showMessage("Failed!");
			    }break;
			    case 1:{
			    	$scope.refresh();
			    	$scope.clean();
			    	showMessage("Success!");
			    }break;
			    case 2:{
			    	showMessage("Line existed.");
			    }break;
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [newProcess]');
		})
    }
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});