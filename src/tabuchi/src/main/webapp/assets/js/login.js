var myApp = angular.module('loginApplication',[])
    .controller('loginController', ['$scope', '$http' , '$timeout', function($scope, $http, $timeout){
    $scope.showLoaderLogin = false;
    $scope.showPreload = function(){
        $scope.showLoaderLogin = true;
        var input = $('.validate-input .input-page');
        var check = true;
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check =false;
            }
        }
        if(check == false){
            $timeout(function () {
                $scope.showLoaderLogin = false;
            }, 1000);
        }
        return check;
    };
    angular.element(document).ready(function(){
        (function ($) {
            "use strict";
            $('.input-page').each(function(){
                $(this).on('blur', function(){
                    if($(this).val().trim() != "") {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            });
            $scope.$watch('aisUsernameLogin', function(){
                if($scope.aisUsernameLogin != '' && $scope.aisUsernameLogin){
                    $('#ais-username-login').addClass('has-val');
                }
            });
            $scope.$watch('aisPasswordLogin', function(){
                if($scope.aisPasswordLogin != '' && $scope.aisPasswordLogin){
                    $('#ais-password-login').addClass('has-val');
                }
            })
            var input = $('.validate-input .input-page');

            $('.validate-form').on('submit',function(){
                var check = true;

                for(var i=0; i<input.length; i++) {
                    if(validate(input[i]) == false){
                        showValidate(input[i]);
                        check=false;
                    }
                }

                return check;
            });
            $(document).on('click','.validate-form .input-page', function(){
                $('.wrap-error-message').hide();
            });

            $('.validate-form .input-page').each(function(){
                $(this).focus(function(){
                    hideValidate(this);
                });
            });
        })(jQuery);
    });
    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }
    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        } else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }
}]);
