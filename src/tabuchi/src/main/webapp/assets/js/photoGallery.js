myApp.controller('photoGalleryCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
	$scope.sliders = [1,2,3,4,5]
	$scope.selectedDate = moment(new Date(), 'DD/MM/YYYY').format('DD/MM/YYYY')
	$scope.factoryList = [];
    $scope.selectedFactory = undefined;
    
    $scope.lineListFilter = [];
    $scope.selectedLine = undefined;
    
    $scope.photoList = [];
  
    
    $scope.shiftList = [];
    $scope.selecteShift = undefined;
    
    $scope.positionList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    $scope.selectedPos = $scope.positionList[0]
    
    var sliderImageAuto = undefined;
    
	$scope.initializeData = function(){
		var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
		var API_FIND_ALL_SHIFT = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllShifts';
		 $q.all([
			 $http({
				    method: 'GET',
				    url: API_FIND_ALL_FACTORY,
				    headers: {'Content-Type': 'application/json; charset=utf-8'}
				}).then(function successCallback(response) {
				    if(response.data){
				    	$scope.factoryList = response.data;
				    	$scope.selectedFactory = $scope.factoryList[0];
				    	if($scope.selectedFactory !=undefined){
				    		$scope.factoryChanged();
				    		$http({
								    method: 'GET',
								    url: API_FIND_ALL_SHIFT,
								    headers: {'Content-Type': 'application/json; charset=utf-8'},
								    params: {factoryId: $scope.selectedFactory.factoryId}
								}).then(function successCallback(response) {
								    if(response.data){
								    	$scope.shiftList = response.data;
								    	$scope.selectedShift = $scope.shiftList[0];
								    }
								}, function errorCallback(response) {
								    console.log('ERROR CALL : [findAllShifts]');
							  })
				    	}
				    	if($scope.selectedLine != undefined){
				    		$scope.find();
				    	}
				    }
				}, function errorCallback(response) {
				    console.log('ERROR CALL : [findAllFactory]');
			  }),
	        ]).then (function () {
	        	
	        })	
	}
	$scope.find = function(){
		var API_FIND_ALL_PHOTOS = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllPhotos';
		var workingDate = moment($scope.selectedDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
		$http({
		    method: 'GET',
		    url: API_FIND_ALL_PHOTOS,
		    headers: {'Content-Type': 'application/json; charset=utf-8'},
	  		params: {workingDate: workingDate, factoryId: $scope.selectedFactory.factoryId, lineId: $scope.selectedLine.lineId, shiftId: $scope.selectedShift.shiftId, position:$scope.selectedPos}
		}).then(function successCallback(response) {
			$scope.photoList = []
		    if(response.data && response.data.length > 0){
		    	var tmp =  response.data;
		    	console.log(tmp);
		    	for(var i =0; i< tmp.length; i++){
		    		$scope.photoList.push({
		    			url: contextPath + '/uploads/' +  tmp[i].url,
		    			captureDate: tmp[i].captureDate
		    		})
		    	}
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllPhotos]');
		})
	}
	$scope.factoryChanged = function(){
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllLine]');
			})
    }
	$scope.changeZoomImage = function (slider, $event) {
	        $event.preventDefault();
	        $event.stopPropagation();
	        $scope.imageZoomURL = slider.url;
	        $scope.imageZoomTitle = slider.captureDate;
	        $scope.showImageZoom = true;
	        heightImage = undefined;
	        widthImage = undefined;
	        scaleImage = undefined;
	        $scope.styleZoom = undefined;
	        $scope.zoomPercent = 100;
	        if(sliderImageAuto){
	            sliderImageAuto.stopAuto();
	        }
	    };
	    $scope.hidePreviewZoom = function ($event){
	        $event.preventDefault();
	        $event.stopPropagation();
	        $scope.showImageZoom = false;
	        if(sliderImageAuto){
	            sliderImageAuto.startAuto();
	        }
	    };
	    var heightImage = undefined;
	    var widthImage = undefined;
	    var scaleImage = undefined;
	    $scope.zoomPercent = 100;
	    $scope.zoomIn = function ($event) {
	        $event.preventDefault();
	        $event.stopPropagation();
	        if(!heightImage || !widthImage || !scaleImage){
	            widthImage = $('.cp-image-container').width();
	            heightImage = $('.cp-image-container').height();
	            scaleImage = widthImage / heightImage;
	            $scope.zoomPercent = 115;
	            $scope.styleZoom = {
	                width: (widthImage * $scope.zoomPercent / 100) +'px',
	                height: (heightImage * $scope.zoomPercent / 100) + 'px'
	            }
	        } else {
	            if($scope.zoomPercent <= 185){
	                $scope.zoomPercent = $scope.zoomPercent + 15;
	            }
	            $scope.styleZoom = {
	                width: (widthImage * $scope.zoomPercent / 100) +'px',
	                height: (heightImage * $scope.zoomPercent / 100) + 'px'
	            }
	        }
	        $scope.isShowPercent = true;
	        showPercentAuto();
	    };
	    function showPercentAuto(){
	        $timeout(function () {
	            $scope.isShowPercent = false;
	        }, 1000);
	    }
	    $scope.zoomOut = function ($event) {
	        $event.preventDefault();
	        $event.stopPropagation();
	        if(!heightImage || !widthImage){
	            widthImage = $('.cp-image-container').width();
	            heightImage = $('.cp-image-container').height();
	            scaleImage = widthImage / heightImage;
	            $scope.zoomPercent = 85;
	            $scope.styleZoom = {
	                width: (widthImage * $scope.zoomPercent / 100) +'px',
	                height: (heightImage * $scope.zoomPercent / 100) + 'px'
	            }
	        } else {
	            if($scope.zoomPercent >= 15){
	                $scope.zoomPercent = $scope.zoomPercent - 15;
	            }
	            $scope.styleZoom = {
	                width: (widthImage * $scope.zoomPercent / 100) +'px',
	                height: (heightImage * $scope.zoomPercent / 100) + 'px'
	            }
	        }
	        $scope.isShowPercent = true;
	        showPercentAuto();
	    };
	    
	
}]);
