myApp.controller('materialMasterCrtl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q','Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    
    $scope.skuUnitList = [];
    $scope.selectedSKUUnit = undefined;
    
    $scope.usageUnitList = [];
    $scope.selectedUsageUnit = undefined;
    
    $scope.materialList = [];
    
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    
    $scope.deleteMaterial = undefined;
    
    $scope.btnVisibility = {
    	newBtn: false,
    	modifyBtn: true
    };
    $scope.pkVisibility = {
    		materialCode: false,
    		barcode: false
    }
    
    $scope.listKeyTable = [{
        code : 'materialCode',
        name : 'Material Code',
        isSortFilter : true,
        iClass : "text-center"
    },{
        code : 'barcode',
        name : 'Barcode',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'materialName',
        name : 'Material Name',
        iClass : "text-center"
    },{
        code : 'usageUnitName',
        name : 'Usage Unit',
        iClass : "text-center"
    },{
        code : 'skuUnitName',
        name : 'SKU Unit',
        iClass : "text-center"
    },{
        code : 'lineProdCode',
        name : 'Product Code',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		var URI_UPLOAD_FILE = 'uploadMaterial';
		$event.stopPropagation();
		$event.preventDefault();
		$scope.progressPercentage = 0;
		var URL_API_UPLOAD = contextPath + CONTSTANT_SERVICE_URI.URI_FILE + URI_UPLOAD_FILE;
		Upload.upload({
			url: URL_API_UPLOAD,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			$scope.materialList = resp.data
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			showMessage('Failed to upload the file. Try again!');
		}, function (evt) {
			$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + $scope.progressPercentage + '% ' );
			$scope.showLoaderAllPage = false;
		});
	};
    $scope.clean = function(){
    	$scope.btnVisibility.newBtn = false;
    	$scope.btnVisibility.modifyBtn = true;
    	$scope.itemObj = {
        		materialCode: '',
        		barcode: '',
        		materialName: '',
        		usageUnitId: 0,
        		skuunitId: 0,
        		skuusageQty : 1,
        		isLineProd: false,
        		productCode: ''
        	};
    	$scope.selectedUsageUnit = $scope.usageUnitList[0];
    	$scope.selectedSKUUnit = $scope.skuUnitList[0];
    	$scope.pkVisibility.materialCode = false;
    	$scope.pkVisibility.barcode = false;
    }
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	$scope.itemObj = {
    		materialCode: '',
    		barcode: '',
    		materialName: '',
    		usageUnitId: 0,
    		skuunitId: 0,
    		skuusageQty : 1,
    		isLineProd: false,
    		productCode: ''
    	}
    	showMessage('Initalizing.....');
    	var API_GET_USAGE_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllUsageUnits';
    	var API_GET_SKU_UNIT = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllSkuUnits';
    	var API_GET_ALL_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllMaterial';
    	$q.all([
			$http({
			    method: 'GET',
			    url: API_GET_SKU_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.skuUnitList  = response.data;
			        $scope.selectedSKUUnit = $scope.skuUnitList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllSkuUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_USAGE_UNIT,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        $scope.usageUnitList  = response.data;
			        $scope.selectedUsageUnit = $scope.usageUnitList[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllUsageUnits]');
			}),
			$http({
			    method: 'GET',
			    url: API_GET_ALL_MATERIAL,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			        var _data  = response.data;
			        for(var i = 0; i<_data.length; i++){
			        	var itemObj = _data[i];
			        	 var obj = {
			        		materialCode: itemObj.materialCode,
			        		barcode: itemObj.barcode,
			        		materialName: itemObj.materialName,
			        		usageUnitId: itemObj.usageUnitId,
			        		skuunitId: itemObj.skuunitId,
			        		skuusageQty : itemObj.skuusageQty,
			        		lineProdCode: itemObj.productCode
					     };
			        	 for(var j =0; j< $scope.usageUnitList.length; j++){
				    		if($scope.usageUnitList[j].unitId == itemObj.usageUnitId)
				    		{
				    			obj.usageUnitName = $scope.usageUnitList[j].unitName; 
				    			break;
				    		}
				    	 }
				    	 for(var j =0; j< $scope.skuUnitList.length; j++){
				    		if($scope.skuUnitList[j].unitId == itemObj.skuunitId)
				    		{
				    			obj.skuUnitName = $scope.skuUnitList[j].unitName; 
				    			break;
				    		}
				    	 }
				    	 $scope.materialList.push(obj);
			        }
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllUsageUnits]');
			}),
			
        ]).then (function () {
        	
        })
        
    };
    $scope.refresh = function(){
    	var API_GET_ALL_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findAllMaterial';
    	$http({
		    method: 'GET',
		    url: API_GET_ALL_MATERIAL,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {
		    if(response.data){
		        var _data  = response.data;
		        $scope.materialList = []
		        for(var i = 0; i<_data.length; i++){
		        	var itemObj = _data[i];
		        	 var obj = {
		        		materialCode: itemObj.materialCode,
		        		barcode: itemObj.barcode,
		        		materialName: itemObj.materialName,
		        		usageUnitId: itemObj.usageUnitId,
		        		skuunitId: itemObj.skuunitId,
		        		skuusageQty : itemObj.skuusageQty,
		        		lineProdCode: itemObj.productCode,
		        		usageUnitName: '',
		        		skuUnitName : ''
		        		
				     };
		        	 for(var j =0; j< $scope.usageUnitList.length; j++){
			    		if($scope.usageUnitList[j].unitId == itemObj.usageUnitId)
			    		{
			    			obj.usageUnitName = $scope.usageUnitList[j].unitName; 
			    			break;
			    		}
			    	 }
			    	 for(var j =0; j< $scope.skuUnitList.length; j++){
			    		if($scope.skuUnitList[j].unitId == itemObj.skuunitId)
			    		{
			    			obj.skuUnitName = $scope.skuUnitList[j].unitName; 
			    			break;
			    		}
			    	 }
			    	 $scope.materialList.push(obj);
		        }
		    }
		}, function errorCallback(response) {
		    console.log('ERROR CALL : [findAllUsageUnits]');
		})
    }
    $scope.showEditMaterial = function(row){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	$scope.pkVisibility.materialCode = true;
    	$scope.pkVisibility.barcode= true;
    	
		$scope.itemObj.materialCode = row.materialCode;
		$scope.itemObj.barcode= row.barcode;
		$scope.itemObj.materialName= row.materialName;
		$scope.itemObj.usageUnitId= row.usageUnitId;
		$scope.itemObj.skuunitId= row.skuunitId;
		$scope.itemObj.skuusageQty = row.skuusageQty;
		$scope.itemObj.isLineProd= false;
		$scope.itemObj.productCode=row.lineProdCode;   	
		if($scope.itemObj.productCode){
			$scope.itemObj.isLineProd = true
		}
    	for(var i =0; i< $scope.usageUnitList.length; i++){
    		if($scope.usageUnitList[i].unitId == row.usageUnitId)
    		{
    			$scope.selectedUsageUnit = $scope.usageUnitList[i]; 
    			break;
    		}
    	}
    	for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == row.skuunitId)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    };
    $scope.showDelete = function(row){
    	$scope.btnVisibility.newBtn = true;
    	$scope.btnVisibility.modifyBtn = false;
    	
		$scope.itemObj.materialCode = row.materialCode;
		$scope.itemObj.barcode= row.barcode;
		$scope.itemObj.materialName= row.materialName;
		$scope.itemObj.usageUnitId= row.usageUnitId;
		$scope.itemObj.skuunitId= row.skuunitId;
		$scope.itemObj.skuusageQty = row.skuusageQty;
		$scope.itemObj.isLineProd= false;
		$scope.itemObj.productCode=row.lineProdCode;
		if($scope.itemObj.productCode){
			$scope.itemObj.isLineProd = true
		}
     
    	for(var i =0; i< $scope.usageUnitList.length; i++){
    		if($scope.usageUnitList[i].unitId == row.usageUnitId)
    		{
    			$scope.selectedUsageUnit = $scope.usageUnitList[i]; 
    			break;
    		}
    	}
    	for(var i =0; i< $scope.skuUnitList.length; i++){
    		if($scope.skuUnitList[i].unitId == row.skuunitId)
    		{
    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
    			break;
    		}
    	}
    };
    $scope.syncMaterial = function(){
    	var API_SYNC_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'syncMaterial';
		$http({
		    method: 'GET',
		    url: API_SYNC_MATERIAL,
		    headers: {'Content-Type': 'application/json; charset=utf-8'}
		}).then(function successCallback(response) {
			if(response.data){
				$scope.refresh()
				showMessage("Success!");
			}else{
				showMessage("Failed");
			}
		});
    }
    $scope.deleteMaterial = function(){
    	if($scope.itemObj.materialCode !='')
		{  		
    		var API_DELETE_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'deleteMaterial/' +  $scope.itemObj.materialCode;
    		$http({
    		    method: 'DELETE',
    		    url: API_DELETE_MATERIAL,
    		    headers: {'Content-Type': 'application/json; charset=utf-8'}
    		}).then(function successCallback(response) {
    		    switch(response.data){
    			    case 0:{
    			    	showMessage("Failed");
    			    }break;
    			    case 1:{
    			    	showMessage("Success!");
    			    	for(var i = 0; i< $scope.materialList.length; i++)
    			    	{
    			    		if($scope.materialList[i].materialCode === $scope.itemObj.materialCode && $scope.materialList[i].barcode === $scope.itemObj.barcode){
    			    			$scope.materialList.splice(i, 1); 
    			    			break;
    			    		}
    			    	}
    			    	$scope.clean();
    			    }
    		    }
    		}, function errorCallback(response) {
    		    console.log('ERROR CALL : [deleteMaterial]');
    		})
		}else{
			showMessage("Material Code or Barcode is not null");
		}
    }
    
    $scope.findMaterial = function(){
    	if($scope.itemObj.materialCode!='' && $scope.itemObj.barcode!=''){
    		var API_FIND_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'findMaterial';
    		$http({
			    method: 'GET',
			    url: API_FIND_MATERIAL,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {materialCode: $scope.itemObj.materialCode}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.itemObj = response.data;
			    	$scope.btnVisibility.newBtn = true;
			    	$scope.btnVisibility.modifyBtn = false;
			    	$scope.pkVisibility.materialCode = true;
			    	$scope.pkVisibility.barcode = true;
			    	for(var i =0; i< $scope.usageUnitList.length; i++){
			    		if($scope.usageUnitList[i].unitId == $scope.itemObj.usageUnitId)
			    		{
			    			$scope.selectedUsageUnit = $scope.usageUnitList[i]; 
			    			break;
			    		}
			    	}
			    	for(var i =0; i< $scope.skuUnitList.length; i++){
			    		if($scope.skuUnitList[i].unitId == $scope.itemObj.skuunitId)
			    		{
			    			$scope.selectedSKUUnit = $scope.skuUnitList[i]; 
			    			break;
			    		}
			    	}
			    }else{
			    	showMessage("Not found Material Code: " + $scope.itemObj.materialCode);
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findMaterial]');
			})
    	}else{
    		showMessage("Material Code or Barcode is not null");
		}
    }
    $scope.modifyMaterial = function(){
    	if($scope.itemObj.materialCode!='' && $scope.itemObj.barcode!=''){
    		var API_UPDATE_ITEM = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'updateMaterial';
    		$scope.itemObj.usageUnitId = $scope.selectedUsageUnit.unitId;
    		$scope.itemObj.skuunitId = $scope.selectedSKUUnit.unitId;
    		$http({
			    method: 'POST',
			    url: API_UPDATE_ITEM,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	for(var i = 0; i< $scope.materialList.length; i++)
    			    	{
    			    		if($scope.materialList[i].materialCode === $scope.itemObj.materialCode && $scope.materialList[i].barcode === $scope.itemObj.barcode){
    			    			$scope.materialList[i].materialName = $scope.itemObj.materialName;
    			    			$scope.materialList[i].usageUnitId =  $scope.usageUnitId;
    			    			$scope.materialList[i].usageUnitName = $scope.selectedUsageUnit.unitName; 
    			    			$scope.materialList[i].skuunitId = $scope.skuunitIdl
    			    			$scope.materialList[i].skuUnitName = $scope.selectedSKUUnit.unitName;
    	    			    	$scope.materialList[i].skuusageQty = $scope.itemObj.skuusageQty;
    	    			    	$scope.materialList[i].lineProdCode = $scope.itemObj.productCode
    			    			break;
    			    		}
    			    	}
				    	 $scope.clean();			    	
				    	 showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newMaterial]');
			})
    	}else{
    		showMessage("Material Code or Barcode is not null");
		}
    };
    $scope.saveMaterial = function(){
    	if($scope.itemObj.materialCode!='' && $scope.itemObj.barcode!=''){
    		var API_NEW_MATERIAL = contextPath + CONTSTANT_SERVICE_URI.URI_ITEM + 'newMaterial';
    		$scope.itemObj.usageUnitId = $scope.selectedUsageUnit.unitId;
    		$scope.itemObj.skuunitId = $scope.selectedSKUUnit.unitId;
    		$http({
			    method: 'POST',
			    url: API_NEW_MATERIAL,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    data: $scope.itemObj
			}).then(function successCallback(response) {
			    switch(response.data){
				    case 0:{
				    	showMessage("Failed");
				    }break;
				    case 1:{
				    	var obj = {
				        		materialCode: $scope.itemObj.materialCode,
				        		barcode: $scope.itemObj.barcode,
				        		materialName: $scope.itemObj.materialName,
				        		usageUnitId: $scope.usageUnitId,
				        		usageUnitName:  $scope.selectedUsageUnit.unitName ,
				        		skuunitId: $scope.skuunitId,
				        		skuUnitName: $scope.selectedSKUUnit.unitName,
				        		skuusageQty : $scope.itemObj.skuusageQty,
				        		lineProdCode: $scope.itemObj.productCode,
				        };
				    	$scope.itemObj = {
				        		materialCode: '',
				        		barcode: '',
				        		materialName: '',
				        		usageUnitId: 0,
				        		skuunitId: 0,
				        		skuusageQty : 1,
				        		isLineProd: false,
				        		productCode: ''
				        	}
				    	$scope.selectedUsageUnit = $scope.usageUnitList[0];
				    	$scope.selectedSKUUnit = $scope.skuUnitList[0];
				    	$scope.materialList.push(obj);
				    	showMessage("Success!");
				    }break;
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [newMaterial]');
			})
    	}else{
			showMessage("Material Code or Barcode is not null");
		}
    };
    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    };
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});