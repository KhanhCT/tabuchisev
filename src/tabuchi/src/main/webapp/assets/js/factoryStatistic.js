myApp.controller('factoryCtrl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q) {
   /*
   * {
        dataSuffix: 'Unit Test',
        name: 'Monthly Factory Productivity',
        title: 'Factory Productivity Chart',
        xtitle: '',
        ytitle: 'Number of ok products',
        data:[]
   }*/ //TODO: DATA TEMPLATE COLUMN
	$scope.columnDataObject = undefined;
   
   $scope.factoryList = [];
   $scope.selectedFactory = undefined;
   
   $scope.selectedDate = moment(new Date(), 'DD/MM/YYYY').format('DD/MM/YYYY')
   $scope.lineData = [];
   $scope.facData = [];
   $scope.today = new Date()
   $scope.curDate = new Date()
   
   $scope.lineListFilter = [];
   $scope.selectedLine = undefined;
   /*
   * {
		    title: 'Line Productivity Chart',
	        xtitle: 'Line',
	        ytitle: 'Number of products',
	        okData:{
	        	okName: 'OK Products',
	 	        data:[]
	        },
	        ngData:{
	        	ngName: 'NG Products',
	        	data:[]
	        }
  }*/ // TODO : TEMPLATE DATA
   $scope.sbsData = undefined;
   $scope.factoryChanged = function(){
   	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
   	$http({
			    method: 'GET',
			    url: API_FIND_ALL_LINE,
			    headers: {'Content-Type': 'application/json; charset=utf-8'},
			    params: {factoryId: $scope.selectedFactory.factoryId}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.lineListFilter = response.data;
			    	$scope.selectedLine = $scope.lineListFilter[0];
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
			})
   }
   
    $scope.initializeData = function(){
    	var API_FIND_ALL_FACTORY = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllFactory';
    	var API_FIND_ALL_LINE = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'findAllLine';
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	var API_GET_FAC_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyFactoryProductivity';
    	var momentFrom = moment($scope.today, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	$http({
			    method: 'GET',
			    url: API_FIND_ALL_FACTORY,
			    headers: {'Content-Type': 'application/json; charset=utf-8'}
			}).then(function successCallback(response) {
			    if(response.data){
			    	$scope.factoryList = response.data;
			    	$scope.selectedFactory = $scope.factoryList[0];
			    	if($scope.selectedFactory !=undefined){
			    		$http({
						    method: 'GET',
						    url: API_FIND_ALL_LINE,
						    headers: {'Content-Type': 'application/json; charset=utf-8'},
						    params: {factoryId: $scope.selectedFactory.factoryId}
						}).then(function successCallback(response) {
						    if(response.data){
						    	$scope.lineListFilter = response.data;
						    	$scope.selectedLine = $scope.lineListFilter[0];
						    	$http({
						            method: 'GET',
						            url: API_GET_LINE_DATA,
						            headers: {'Content-Type': 'application/json; charset=utf-8'},
						            params: {date: momentFrom, lineId: $scope.selectedLine.lineId, factoryId: $scope.selectedFactory.factoryId}
						        }).then(function successCallback(response) {
                                    var columnDataObj = {
                                        dataSuffix: 'Unit Test',
                                        name: 'Monthly Line Productivity',
                                        title: 'Line Productivity Chart',
                                        xtitle: '',
                                        ytitle: 'Number of ok products',
                                        data: []
                                    };
									if(response.data && response.data !=''){
										var lineData = response.data;
										for(var i =0; i< response.data.length; i++){
                                            columnDataObj.data.push({
											   time: lineData[i].key ,
											   color: '#91b6f2',
											   value: lineData[i].value,
										   });
										}
									}
                                    $scope.columnDataObject = angular.copy(columnDataObj);
						        }, function errorCallback(response) {
									$scope.showLoaderAllPage = false;
						            console.log( + ' [ERROR Call getMonthyLineProductivity]');
						        });
						    	$http({
						            method: 'GET',
						            url: API_GET_FAC_DATA,
						            headers: {'Content-Type': 'application/json; charset=utf-8'},
						            params: {date: momentFrom,  factoryId: 1}
						        }).then(function successCallback(response) {
                                    var objectDataSbs = {
                                        title: 'Factory Productivity Chart',
                                        xtitle: 'Line',
                                        ytitle: 'Number of products',
                                        okData:{
                                            okName: 'OK Products',
                                            data:[]
                                        },
                                        ngData:{
                                            ngName: 'NG Products',
                                            data:[]
                                        }
                                    };
						            if(response.data && response.data!= ''){
						            	$scope.facData = response.data;
						            	for(var i =0; i< $scope.facData.length; i++){
                                            objectDataSbs.okData.data.push({
						                    	line: $scope.facData[i].key,
						             		   	color: '#91b6f2',
						             		   	value:  $scope.facData[i].value
						                    });
                                            objectDataSbs.ngData.data.push({
						                		line: $scope.facData[i].key,
						             		   	color: '#01b6f2',
						             		   	value:$scope.facData[i].otherValue
						                    });
						            	}
						            }
                                    $scope.sbsData = angular.copy(objectDataSbs);
						        }, function errorCallback(response) {
									$scope.showLoaderAllPage = false;
						            console.log( + ' [ERROR Call getMonthyFactoryProductivity]');
						        });
						    }
						}, function errorCallback(response) {
						    console.log('ERROR CALL : [findAllFactory]');
						})
			    	}
			    }
			}, function errorCallback(response) {
			    console.log('ERROR CALL : [findAllFactory]');
		});
    	
    	
    	
    }
    
    $scope.filter = function(){
    	var momentFrom = moment($scope.selectedDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_FAC_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyFactoryProductivity';
    	$http({
            method: 'GET',
            url: API_GET_FAC_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom,  factoryId: $scope.selectedFactory.factoryId}
        }).then(function successCallback(response) {
            var objectDataSbs = {
                title: 'Line Productivity Chart',
                xtitle: 'Line',
                ytitle: 'Number of products',
                okData:{
                    okName: 'OK Products',
                    data:[]
                },
                ngData:{
                    ngName: 'NG Products',
                    data:[]
                }
            };
            if(response.data && response.data!= ''){
            	$scope.facData = response.data;
            	for(var i =0; i< $scope.facData.length; i++){
                    objectDataSbs.okData.data.push({
                    	line: $scope.facData[i].key,
             		   	color: '#91b6f2',
             		   	value:  $scope.facData[i].value
                    });
                    objectDataSbs.ngData.data.push({
                		line: $scope.facData[i].key,
             		   	color: '#01b6f2',
             		   	value: $scope.facData[i].otherValue
                    });
            	}	  
            }
            $scope.sbsData = angular.copy(objectDataSbs);
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyFactoryProductivity]');
        });
    }
    
    $scope.nextMonth = function(){
    	
    	$scope.curDate.setMonth($scope.curDate.getMonth() + 1);
    	var momentFrom = moment($scope.curDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: 1, factoryId: 1}
        }).then(function successCallback(response) {
            var columnDataObj = {
                dataSuffix: 'Unit Test',
                name: 'Monthly Factory Productivity',
                title: 'Factory Productivity Chart',
                xtitle: '',
                ytitle: 'Number of ok products',
                data: []
            };
            if(response.data && response.data !=''){
            	var lineData = response.data;
            	for(var i =0; i< response.data.length; i++){
                    columnDataObj.data.push({
					   time: lineData[i].key ,
					   color: '#91b6f2',
					   value: lineData[i].value,
				   });
            	}
            }
            $scope.columnDataObject = angular.copy(columnDataObj);
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    	
    }
    
    
    $scope.thisMonth = function(){
    	$scope.lineData = []
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	var momentFrom = moment($scope.today, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: 1, factoryId: 1}
        }).then(function successCallback(response) {
            var columnDataObj = {
                dataSuffix: 'Unit Test',
                name: 'Monthly Factory Productivity',
                title: 'Factory Productivity Chart',
                xtitle: '',
                ytitle: 'Number of ok products',
                data: []
            };
            if(response.data && response.data !=''){
            	var lineData = response.data;
            	for(var i =0; i< response.data.length; i++){
                    columnDataObj.data.push({
					   time: lineData[i].key ,
					   color: '#91b6f2',
					   value: lineData[i].value,
				   });
            	}
            }
           	$scope.columnDataObject = angular.copy(columnDataObj);
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    	
    }
    $scope.lastMonth = function(){
    	var date = new Date();
    	date.setMonth($scope.curDate.getMonth() - 1);
    	var momentFrom = moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: 1, factoryId: 1}
        }).then(function successCallback(response) {
            var columnDataObj = {
                dataSuffix: 'Unit Test',
                name: 'Monthly Factory Productivity',
                title: 'Factory Productivity Chart',
                xtitle: '',
                ytitle: 'Number of ok products',
                data: []
            };
        	if(response.data && response.data !=''){
            	var lineData = response.data;
            	for(var i =0; i< response.data.length; i++){
                    columnDataObj.data.push({
					   time: lineData[i].key ,
					   color: '#91b6f2',
					   value: lineData[i].value,
				   });
            	}
            }
            $scope.columnDataObject = angular.copy(columnDataObj);
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    }
    $scope.backMonth = function(){
    	$scope.curDate.setMonth($scope.curDate.getMonth() - 1);
    	var momentFrom = moment($scope.curDate, 'DD/MM/YYYY').format('DD/MM/YYYY')
    	var API_GET_LINE_DATA = contextPath + CONTSTANT_SERVICE_URI.URI_LINE + 'getMonthyLineProductivity';
    	$http({
            method: 'GET',
            url: API_GET_LINE_DATA,
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            params: {date: momentFrom, lineId: 1, factoryId: 1}
        }).then(function successCallback(response) {	
        	var columnDataObj = {
                dataSuffix: 'Unit Test',
                name: 'Monthly Factory Productivity',
                title: 'Factory Productivity Chart',
                xtitle: '',
                ytitle: 'Number of ok products',
                data:[]
            };
            if(response.data && response.data !=''){
            	var lineData = response.data;
            	for(var i =0; i< response.data.length; i++){
                    columnDataObj.data.push({
					   time: lineData[i].key ,
					   color: '#91b6f2',
					   value: lineData[i].value,
				   });
            	}
            }
            $scope.columnDataObject = angular.copy(columnDataObj);
        }, function errorCallback(response) {
			$scope.showLoaderAllPage = false;
            console.log( + ' [ERROR Call getMonthyLineProductivity]');
        });
    }
}]);
