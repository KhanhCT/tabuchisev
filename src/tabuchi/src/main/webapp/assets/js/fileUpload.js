myApp.controller('fileUploadCrtl', ['$scope', 'NgTableParams', 'ngTableEventsChannel', '$http','$timeout', '$q', 'Upload', function($scope, NgTableParams, ngTableEventsChannel, $http,$timeout, $q, Upload) {
    $scope.itemObj = {};
    $scope.showTable = true;
    
    $scope.mesObj = undefined;
    $scope.showLoaderAllPage = false;
    $scope.fileToUpload = undefined;
    $scope.videoList = [];
    $scope.desc = ''
    $scope.listKeyTable = [{
        code : 'link',
        name : 'Link',
        isSortFilter : true,
        iClass : "text-center"
    }, {
        code : 'desc',
        name : 'Description',
        iClass : "text-center"
    }];
    function showMessage(message) {
		$scope.mesObj = {
				type: true,
				text: message,
				active: true
		}
		$timeout(function () {
			$scope.mesObj.active = false;
        }, 2000);
	}
    /*TABLE*/
    $scope.itemGap = 10;
    $scope.listGap = [1,5,10,20];
    $scope.setItemGap = function(gap){
        $scope.itemGap = gap;
    };
    $scope.showFilter =function(key){
        key.openFilter = true;
    };
    $scope.clearFilter = function(key){
        key.textFilter = '';
    };
    $scope.changeSort = function(key){
        if(!$scope.sortColumn || $scope.sortColumn !== key) {
            if($scope.sortColumn){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = false;
            }
            key['sortAZ'] = true;
            key['sortZA'] = false;
            $scope.sortColumn = key;
        } else if($scope.sortColumn && $scope.sortColumn === key) {
            if($scope.sortColumn['sortAZ']){
                $scope.sortColumn['sortAZ'] = false;
                $scope.sortColumn['sortZA'] = true;
            } else if(key['sortZA']){
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            } else {
                $scope.sortColumn['sortAZ'] = true;
                $scope.sortColumn['sortZA'] = false;
            }
        }
    };

    $scope.hideFilter = function (key){
        key.openFilter = false;
    };
    
    $scope.uploadFile = function ($event) {
		$scope.showLoaderAllPage = false;
		$event.stopPropagation();
		$event.preventDefault();
		var URL_API_UPLOAD = 'http://192.168.1.26:5000/file';
		Upload.upload({
			url: URL_API_UPLOAD,
			transformRequest: angular.identity,
			method: 'POST',
			data: {file: $scope.fileToUpload}
		}).then(function (resp) {
			if(resp.data){
				var obj = {
					link: resp.data.link,
					desc: $scope.desc
				}
				$scope.videoList.push(obj);
			}
			$timeout(function(){
				$scope.showLoaderAllPage = false;
			}, 500);
			
		}, function (resp) {
			$scope.showLoaderAllPage = false;
			
		}, function (evt) {
			$scope.showLoaderAllPage = false;
		});
	}
    
    $scope.initializeData = function () {
    	$scope.showLoaderAllPage = false
    	showMessage('Initalizing.....');
    	$scope.itemObj = {  
  
        };
      
       $q.all([
			
        ]).then (function () {
        	
        })
    };
    $scope.clean = function(){
    	$scope.itemObj = {  
        		lineProdId: 0,
    			lineProdCode: '',
    			processId: $scope.processIdList[0],
    			processName: '',
    			minSt: 0,
    			maxSt: 0,
    			standardVideoLink : '',
    			workVideoLink: '',
    			wplaceQty: 1
        }
    };

    $scope.checkedFunction = function () {
    	if ($scope.isSameAll) {
    		$scope.showTable = false;
    	} else {
    		$scope.showTable = true;
    	}
    }
   
}]);
myApp.filter('orderByCustom', function () {
    return function (arrays, keySort) {
        if(keySort){
            arrays.sort(function (a, b) {
                if(keySort['sortAZ']){
                    if(!a[keySort['code']]) return -1;
                    if(!b[keySort['code']]) return 1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return 1;
                        else if (Number(tempA) < Number(tempB)) return -1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return 1;
                        else if (tempA < tempB) return -1;
                        else if (tempA === tempB) return 0;
                    }
                } else {
                    if(!a[keySort['code']]) return 1;
                    if(!b[keySort['code']]) return -1;
                    var tempA = a[keySort['code']];
                    var tempB = b[keySort['code']];
                    if(!isNaN(tempA) && !isNaN(tempB)){
                        if(Number(tempA) > Number(tempB)) return -1;
                        else if (Number(tempA) < Number(tempB)) return 1;
                        else if (Number(tempA) === Number(tempB)) return 0;
                    } else{
                        tempA = tempA.toString().toLowerCase();
                        tempB = tempB.toString().toLowerCase();
                        if(tempA > tempB) return -1;
                        else if (tempA < tempB) return 1;
                        else if (tempA === tempB) return 0;
                    }
                }
            });
            return arrays;
        } else {
            return arrays;
        }
    };
});
myApp.filter('customFilterText', function ($filter) {
    return function (arrObject, keyFilter) {
        var result = [];
        if(keyFilter){
            angular.forEach(arrObject, function (objArr) {
                var isFindTrue = 0;
                var countFilter = 0;
                for(var i = 0; i < keyFilter.length; i++){
                    if(keyFilter[i]['textFilter']
                        && keyFilter[i]['textFilter'] !== ''
                        && objArr[keyFilter[i]['code']]){
                        countFilter++;
                        var textSearch = keyFilter[i]['textFilter'].toLowerCase();
                        var textTemp = objArr[keyFilter[i]['code']].toString().toLowerCase();
                        if(textTemp.indexOf(textSearch) !== -1){
                            isFindTrue++;
                        }
                    }
                }
                if(countFilter === isFindTrue || countFilter === 0){
                    result.push(objArr);
                }
            });
            return result;
        } else {
            return arrObject;
        }
    };
});