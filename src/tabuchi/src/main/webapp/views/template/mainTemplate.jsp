<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 29/05/2018
  Time: 22:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
<head>
    <c:set var="titlePage"><tiles:getAsString name="titlePage" /></c:set>
    <%--<tiles:importAttribute name="titlePage" />--%>
    <title><spring:message code="${titlePage}"></spring:message></title>
    <%--<title><tiles:insertAttribute name="titlePage" /></title>--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/bootstrap/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/font-awesome-4.7.0/css/font-awesome.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/OwlCarousel2-2.2.1/owl.carousel.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/OwlCarousel2-2.2.1/owl.theme.default.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/OwlCarousel2-2.2.1/animate.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/ng-table/ng-table.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/flag-icon.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/responsive.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/jquery-confirm.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/css/util-css.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/css/bootstrap-datepicker.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/main_styles.css"/>">
    <script src="<c:url value="/assets/libs/js/jquery-3.2.1.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/js/highcharts_v5.0.7.js"/>"></script>
    <script src="<c:url value="/assets/libs/js/lodash.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/bootstrap/js/popper.js"/>"></script>
    <script src="<c:url value="/assets/libs/bootstrap/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/Isotope/isotope.pkgd.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/OwlCarousel2-2.2.1/owl.carousel.js"/>"></script>
    <script src="<c:url value="/assets/libs/easing/easing.js"/>"></script>
    <script src="<c:url value="/assets/js/custom.js"/>"></script>
    <script src="<c:url value="/assets/js/moment-with-locales.js"/>"></script>
    <script src="<c:url value="/assets/js/angular.min.js"/>"></script>
    <script src="<c:url value="/assets/js/jquery-ui.min.js"/>"></script>
    <script src="<c:url value="/assets/js/ng-file-upload-shim.min.js"/>"></script>
    <script src="<c:url value="/assets/js/ng-file-upload.min.js"/>"></script>
    <script src="<c:url value="/assets/js/jquery-confirm.js"/>"></script>
    <script src="<c:url value="/assets/js/jquery.inputmask.bundle.min.js"/>"></script>
    <script src="<c:url value="/assets/js/dirPagination.js"/>"></script>
    <script src="<c:url value="/assets/libs/js/bootstrap-datepicker.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/js/jquery.nicescroll.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/ng-table/ng-table.min.js"/>"></script>
    <script src="<c:url value="/assets/libs/js/angular-click-outside.js"/>"></script>
    <script src="<c:url value="/assets/js/app.js"/>"></script>
</head>
<body ng-app="AisApplication">
<div class="super_container">
    <!-- START HEADER -->
    <header class="header trans_300">
        <!-- Top Navigation -->
        <div class="top_nav">
            <div class="container h-full">
                <div class="row h-full">
                    <div class="col-md-6 offset-md-6 text-right">
                        <div class="top-social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
                            <ul>
                                <li><a href="https://www.facebook.com/AustralianInternationalSchoolVietnam/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/#!/aisvietnam"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://plus.google.com/u/0/+AustralianInternationalSchoolVietnamTT/posts"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                <li><a href="http://www.youtube.com/user/aisvietnam"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                <li><a href="http://portal.office.com/"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Navigation -->
        <div class="main_nav_container">
            <div class="container h-full">
                <div class="row h-full">
                    <div class="col-lg-12 text-right">
                        <div class="logo_container">
                            <a href="#"><img src="<c:url value="/assets/images/tabuchi_logo.png"></c:url>" alt=""></a>
                        </div>
                        <nav class="navbar nabar-cust">
                            <ul class="navbar_menu">
                    
                                <li class="navbar_menu_drp <c:if test="${menuObj == 'worker'}">active</c:if>">
                                    <a href="#">Worker Master</a>
                                    <ul class="drp_menu_selection">
                                        <li><a href="workerMaster">Register Worker</a></li>
                                    </ul>
                                </li>
                                <li class="navbar_menu_drp <c:if test="${menuObj == 'material'}">active</c:if>">
                                    <a href="#"><spring:message code="im.grp_ft_title"/></a>
                                    <ul class="drp_menu_selection">
                                        <li><a href="materialMaster">Material</a></li>
                                        <li><a href="lineProdMaster">Line Product</a></li>
                                        <li><a href="lineProdBOM">Line Product BOM</a></li>
                                        <li><a href="processProdBOM">Process Product BOM</a></li>
                                        <li><a href="unitMaster">Unit Master</a></li>
                                    </ul>
                                </li>
                               <li class="navbar_menu_drp <c:if test="${menuObj == 'line'}">active</c:if>">
                                    <a href="#">Line Master</a>
                                    <ul class="drp_menu_selection">
                                    	<li><a href="factoryMaster">Factory Detail</a></li>
                                    	<li><a href="processMaster">Process Detail</a></li>
                                        <li><a href="lineProcess">Line Process Config</a></li>
                                        <li><a href="prodLine">Production Line</a></li>  
                                        <li><a href="workPlace">Work Place</a></li>    
                                        <li><a href="productionPlan">Production Plan</a></li>   
                                        <li><a href="fileUploader">File Uploader</a></li>
                                    </ul>         
                                </li>
                                <li class="navbar_menu_drp <c:if test="${menuObj == 'line'}">active</c:if>">
                                    <a href="#">Statistic</a>
                                    <ul class="drp_menu_selection">
                                     	<li><a href="photoGallery">Photo Gallery</a></li>
                                    	<li><a href="factoryStatistic">Factory Productivity</a></li>                                 
                                        <li><a href="workerProductivity">Worker Productivity</a></li>
                                    </ul>         
                                </li>
                                
                                <li class="navbar_menu_drp <c:if test="${menuObj == 'user'}">active</c:if>">
                                    <a href="#"><spring:message code="um.grp_ft_title"/></a>
                                    <ul class="drp_menu_selection">
                                        <li><a href="registerUser"><spring:message code="um.ft_register_user"/></a></li>
                                        <li><a href="userManagement"><spring:message code="um.ft_show_all_user"/></a></li>
                                    </ul>
                                </li>          
                                <li><a href="logout <c:if test="${menuObj == 'login'}">active</c:if>"><spring:message code="sys.logout"/></a></li>
                            </ul>
                            <div class="hamburger_container">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="fs_menu_overlay"></div>
    <div class="hamburger_menu">
        <div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="hamburger_menu_content text-right" m-cus-scroll-inside>
            <ul class="menu_top_nav">
					
					<li
						class="menu_item <c:if test="${menuObj == 'customer'}">active</c:if>">
						<a href="#"><spring:message code="cus.gr_ft_title" /></a>
						<ul class="drp_menu_selection">
							<li><a href="registerCustomer"><spring:message
										code="cus.ft.register_customer" /></a></li>
							<li><a href="registerCustomerFromFile"><spring:message
										code="cus.ft.upload_customer_list" /></a></li>
						</ul>
					</li>
					<li
						class="menu_item <c:if test="${menuObj == 'material'}">active</c:if>">
						<a href="#"><spring:message code="im.grp_ft_title" /></a>
						<ul class="drp_menu_selection">
							<li><a href="itemMaster"><spring:message
										code="im.ni_title_page" /></a></li>
							<li><a href="lineProdMater">Line Product Master</a></li>
						</ul>
					</li>

					<li
						class="menu_item <c:if test="${menuObj == 'user'}">active</c:if>">
						<a href="#"><spring:message code="um.grp_ft_title" /></a>
						<ul class="drp_menu_selection">
							<li><a href="registerUser"><spring:message
										code="um.ft_register_user" /></a></li>
							<li><a href="userManagement"><spring:message
										code="um.ft_show_all_user" /></a></li>
						</ul>
					</li>
					<li class="menu_item"><a
						href="../logout <c:if test="${menuObj == 'login'}">active</c:if>"><spring:message
								code="sys.logout" /></a></li>

					<li class="menu_item social-hambuger">
                    <ul>
                        <li><a href="https://www.facebook.com/AustralianInternationalSchoolVietnam/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/#!/aisvietnam"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://plus.google.com/u/0/+AustralianInternationalSchoolVietnamTT/posts"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                        <li><a href="http://www.youtube.com/user/aisvietnam"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="http://portal.office.com/"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END HEADER -->
    <div class="main-content-template">
        <tiles:insertAttribute name="mainContent" />
    </div>

<!--     <footer class="footer" id="footer">
        <div class="inner">
            <div class="container">
                <div class="row ff">
                    widget
                    <div class="col-md-3">
                        <div id="text-7" class="widget widget_text">
                            <h5 class="widget-title">TABUCHI ELECTRIC COMPANY</h5>
                            <div class="textWidget">
                                <p>(Kindergarten – Year 13)<br>264 Mai Chi Tho (East-West Highway)<br>An Phu Ward | District 2<br>HCMC | Vietnam<br>T: +84 28 3742 4040</p>
                            </div>
                        </div>
                        <div id="text-8" class="widget widget_text">
                            <div class="textWidget">enrolments@aisvietnam.com <br>www.aisvietnam.com</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="text-3" class="widget widget_text">
                            <h5 class="widget-title">Thao Dien</h5>
                            <div class="textWidget"><p>(Kindergarten – Year 6)<br>APSC Compound&nbsp;36 Thao Dien Road<br>Thao Dien Ward | District 2<br>HCMC | Vietnam<br>T: +84 28 3744 6960</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="text-2" class="widget widget_text">
                            <h5 class="widget-title">Xi</h5>
                            <div class="textWidget"><p>(Kindergarten)<br>190 Nguyen Van Huong Street<br>Thao Dien Ward | District 2<br>HCMC | Vietnam<br>T: +84 28 3519 2727<span style="text-decoration: none;"><br></span></p>
                            </div>
                        </div>
                        <div id="text-10" class="widget widget_text">
                            <div class="textWidget"></div>
                        </div>
                    </div>
 
                </div>
            </div>
        </div>
    </footer> -->
    <div class="language-bar-bottom">
        <ul style="text-align: center;">
            <li class="language-item">
                <a href="?lang=vi">
                    <span class="flag-icon flag-icon-vn"></span>
                    <span class="language-text">Tiếng Việt</span>
                </a>
            </li>
            <li class="language-item">
                <a href="?lang=en">
                    <span class="flag-icon flag-icon-gb"></span>
                    <span class="language-text">English</span>
                </a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>
