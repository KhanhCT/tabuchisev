<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 10/06/2018
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/lineProcess.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container wrapper-item-master" ng-controller="lineProcessCtrl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
		<div class="loader"></div>
	</div>
    <div class="row">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Line Process Master</span>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="productID" class="col-4 col-form-label text-left">Product Code</label>
                <div class="col-8">
                     <select id="productID" class="form-control custom-select custom-select-sm" ng-model = "selectedProduct" ng-disabled="pkVisibility.processId"
                    ng-options="obj.lineProdName for obj in productList" >
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="processCode" class="col-4 col-form-label text-left">Process ID</label>
                <div class="col-8">
                    <select id="usageUnitID" class="form-control custom-select custom-select-sm" ng-model = "selectedProcess" ng-disabled="pkVisibility.processId"
                    ng-options="obj.processCode for obj in processIdList" >
                       
                    </select>
                </div>
            </div>
        </div>
    </div>
     <div class="row m-t-20">	  
        <div class="col-6">
            <div class="form-group row">
                <label for="wPlaceQtyID" class="col-4 col-form-label text-left">Work Place Qty</label>
                <div class="col-8">
                    <div class="input-group">
                        <input type="number" id="wPlaceQtyID" class="form-control" ng-model = "itemObj.wplaceQty" placeholder="Work Place Quantity" >
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label class="wrap-checkbox">
                    <input type="checkbox" name="check" ng-model = "itemObj.disabled"> <span class="label-text">Status </span>
                </label>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="minSTID" class="col-4 col-form-label text-left">Min ST</label>
                <div class="col-8">
                    <div class="input-group">
                        <input type="number" id="minSTID" class="form-control" ng-model = "itemObj.minSt" placeholder="Minimum Standard Time" >
                    </div>
                </div>
            </div>
        </div>
         <div class="col-sm-6">
            <div class="form-group row">
                <label for="maxSTID" class="col-4 col-form-label text-left">Max ST</label>
                <div class="col-8">
                    <div class="input-group">
                        <input type="number" id="maxSTID" class="form-control" ng-model = "itemObj.maxSt" placeholder="Maximum Standard Time" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
    	<div class="col-sm-6">
            <div class="form-group row">
                <label for="sVideoID" class="col-4 col-form-label text-left">Standard Video Link</label>
                <div class="col-8">
                    <div class="input-group">
                        <input type="text" id="sVideoID" class="form-control" ng-model = "itemObj.standardVideoLink" placeholder="Standard Video Link" >
                    </div>
                </div>
            </div>
        </div>
         <div class="col-6">
            <div class="form-group row">
                <label for="workVideoLinkID" class="col-4 col-form-label text-left">Work Video Link</label>
                <div class="col-8">
                    <div class="input-group">
                        <input type="text" id="workVideoLinkID" class="form-control" ng-model = "itemObj.workVideoLink" placeholder="Work Video Link" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   
    <div class="row m-t-20">
    	<div class="col-7 offset-md-2">
    		<button class="col-2 btn btn-success m-l-15" ng-click = "saveProcess()" ng-disabled="btnVisibility.newBtn"><spring:message code="cm.new" /></button>
	    	<button class="col-2 btn btn-danger m-l-15" ng-click = "modifyProcess()" ng-disabled="btnVisibility.modifyBtn"><spring:message code="cm.modify" /></button>
	    	<button class="col-2 btn btn-warning  m-l-15" ng-click = "findProcess()" ><spring:message code="cm.find" /></button>
	    	<button class="col-2 btn btn-danger  m-l-15" ng-click = "deleteProcess()">Delete</button>
    		<button class="col-2 btn btn-info  m-l-15" ng-click = "clean()" >Clear</button>
     	</div>
    </div>
    <div class="row m-t-20 m-b-5">
    	<div class="col-12"><div class="separate-line"></div></div>
    </div>
    <div class="row">
		<div class="col-12" ng-form="uploadForm" ng-cloak>
			<div ngf-drop ngf-select ng-model="fileToUpload" class="drop-box"
				ngf-drag-over-class="'dragover'" ngf-multiple="false"
				ngf-allow-dir="true" ngf-pattern="'.xls,.csv,.xlsx'"
				accept=".xls,.csv,.xlsx" ngf-max-size="20MB">
				<div class="content-drag">
					<i class="fa fa-cloud-upload custom-drag"></i>
					<div class="titlte-drag" ng-cloak>
						<span ng-if="!fileToUpload">Browser file to upload...</span> <span
							ng-if="fileToUpload" class="text-primary"
							ng-bind="fileToUpload.name"></span>
					</div>
					<button class="btn btn-primary custom-button"
						ng-disabled="!fileToUpload" ng-click="uploadFile($event)">UPLOAD</button>
				</div>
			</div>
		</div>
	</div>
    
    <div class="row m-t-5">
    	<div class="col-12"><div class="separate-line"></div></div>
    </div>
    <div class="row m-t-20" ng-if = "showTable != false">
        <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th ng-repeat="key in listKeyTable">
                                <div class="name-header" >
                                    <span ng-bind="key['name']"></span>
                                </div>
                                <div class="wrap-filter-sort">
                                    <div class="sort-icon" ng-click="changeSort(key)">
                                        <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src=  "assets/images/sort-couple-gray.png">
                                        <img ng-if="key['sortAZ']" class="img-sort" ng-src="assets/images/sort-desc-white.png">
                                        <img ng-if="key['sortZA']" class="img-sort" ng-src="assets/images/sort-asc-white.png">
                                    </div>
                                    <div class="filter-table" ng-click="showFilter(key)">
                                        <img class="img-filter" ng-if="key['textFilter']" ng-src="assets/images/filter-white.png">
                                        <img class="img-filter" ng-if="!key['textFilter']" ng-src="assets/images/filter-gray.png">
                                    </div>
                                </div>
                                <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                    <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                    <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                    <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                </div>
                            </th>
                             <th style="text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
	                        <tr dir-paginate="obj in lineProcessList | customFilterText:listKeyTable | itemsPerPage:itemGap">
	                            <td><span ng-bind="{{$index + 1}}"></span></td>
	                            <td ng-repeat="key in listKeyTable" ng-class="key.iClass">
										<span ng-bind="obj[key['code']]"></span>
									</td>
								<td style="text-align: center">
									<div class="btn btn-outline-primary" ng-click="showEdit(obj)">
										<span class="fa fa-pencil"></span>
									</div>
								</td>
							</tr>
	                        <tr ng-if="lineProcessList.length === 0">
	                            <td class="text-center" colspan="{{listKeyTable.length + 2}}">No data available</span></td>
	                        </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <dir-pagination-controls
                                    template-url="assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-md-6">
                            <ul class="pagination pull-right" ng-if="lineProcessList.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-accept-delete">
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content ">
                    <div class="modal-header">
                        <h3 class="modal-title float-l" id="deleteModalLongTitle"><spring:message code="cm.comfirmation"/></h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <spring:message code="va.notice"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><spring:message code="cm.cancel"/></button>
                        <button type="button" class="btn btn-success" ng-click="deleteProcess()" data-dismiss="modal"><spring:message code="cm.accept"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <alert-message type-alert="mesObj.type" message-alert="mesObj.text"
		show-alert="mesObj.active"></alert-message>
</div>
<script src="<c:url value="/assets/js/lineProcess.js"></c:url>"></script>

