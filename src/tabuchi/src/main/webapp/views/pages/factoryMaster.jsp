<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 10/06/2018
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/factoryMaster.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container wrapper-item-master" ng-controller="factoryMasterCtrl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
		<div class="loader"></div>
	</div>
    <div class="row">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Factory Information</span>
        </div>
    </div>

    <div class="row m-t-20" ng-if = "showTable != false">
        <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th ng-repeat="key in factoryKeyTable">
                                <div class="name-header" >
                                    <span ng-bind="key['name']"></span>
                                </div>
                                <div class="wrap-filter-sort">
                                    <div class="sort-icon" ng-click="changeSort(key)">
                                        <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src=  "assets/images/sort-couple-gray.png">
                                        <img ng-if="key['sortAZ']" class="img-sort" ng-src="assets/images/sort-desc-white.png">
                                        <img ng-if="key['sortZA']" class="img-sort" ng-src="assets/images/sort-asc-white.png">
                                    </div>
                                    <div class="filter-table" ng-click="showFilter(key)">
                                        <img class="img-filter" ng-if="key['textFilter']" ng-src="assets/images/filter-white.png">
                                        <img class="img-filter" ng-if="!key['textFilter']" ng-src="assets/images/filter-gray.png">
                                    </div>
                                </div>
                                <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                    <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                    <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                    <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                </div>
                            </th>
                      
                        </tr>
                        </thead>
                        <tbody>
	                        <tr dir-paginate="obj in factoryList | customFilterText:factoryKeyTable | itemsPerPage:itemGap">
	                            <td><span ng-bind="{{$index + 1}}"></span></td>
	                            <td ng-repeat="key in factoryKeyTable" ng-class="key.iClass">
										<span ng-bind="obj[key['code']]"></span>
									</td>
							</tr>
	                        <tr ng-if="factoryList.length === 0">
	                            <td class="text-center" colspan="{{factoryKeyTable.length + 1}}">No data available</span></td>
	                        </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <dir-pagination-controls
                                    template-url="assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-md-6">
                            <ul class="pagination pull-right" ng-if="factoryList.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-left font-mont">
            <span class="tag text-center">Detail Line Information</span>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-6">
            <div class="form-group row">
                <label for="factoryID" class="col-4 col-form-label text-left">Factory</label>
                <div class="col-8">
                     <select id="factoryID" class="form-control custom-select custom-select-sm" ng-change="factoryChanged()" ng-model = "selectedFactory" ng-disabled="pkVisibility.factoryId"
                    ng-options="obj.factoryName for obj in factoryList" >
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20 m-b-5">
    	<div class="col-12"><div class="separate-line"></div></div>
    </div>
    <div class="row m-t-20" ng-if = "showTable != false">
        <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableTransaction.total() == 0}">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th ng-repeat="key in lineKeyTable">
                                <div class="name-header" >
                                    <span ng-bind="key['name']"></span>
                                </div>
                                <div class="wrap-filter-sort">
                                    <div class="sort-icon" ng-click="changeSort(key)">
                                        <img ng-if="!key['sortAZ'] && !key['sortZA']" class="img-sort" ng-src=  "assets/images/sort-couple-gray.png">
                                        <img ng-if="key['sortAZ']" class="img-sort" ng-src="assets/images/sort-desc-white.png">
                                        <img ng-if="key['sortZA']" class="img-sort" ng-src="assets/images/sort-asc-white.png">
                                    </div>
                                    <div class="filter-table" ng-click="showFilter(key)">
                                        <img class="img-filter" ng-if="key['textFilter']" ng-src="assets/images/filter-white.png">
                                        <img class="img-filter" ng-if="!key['textFilter']" ng-src="assets/images/filter-gray.png">
                                    </div>
                                </div>
                                <div class="wrap-input-filter" ng-if="key['openFilter']" tw-click-outside="hideFilter(key)">
                                    <input class="input-filter" placeholder="Search..." type="text" ng-model="key['textFilter']">
                                    <span class="fa fa-search p-l-6 color-black" ng-if="!key['textFilter']"></span>
                                    <span class="fa fa-close close-custom" ng-click="clearFilter(key)" ng-if="key['textFilter']"></span>
                                </div>
                            </th>
                      
                        </tr>
                        </thead>
                        <tbody>
	                        <tr dir-paginate="obj in lineListFilter | customFilterText:lineKeyTable | itemsPerPage:itemGap">
	                            <td><span ng-bind="{{$index + 1}}"></span></td>
	                            <td ng-repeat="key in lineKeyTable" ng-class="key.iClass">
										<span ng-bind="obj[key['code']]"></span>
									</td>
							</tr>
	                        <tr ng-if="lineListFilter.length === 0">
	                            <td class="text-center" colspan="{{lineKeyTable.length + 1}}">No data available</span></td>
	                        </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <dir-pagination-controls
                                    template-url="assets/html/dirPagination.tpl.html"
                                    max-size="10"
                                    direction-links="true"
                                    boundary-links="true" >
                            </dir-pagination-controls>
                        </div>
                        <div class="col-md-6">
                            <ul class="pagination pull-right" ng-if="lineListFilter.length > 0" ng-cloak>
                                <li ng-repeat="gap in listGap" ng-class="{ active : gap === itemGap }">
                                    <a href="" ng-click="setItemGap(gap)"><span ng-bind="gap"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <alert-message type-alert="mesObj.type" message-alert="mesObj.text"
		show-alert="mesObj.active"></alert-message>
</div>
<script src="<c:url value="/assets/js/factoryMaster.js"></c:url>"></script>

