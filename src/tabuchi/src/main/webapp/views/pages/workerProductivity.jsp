<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 06-Aug-18
  Time: 12:23 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/workerProductivity.css"></c:url>">
<script src="<c:url value="/assets/js/workerProductivity.js"></c:url>"></script>
<script src="<c:url value="/assets/js/barChart.js"></c:url>"></script>
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container " ng-controller="workerCtrl" ng-init="initializeData()">
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<span class="col-3 col-form-label  normal_label">Date</span>
				<div class="col-sm-9">
					<div class="input-group">
						<input type="text" id="toDate" class="form-control normal_label"
							placeholder="dd/mm/yyyy" ng-class="{'invalid-input': !toDate}"
							ng-model="selectedDate" date-picker
							aria-describedby="toDateAddon">
						<div class="input-group-append">
							<span class="input-group-text" id="toDateAddon"><i
								class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<label for="factoryID" class="col-3 col-form-label text-left">Factory</label>
				<div class="col-9">
					<select id="factoryID"
						class="form-control custom-select custom-select-sm"
						style="font-size: 16;" ng-model="selectedFactory"
						ng-change="factoryChanged()"
						ng-options="obj.factoryName for obj in factoryList ">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<label for="lineID" class="col-3 col-form-label text-left">Line</label>
				<div class="col-9">
					<select id="lineID"
						class="form-control custom-select custom-select-sm"
						ng-model="selectedLine" style="font-size: 16;"
						ng-options="obj.lineDesc for obj in lineListFilter ">
					</select>
				</div>
			</div>
		</div>
	</div>
<!-- 	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<label for="shiftId" class="col-3 col-form-label text-left">Line</label>
				<div class="col-9">
					<select id="shiftId"
						class="form-control custom-select custom-select-sm"
						ng-model="selectedLine" style="font-size: 16;"
						ng-options="obj.lineDesc for obj in lineListFilter ">
					</select>
				</div>
			</div>
		</div>
	</div> -->
	<div class="row m-t-5 ">
		<div class="col-4 offset-4">
			<button class="btn btn-warning custom-view col-12" type="button"
				ng-click="filter()">Filter</button>
		</div>
	</div>
	<div class="row card m-t-20">
		<div class="card-body">
			<h5 class="card-title">Factory Productivity Chart</h5>
			<ng-sbs-barchart column-data="sbsData"></ng-sbs-barchart>
		</div>
	</div>
	<br/>
	<div class="row card m-t-20">
		<div class="card-body">
			<h5 class="card-title">Line Productivity Chart</h5>
			<ng-barchart column-data-object="columnDataObject"></ng-barchart>
		</div>
	</div>
	 <div class="row m-t-20">
    	<div class="col-12 offset-2" >
    		<button class="col-2 btn btn-warning m-l-15 " ng-click = "backMonth()" ><span class="fa fa-angle-left"></span></button>
    		<button class="col-2 btn btn-success m-l-15 " ng-click = "lastMonth()" ><span >Last Month</span></button>
    		<button class="col-2 btn btn-success m-l-15 " ng-click = "thisMonth()" ><span >This Month</span></button>
    		<button class="col-2 btn btn-warning m-l-15 " ng-click = "nextMonth()" ><span class="fa fa-angle-right"></span></button>
    	</div>
    </div>
    <alert-message type-alert="mesObj.type" message-alert="mesObj.text"
		show-alert="mesObj.active"></alert-message>
</div>