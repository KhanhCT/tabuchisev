<%--
  Created by IntelliJ IDEA.
  User: chien
  Date: 29/05/2018
  Time: 22:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/bootstrap/css/bootstrap.min.css"></c:url>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/font-awesome-4.7.0/css/font-awesome.min.css"></c:url>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/loginStyle.css"></c:url>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/css/util-css.css"></c:url>">
    <script type="text/javascript" src="<c:url value="/assets/libs/js/jquery-3.2.1.min.js"></c:url>"></script>
    <script type="text/javascript" src="<c:url value="/assets/js/angular.min.js"></c:url>"></script>
    <script type="text/javascript" src="<c:url value="/assets/libs/bootstrap/js/bootstrap.min.js"></c:url>"></script>
    <script type="text/javascript" src="<c:url value="/assets/js/login.js"></c:url>"></script>
</head>
<body ng-app="loginApplication">
    <div class="limiter" ng-controller="loginController">
        <div class="container-login-page">
            <div class="wrap-login-page">
                <div class="login-page-form-title">
                        <span class="login-page-form-title-1">
                            Sign In
                        </span>
                </div>

                <form class="login-page-form validate-form" name="loginForm" method="POST" ng-submit="showPreload()"
                      action="${pageContext.servletContext.contextPath}/authentication">
                    <div id="loader-wrapper" ng-if="showLoaderLogin" ng-cloak>
                        <div id="loader"></div>
                    </div>
                    <c:if test="${message eq 'error'}">
                    <div class="wrap-error-message">
                        <span class="error-message">
                            <spring:message  code="label.login_error" />
                        </span>
                    </div>
                    </c:if>
                    <div class="wrap-input-page validate-input m-b-26" data-validate="Username is required">
                        <span class="label-input-page">Username</span>
                        <input class="input-page" type="text" name="username" id="ais-username-login" ng-model="aisUsernameLogin" placeholder="Enter username">
                        <span class="focus-input-page"></span>
                    </div>

                    <div class="wrap-input-page validate-input m-b-18" data-validate = "Password is required">
                        <span class="label-input-page">Password</span>
                        <input class="input-page" type="password" name="password" id="ais-password-login" ng-model="aisPasswordLogin" placeholder="Enter password">
                        <span class="focus-input-page"></span>
                    </div>

                    <div class="flex-sb-m w-full p-b-30">
                        <div class="contact-page-form-checkbox">
                            <input class="input-checkbox-page" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox-page" for="ckb1">
                                Remember me
                            </label>
                        </div>

                        <div>
                            <a href="#" class="txt1">
                                Forgot Password?
                            </a>
                        </div>
                    </div>

                    <div class="container-login-page-form-btn">
                        <button class="login-page-form-btn" ng-click="showPreload()">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
