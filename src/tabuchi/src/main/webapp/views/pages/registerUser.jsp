<%--
  Created by IntelliJ IDEA.
  User: tdchien
  Date: 6/20/18
  Time: 12:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/registerUser.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<div class="container wrap-register-user" ng-controller="registerUserCtrl" ng-init="initializeData()">
    <div class="loader-wrapper" ng-if="showLoaderAllPage">
        <div class="loader"></div>
    </div>
    <div class="row m-b-20">
        <div class="col-md-8 offset-md-2">
            <h3><spring:message code ="um.ru.title_page"/></h3>
        </div>
    </div>
    <div class="row wrap-form-register" ng-form="formRegister">
        <div class="col-md-6 offset-md-3 wrap-from-content">
            <div class="loader-wrapper" ng-if="showLoaderUser" ng-cloak>
                <div class="loader"></div>
            </div>
            <div class="form-group row">
                <label for="userNameId" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.username"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="userNameId" ng-model="userReg.userName"
                               class="form-control" required="required"
                               autocomplete="off" placeholder="Username" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="passwordId" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.password"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="password" id="passwordId" ng-model="userReg.password"
                               class="form-control" required="required"
                               autocomplete="off" placeholder="Password" >
                    </div>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="Gender" class="col-sm-5 col-form-label text-left">Gender</label>
                <div class="col-sm-7" id="Gender">
                    <span class="checkbox-switch-label-first"
                          ng-class="{disabled : userReg['gender'] != 1 || userReg['gender'] == 0}">Male</span>
                    <input class="checkboxSwitch" type="checkbox"
                           ng-model="userReg['gender']"
                           ng-true-value="1"
                           ng-false-value="0"
                           ng-checked="userReg['gender'] == 1"
                           id="inputSwitchGender"/><label class="label-checkboxSwitch"
                                                          for="inputSwitchGender"></label>
                    <span class="checkbox-switch-label"
                          ng-class="{disabled : userReg['gender'] == 1}">Female</span>
                </div>
            </div> -->
            <div class="form-group row">
                <label for="role" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.role"/></label>
                <div class="col-sm-7">
					<select id="role" ng-model="selectedRole" required="required"
						class="form-control custom-select custom-select-sm" ng-cloak
						ng-options="userRolesObj as userRolesObj.name for userRolesObj in userRolesList track by userRolesObj.userRoleId">
					</select>
				</div>
            </div>
            <div class="form-group row">
                <label for="cusId" class="col-sm-5 col-form-label text-left"><spring:message code ="cum.cus_id"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="cusId" ng-model="userReg.customerId" class="form-control" placeholder="<spring:message code ="cum.cus_id"/>" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="addressId" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.address"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="addressId" ng-model="userReg.address" class="form-control" placeholder="Address" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="phoneNum" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.phone_num"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="phoneNum" ng-model="userReg.phoneNum" class="form-control" placeholder="Phone number" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-5 col-form-label text-left"><spring:message code ="ru.email"/></label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="email" id="email" ng-model="userReg.email" class="form-control" placeholder="<spring:message code ="ru.email"/>" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-5 offset-4 text-center">
                    <button class="btn btn-outline-secondary" ng-cloak ng-if="formRegister.$invalid"><spring:message code ="cm.register"/></button>
                    <button class="btn btn-outline-success" ng-cloak ng-if="formRegister.$valid" ng-click="registerUser()"><spring:message code ="cm.register"/></button>
                </div>
            </div>
        </div>
    </div>
    <alert-message type-alert="mesObj.type" message-alert="mesObj.text" show-alert="mesObj.active"></alert-message>
    
</div>
<script src="<c:url value="/assets/js/registerUser.js"></c:url>"></script>
