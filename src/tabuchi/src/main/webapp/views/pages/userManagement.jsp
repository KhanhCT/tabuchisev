<%--
  Created by IntelliJ IDEA.
  User: tdchien
  Date: 6/20/2018
  Time: 3:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/css/userManagement.css"></c:url>">
<link rel="stylesheet" type="text/css" href="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.min.css"></c:url>">
<script>var contextPath = '${pageContext.servletContext.contextPath}';</script>
<script src="<c:url value="/assets/libs/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"></c:url>"></script>
<div class="container" ng-controller="userManagementCtrl" ng-init="initData()">
    <div class="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
        <div class="loader"></div>
    </div>
    <div class="row m-b-20">
        <div class="col-md-12 text-left">
            <h3><spring:message code="va.title_page"/></h3>
        </div>
    </div>
    <div class="row wrapper-content">
        <div class="col-md-5">
            <div class="wrap-form-management">
                <div class="loader-wrapper" ng-if="showLoaderUser" ng-cloak>
                    <div class="loader"></div>
                </div>
                <div class="form-group row">
                    <label for="userID" class="col-sm-5 col-form-label text-center"><spring:message code="ru.username"/></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text" id="userID" autocomplete="off" class="form-control" placeholder="<spring:message code="ru.username"/>" ng-model="selectedUserClone.userName" >
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="passWord" class="col-sm-5 col-form-label text-center"><spring:message code="ru.password"/></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="password" id="passWord" autocomplete="off" class="form-control" placeholder="<spring:message code="ru.password"/>" ng-model="selectedUserClone.passWord">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Gender" class="col-sm-5 col-form-label text-center"><spring:message code="cum.gender"/></label>
                    <div class="col-sm-7" id="Gender">
                    <span class="checkbox-switch-label-first"
                          ng-class="{disabled : selectedUserClone['gender'] != 1 || selectedUserClone['gender'] == 0}"><spring:message code="cum.male"/></span>
                        <input class="checkboxSwitch" type="checkbox"
                               ng-model="selectedUserClone['gender']"
                               ng-true-value="1"
                               ng-false-value="0"
                               ng-checked="selectedUserClone['gender'] == 1"
                               id="inputSwitchGender"/><label class="label-checkboxSwitch"
                                                              for="inputSwitchGender"></label>
                        <span class="checkbox-switch-label"
                              ng-class="{disabled : selectedUserClone['gender'] == 1}"><spring:message code="cum.female"/></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-5 col-form-label text-center"><spring:message code="ru.address"/></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text" id="address" class="form-control" placeholder="<spring:message code="ru.address"/>" ng-model="selectedUserClone.address" >
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phoneNumber" class="col-sm-5 col-form-label text-center"><spring:message code="ru.phone_num"/></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text" id="phoneNumber" class="form-control" placeholder="<spring:message code="ru.phone_num"/>" ng-model="selectedUserClone.phoneNum">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-5 col-form-label text-center"><spring:message code="ru.email"/></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="email" id="email" class="form-control" placeholder="<spring:message code="ru.email"/>" ng-model="selectedUserClone.email">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button class="btn btn-sm btn-success pull-right" ng-click="saveModifiUser()"><spring:message code="cm.modify"/></button>
                        <button class="btn btn-sm btn-danger m-r-15 pull-right" ng-click="clearModifiUser()"><spring:message code="cm.clear"/></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="wrap-table-config row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-md-12 row-custom table-custom-config-wrap">
                    <table ng-cloak ng-table="tableUserManagement"
                           class="table table-condensed table-bordered table-striped table-custom-config"
                           ng-class="{'nomarginbottom': tableUserManagement.total() == 0}">
                        <tr ng-repeat="row in $data">
                            <td data-title="'Username'" class="userName" header-class="'userName'" sortable="'userName'" >{{row.userName}}</td>
                            
                            <td data-title="'Address'" class="text-center" sortable="'address'">{{row.address}}</td>
                            <td data-title="'Phone number'" class="text-center" sortable="'phoneNum'">{{row.phoneNum}}</td>
                            <td data-title="'Email'" class="text-center" sortable="'email'">{{row.email}}</td>
                            <td width="100" style="text-align: center" data-title="'Action'">
                                <div class="btn btn-outline-primary" ng-click="modifiUser(row)">
                                    <span class="fa fa-pencil"></span>
                                </div>
                                <div class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal" ng-click="showDeleteUser(row)">
                                    <span class="fa fa-trash"></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="showNoData" ng-if="tableUserManagement.total() == 0" ng-cloak><spring:message code="label.no_data"/></div>
                </div>
            </div>
        </div>
    </div>
    <!--DELETE MEAL-->
    <div class="wrap-accept-delete">
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content ">
                    <div class="modal-header">
                        <h3 class="modal-title float-l" id="deleteModalLongTitle"><spring:message code="cm.comfirmation"/></h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <spring:message code="va.notice"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><spring:message code="cm.cancel"/></button>
                        <button type="button" class="btn btn-success" ng-click="validateDeleteUser()" data-dismiss="modal"><spring:message code="cm.accept"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value="/assets/js/userManagement.js"></c:url>"></script>