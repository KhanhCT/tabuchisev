<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/assets/css/photoGallery.css"></c:url>">
<link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<script>
	var contextPath = '${pageContext.servletContext.contextPath}';
</script>
<div class="container wrapper-item-master gallery-container"
	ng-controller="photoGalleryCtrl" ng-init="initializeData()">
	<div id="loader-wrapper" ng-if="showLoaderAllPage" ng-cloak>
		<div class="loader"></div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<span class="col-3 col-form-label  normal_label">Date</span>
				<div class="col-sm-9">
					<div class="input-group">
						<input type="text" id="toDate" class="form-control normal_label"
							placeholder="dd/mm/yyyy" ng-class="{'invalid-input': !toDate}"
							ng-model="selectedDate" date-picker
							aria-describedby="toDateAddon">
						<div class="input-group-append">
							<span class="input-group-text" id="toDateAddon"><i
								class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<label for="factoryID" class="col-3 col-form-label text-left">Factory</label>
				<div class="col-9">
					<select id="factoryID"
						class="form-control custom-select custom-select-sm"
						style="font-size: 16;" ng-model="selectedFactory"
						ng-change="factoryChanged()"
						ng-options="obj.factoryName for obj in factoryList ">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<div class="form-group row">
				<label for="lineID" class="col-3 col-form-label text-left">Line</label>
				<div class="col-9">
					<select id="lineID"
						class="form-control custom-select custom-select-sm"
						ng-model="selectedLine" style="font-size: 16;"
						ng-options="obj.lineDesc for obj in lineListFilter ">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4 col-xs-12 offset-xs-1">
			<div class="form-group row">
				<label for="shiftID" class="col-3 col-form-label text-left">Shift</label>
				<div class="col-9">
					<select id="shiftID"
						class="form-control custom-select custom-select-sm"
						ng-model="selectedShift" style="font-size: 16;"
						ng-options="obj.shiftName for obj in shiftList ">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-5">
		<div class="col-4 offset-4 col-xs-12 offset-xs-1">
			<div class="form-group row">
				<label for=""positionID"" class="col-3 col-form-label text-left">Position</label>
				<div class="col-9">
					<select id="positionID"
						class="form-control custom-select custom-select-sm"
						ng-model="selectedPos" style="font-size: 16;"
						ng-options="obj for obj in positionList">
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row m-t-5">
		<div class="col-4 offset-4">
			<button class="btn btn-warning custom-view col-12" type="button"
				ng-click="find()">Find</button>
		</div>
	</div>
	<div class="border m-t-20">
		<div class="tz-gallery">
			<div ng-repeat="slider in photoList" style="display: inline-block;">
				<div class="col-3">
					<div class="thumbnail">
						<div class="text-center">
							<a class="lightbox text-center" href="{{slider.url}}"> <img
								alt="Park" style="width: 120; height: 75" class="rounded"
								ng-click="changeZoomImage(slider, $event)"
								ng-src="{{slider.url}}" />
							</a>
						</div>
						<div class="caption">
							<h3>{{slider.captureDate}}</h3>
						</div>
					</div>
				</div>
			</div>
			<span ng-if="photoList.length == 0" class="text-center"> No data available!</span>
		</div>
	</div>

	<div class="wrap-all-page-preview" ng-if="showImageZoom" ng-cloak>
		<div id="cp-header" class="ais-group">
			<div id="cp-title-container" class="aui-item expanded">
				<div>
					<span class="cp-image-icon size-24 cp-file-icon"></span> <span
						ng-bind="imageZoomTitle"></span>
				</div>
			</div>
			<div id="cp-file-controls" class="aui-item">
				<span class="fv-close-button">
					<button role="button" ng-click="hidePreviewZoom($event)"
						id="cp-control-panel-close" href="#" class="cp-icon">Close</button>
				</span>
			</div>
		</div>
		<div id="cp-body" class="ais-group">
			<div id="cp-file-body" class="meta-banner">

				<div class="cp-viewer-layer">
					<div id="cp-image-preview">
						<div class="cp-scale-info" ng-show="isShowPercent" ng-cloak>
							<span ng-bind="zoomPercent"></span>%
						</div>
						<div class="cp-image-container">
							<img id="cp-img" ng-src="{{imageZoomURL}}" ng-style="styleZoom" />
						</div>
						<span class="cp-baseline-extension"></span>
					</div>
				</div>
				<div class="cp-toolbar cp-content">
					<button class="cp-toolbar-minus cp-icon" ng-click="zoomOut($event)">Zoom
						out</button>
					<button class="cp-toolbar-plus cp-icon" ng-click="zoomIn($event)">Zoom
						in</button>
				</div>
			</div>
		</div>
		<div id="cp-footer">
			<div id="cp-info">
				<div class="thumb-img" ng-repeat="slider in sliders"
					ng-click="changeZoomImage(slider, $event)">
					<div class="mask-thumb"></div>
					<img ng-src="{{slider.imageUrl}}" />
				</div>
			</div>
		</div>
	</div>

</div>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script src="<c:url value="/assets/js/photoGallery.js"></c:url>"></script>
<script>
	baguetteBox.run('.tz-gallery');
</script>
