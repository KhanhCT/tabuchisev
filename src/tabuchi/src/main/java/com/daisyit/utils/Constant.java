package com.daisyit.utils;

import org.springframework.beans.factory.annotation.Autowired;

import com.daisyit.backend.dao.SysvarDAO;

public class Constant {
	private static Constant instance;
	@Autowired
	private SysvarDAO sysvarDAO;
	//private String uploadFoler;
	public Constant()
	{
		//uploadFoler = sysvarDAO.getValueFromName("m_UploadDir");
	}
	public static Constant getInstance()
	{
		if(instance ==  null)
			instance = new Constant();
		return instance;
	}
	public String findUploadFolder()
	{
		return sysvarDAO.getValueFromName("m_UploadDir");
	}
	
}
