package com.daisyit.utils;


public interface ConstantKey {
	public static final String API_URL = "";
	public static final String USER_CODE = "UserCode";
	public static final String TOKEN = "TOKEN";
	public static final String LIMITTED_MONEY = "m_LiAmount";
	public static final String UPLOAD_FOLDER = "m_UploadDir";
	public static final String EXE_API_URL = "m_EXEApiUrl";
	public interface ROLE {
		public static final String ROLE_ADMIN = "Admin";
		public static final String ROLE_USER = "User";
	}
	public static final String CASH_TYPE = "VND";
	public static final String PAY_TYPE = "CARD";
	public interface DATE_FORMAT {
		public static final String DATE_DATA_FORMAT = "dd/MM/yyyy HH:mm:ss";
		public static final String DATE_SLASH_FORMAT = "dd/MM/yyyy";
		public static final String DATE_TIME_FORMAT = "ddMMyyyy";
		public static final String TIME_FORMAT = "HH:mm:ss";
	}

	public interface EXPORT_TYPE {
		public static final String TRANSACTION_LIST = "TransList";
		public static final String CASH_BALANCE = "CashBalance";
	}
	public interface ALLCODE
	{
		public static final String CUSTOMER_TYPE = "CUSTOMER_TYPE";
		public static final String TRANS_TYPE = "TRANS_TYPE";
		public static final String PRICE_UNIT = "PRICE_UNIT";
		public static final String PRCODE = "PRCODE";
		public static final String LOCATION_TYPE = "LOCATION_TYPE";
		public static final String EXE = "EXE";
		public static final String CAMPUS = "CAMPUS";
		public static final String USER = "USER";
		public static final String PASSWORD = "PASSWORD";
		public static final String PRCODE_DEFAULT = "PRCODE_DEFAULT";
		public static final String CURRENCY = "CURRENCY";
	}
	
	public interface CUSTOMER_TYPE
	{
		public static final String STUDENT = "01";
		public static final String TEACHER = "02";
		public static final String OTHER = "03";
	}
	public interface LOCATION_TYPE
	{
		public static final String CANTEEN = "01";
		public static final String CAMPUS = "02";
	}
	public interface TRANS_TYPE
	{
		public static final String TOP_UP = "01";
		public static final String ORDER = "02";
		public static final String TRASFER = "03";
		public static final String WITHDRAW = "04";
	}
	
	public interface RESPONSE_CODE{
		public static final Integer ERROR = 0;
		public static final Integer SUCCESS = 1;
		public static final Integer EXITS = 2;
	}
	public interface CACHE
	{
		public static final String USER_CACHE = "Users";
	}
	public interface REPORT_KEY
	{
		public static final String DATASOURCE="datasource"; 
		public static final String TITLE= "title";
		public static final String FROM_DATE="fromDate";
		public static final String TO_DATE = "toDate";
		public static final String CUR_DATE="curDate";
		public static final String CUSNAME="cusName";
		public static final String OWNER="owner";
		public static final String REPORT_DATE="reportDate";
		public static final String GRADE="grade";
		public static final String BALANCE="balance";
		public static final String TOTAL_CASH_OUT="totalCashOut";
		public static final String FORMAT="format"; 
		public interface CUSTRANS
		{
			public static final String TRANS_DATE = "transDate";
			public static final String TRANS_NUM = "transNum";
			public static final String QUANTITY = "quantity";
			public static final String CASH_IN = "cashIn";
			public static final String CASH_OUT = "cashOut";
			public static final String ACTION = "action";
		}
		public interface TRANS_TYPE{
			public static final String CASH_IN = "00";
			public static final String CASH_OUT="01";
			public static final String CASH_TRANSFER="02";
			public static final String CASH_WITHDRAW="03";
		}
		public interface TYPE{
			public static final int PDF = 0;
			public static final int CSV = 2;
			public static final int EXCEL = 1;
		}
	}
}
