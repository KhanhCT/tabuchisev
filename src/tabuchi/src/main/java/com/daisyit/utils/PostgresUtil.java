package com.daisyit.utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import com.daisyit.backend.model.Sysvar;

public class PostgresUtil {
    public static final String MSSQL_DRIVER="net.sourceforge.jtds.jdbc.Driver";
    public static final String MSSQL_ERROR_TAG = "QLError";
    public static final String MSSQL_SUCCESS_TAG = "SQLSuccess";
    public static Connection openConnection( List<Sysvar> envs)
    {
       
        String _dbAddress = "localhost";
        int _dbPort = 5432;
        String _dbName = "vte2";
        String _dbUserName = "tabuchi";
        String _dbPassword = "tabuchi";
        
        for(Sysvar env :envs){
            switch (env.getVarName().trim()){
	            case "HOSTNAME":
	            {
	            	_dbAddress = env.getVarValue();
	            }break;
                case "USER":
                {
                    _dbUserName = env.getVarValue();
                }break;
                case "PASSWORD":
                {
                    _dbPassword = env.getVarValue();
                }break;
                case "PORT":
                {
                    _dbPort = Integer.parseInt(env.getVarValue());
                }break;
                case "DATABASE":
                {
                	_dbName = env.getVarValue();
                }break;
               
                default:break;
            }
        }
        Connection _conn = null;
        String _connURL = null;
        try {
        	Class.forName("org.postgresql.Driver");
            _connURL = "jdbc:postgresql://" + _dbAddress + ":" + String.valueOf(_dbPort)+  "/" + _dbName ;
            _conn = DriverManager.getConnection(_connURL, _dbUserName, _dbPassword);
        }catch (SQLException e)
        {
        	e.printStackTrace();
        }catch (Exception e) {
        	e.printStackTrace();
		}
        return _conn;
    }
    
    public static void close(Connection conn)
    {
        try{
            if(conn !=null)
                conn.close();
        }catch (SQLException ex)
        {
        }
    }
}