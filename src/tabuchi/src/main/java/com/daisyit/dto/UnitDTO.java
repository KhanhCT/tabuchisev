package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Units;
import com.daisyit.backend.model.UnitsId;

public class UnitDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int unitId;
	private String unitCode;
	private String unitName;
	private Boolean unitUsage;
	private Boolean unitSKU;
	private Boolean disabled;
	public int getUnitId() {
		return unitId;
	}
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Boolean getUnitUsage() {
		return unitUsage;
	}
	public void setUnitUsage(Boolean unitUsage) {
		this.unitUsage = unitUsage;
	}
	public Boolean getUnitSKU() {
		return unitSKU;
	}
	public void setUnitSKU(Boolean unitSKU) {
		this.unitSKU = unitSKU;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public void model2DTO(Units unit) {
		this.unitId = unit.getId().getUnitId();
		this.unitCode = unit.getId().getUnitCode();
		this.unitName = unit.getUnitName();
		this.unitUsage = unit.getUnitUsage();
		this.unitSKU = unit.getUnitSku();
		this.disabled = unit.getDisabled();
	}
	public Units dto2Model() {
		Units unit = new Units();
		UnitsId id = new UnitsId();id.setUnitCode(this.unitCode);
		unit.setId(id);
		unit.setUnitName(this.unitName);
		unit.setUnitUsage(false);
		unit.setUnitSku(true);
		unit.setDisabled(false);
		return unit;
	}
	
	
}
