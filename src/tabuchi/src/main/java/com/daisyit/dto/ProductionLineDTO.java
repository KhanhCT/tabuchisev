package com.daisyit.dto;

import java.sql.Date;

import org.springlib.core.utils.DateTimeUtils;

import com.daisyit.backend.model.ProductionLine;
import com.daisyit.backend.model.ProductionLineId;

public class ProductionLineDTO {

	private int factoryId;
	private String factoryName;
	private int lineId;
	private String lineDesc;
	private int lineProdId;
	private String lineProdName;
	private int shiftId;
	private String shiftName;
	private String workingDate;
	private int orderedQty;
	private Boolean disabled;
	private int startTime;
	private int stopTime;
	private String orderRefNo;
	private String orderRefDate;
	private int goodProdQty;
	private int notGoodProdQty;
	
	public int getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(int fatoryId) {
		this.factoryId = fatoryId;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public int getLineId() {
		return lineId;
	}
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}
	public String getLineDesc() {
		return lineDesc;
	}
	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}
	public int getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(int lineProdId) {
		this.lineProdId = lineProdId;
	}
	public String getLineProdName() {
		return lineProdName;
	}
	public void setLineProdName(String lineProdName) {
		this.lineProdName = lineProdName;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public String getWorkingDate() {
		return workingDate;
	}
	public void setWorkingDate(String workingDate) {
		this.workingDate = workingDate;
	}
	public int getOrderedQty() {
		return orderedQty;
	}
	public void setOrderedQty(int orderedQty) {
		this.orderedQty = orderedQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getStopTime() {
		return stopTime;
	}
	public void setStopTime(int stopTime) {
		this.stopTime = stopTime;
	}
	public String getOrderRefNo() {
		return orderRefNo;
	}
	public void setOrderRefNo(String orderRefNo) {
		this.orderRefNo = orderRefNo;
	}
	public String getOrderRefDate() {
		return orderRefDate;
	}
	public void setOrderRefDate(String orderRefDate) {
		this.orderRefDate = orderRefDate;
	}
	public int getGoodProdQty() {
		return goodProdQty;
	}
	public void setGoodProdQty(int goodProdQty) {
		this.goodProdQty = goodProdQty;
	}
	public int getNotGoodProdQty() {
		return notGoodProdQty;
	}
	public void setNotGoodProdQty(int notGoodProdQty) {
		this.notGoodProdQty = notGoodProdQty;
	}
	public ProductionLine dto2Model() {
		java.util.Date date = DateTimeUtils.convertDateByFormatLocal(this.workingDate, "yyyy-MM-dd");
		ProductionLineId id = new ProductionLineId(this.factoryId, this.lineId, date, this.shiftId);
		ProductionLine line = new ProductionLine();
		line.setId(id);
		line.setOrderedQty(this.orderedQty);
		line.setLineProdId(this.lineProdId);
		line.setDisabled(false);
		line.setGoodProdQty(0);
		
		return line;
	}
	
}
