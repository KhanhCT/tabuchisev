package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.LineProdProcess;
import com.daisyit.backend.model.LineProdProcessId;

public class LineProcessDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lineProdId;
	private String lineProdCode;
	private String lineProdName;
	private int processId;
	private String processName;
	private Integer minSt;
	private Integer maxSt;
	private String standardVideoLink;
	private String workVideoLink;
	private Integer wplaceQty;
	private Boolean disabled;
	public int getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(int lineProdId) {
		this.lineProdId = lineProdId;
	}
	public int getProcessId() {
		return processId;
	}
	
	public String getLineProdCode() {
		return lineProdCode;
	}
	public void setLineProdCode(String lineProdCode) {
		this.lineProdCode = lineProdCode;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public Integer getMinSt() {
		return minSt;
	}
	public void setMinSt(Integer minSt) {
		this.minSt = minSt;
	}
	public Integer getMaxSt() {
		return maxSt;
	}
	public void setMaxSt(Integer maxSt) {
		this.maxSt = maxSt;
	}
	public String getStandardVideoLink() {
		return standardVideoLink;
	}
	public void setStandardVideoLink(String standardVideoLink) {
		this.standardVideoLink = standardVideoLink;
	}
	public String getWorkVideoLink() {
		return workVideoLink;
	}
	public void setWorkVideoLink(String workVideoLink) {
		this.workVideoLink = workVideoLink;
	}
	public Integer getWplaceQty() {
		return wplaceQty;
	}
	public void setWplaceQty(Integer wplaceQty) {
		this.wplaceQty = wplaceQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public String getLineProdName() {
		return lineProdName;
	}
	public void setLineProdName(String lineProdName) {
		this.lineProdName = lineProdName;
	}
	public void model2DTO(LineProdProcess process) {
		this.lineProdId = process.getId().getLineProdId();
		this.processId = process.getId().getProcessId();
		this.processName = process.getProcessName();
		this.minSt = process.getMinSt();
		this.maxSt = process.getMaxSt();
		this.standardVideoLink = process.getStandardVideoLink();
		this.workVideoLink = process.getWorkVideoLink();
		this.wplaceQty = process.getWplaceQty();
		this.disabled = process.getDisabled();
	}
	
	public LineProdProcess dto2Model() {
		LineProdProcess process = new LineProdProcess();
		LineProdProcessId id = new LineProdProcessId();
		id.setLineProdId(this.lineProdId);
		id.setProcessId(this.processId);
		process.setId(id);
		process.setProcessName(this.processName);
		process.setMinSt(this.minSt);
		process.setMaxSt(this.maxSt);
		process.setStandardVideoLink(this.standardVideoLink);
		process.setWorkVideoLink(this.workVideoLink);
		process.setWplaceQty(this.wplaceQty);
		process.setDisabled(this.disabled);
		return process;
	}

}
