package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.LineProdBom;
import com.daisyit.backend.model.LineProdBomId;

public class ProductBOMDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lineProdId;
	private String lineProdCode;
    private int materialId;
    private String materialCode;
    private Integer lineSkuunitId;
    private Double lineSkuQty;
    private Integer mateUsageUnitId;
    private Double mateUsageQty;
    private Double mateSkuQty;
    private Boolean disabled;
	public int getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(int lineProdId) {
		this.lineProdId = lineProdId;
	}
	public int getMaterialId() {
		return materialId;
	}
	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}
	public Integer getLineSkuunitId() {
		return lineSkuunitId;
	}
	public void setLineSkuunitId(Integer lineSkuunitId) {
		this.lineSkuunitId = lineSkuunitId;
	}
	public Double getLineSkuQty() {
		return lineSkuQty;
	}
	public void setLineSkuQty(Double lineSkuQty) {
		this.lineSkuQty = lineSkuQty;
	}
	public Integer getMateUsageUnitId() {
		return mateUsageUnitId;
	}
	public void setMateUsageUnitId(Integer mateUsageUnitId) {
		this.mateUsageUnitId = mateUsageUnitId;
	}
	public Double getMateUsageQty() {
		return mateUsageQty;
	}
	public void setMateUsageQty(Double mateUsageQty) {
		this.mateUsageQty = mateUsageQty;
	}
	public Double getMateSkuQty() {
		return mateSkuQty;
	}
	public void setMateSkuQty(Double mateSkuQty) {
		this.mateSkuQty = mateSkuQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public String getLineProdCode() {
		return lineProdCode;
	}
	public void setLineProdCode(String lineProdCode) {
		this.lineProdCode = lineProdCode;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public LineProdBom dto2Model() {
		LineProdBom bom = new LineProdBom();
		LineProdBomId id  = new LineProdBomId();
		id.setLineProdId(this.lineProdId);
		id.setMaterialId(this.materialId);
		bom.setId(id);
		bom.setLineSkuunitId(this.lineSkuunitId);
		bom.setLineSkuqty(this.lineSkuQty);
		bom.setMateUsageUnitId(this.mateUsageUnitId);
		bom.setMateUsageQty(this.mateUsageQty);
		bom.setMateSkuqty(this.mateSkuQty);
		bom.setDisabled(this.getDisabled());
		return bom;
	}
	public void mode2DTO(LineProdBom bom) {
		this.lineProdId = bom.getId().getLineProdId();
		this.materialId = bom.getId().getMaterialId();
		this.lineSkuunitId = bom.getLineSkuunitId();
		this.lineSkuQty = bom.getLineSkuqty();
		this.mateUsageUnitId = bom.getMateUsageUnitId();
		this.mateUsageQty = bom.getMateSkuqty();
		this.mateSkuQty = bom.getMateSkuqty();
		this.disabled = bom.getDisabled();
	}
    
}
