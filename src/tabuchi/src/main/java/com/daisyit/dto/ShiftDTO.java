package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Shift;

public class ShiftDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shiftCode;
	private String shiftName;
	private Integer startTime;
	private Integer stopTime;
	private Boolean disabled;

	private int factoryId;
	private int shiftId;
	public String getShiftCode() {
		return shiftCode;
	}
	public void setShiftCode(String shiftCode) {
		this.shiftCode = shiftCode;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public Integer getStartTime() {
		return startTime;
	}
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	public Integer getStopTime() {
		return stopTime;
	}
	public void setStopTime(Integer stopTime) {
		this.stopTime = stopTime;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public int getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(int factoryId) {
		this.factoryId = factoryId;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	
	public void model2DTO(Shift shift) {
		this.factoryId = shift.getId().getFactoryId();
		this.shiftId = shift.getId().getShiftId();
		this.shiftName = shift.getShiftName();
	}

}
