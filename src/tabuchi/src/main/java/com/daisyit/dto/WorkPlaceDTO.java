package com.daisyit.dto;

import com.daisyit.backend.model.WorkPlace;
import com.daisyit.backend.model.WorkPlaceId;

public class WorkPlaceDTO {
	
	private int factoryId;
	private String factoryCode;
	private String factoryName;
	private int lineId;
	private String lineTagCode;
	private String lineDesc;
	private int processId;
	private String processCode;
	private String processName;
	private String description;
	private String ipv4Cam;
	private String ipv4Tab;
	private Boolean disabled;
	private int wPlaceId;
	
	public int getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(int factoryId) {
		this.factoryId = factoryId;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public int getLineId() {
		return lineId;
	}
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}
	public String getLineTagCode() {
		return lineTagCode;
	}
	public void setLineTagCode(String lineTagCode) {
		this.lineTagCode = lineTagCode;
	}
	public String getLineDesc() {
		return lineDesc;
	}
	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}
	public int getProcessId() {
		return processId;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public String getProcessCode() {
		return processCode;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIpv4Cam() {
		return ipv4Cam;
	}
	public void setIpv4Cam(String ipv4Cam) {
		this.ipv4Cam = ipv4Cam;
	}
	public String getIpv4Tab() {
		return ipv4Tab;
	}
	public void setIpv4Tab(String ipv4Tab) {
		this.ipv4Tab = ipv4Tab;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public int getwPlaceId() {
		return wPlaceId;
	}
	public void setwPlaceId(int wPlaceId) {
		this.wPlaceId = wPlaceId;
	}
	
	public String getFactoryCode() {
		return factoryCode;
	}
	public void setFactoryCode(String factoryCode) {
		this.factoryCode = factoryCode;
	}
	public WorkPlace dto2Model() {
		WorkPlace w = new WorkPlace();
		WorkPlaceId id = new WorkPlaceId();
		id.setFactoryId(this.factoryId);
		id.setLineId(this.lineId);
		id.setWplaceId(this.wPlaceId);
		w.setId(id);
		w.setDescription(this.description);
		w.setIpv4Cam(this.ipv4Cam);
		w.setIpv4Tab(this.getIpv4Tab());
		w.setProcessId(this.processId);
		w.setDisabled(this.disabled);
		return w;
	}
	public void model2DTO(WorkPlace wPlace) {
		this.factoryId = wPlace.getId().getFactoryId();
		this.lineId = wPlace.getId().getLineId();
		this.wPlaceId = wPlace.getId().getWplaceId();
		this.description = wPlace.getDescription();
		this.processId = wPlace.getProcessId();
		this.ipv4Cam = wPlace.getIpv4Cam();
		this.ipv4Tab = wPlace.getIpv4Tab();
		this.disabled = wPlace.getDisabled();
	}
}
