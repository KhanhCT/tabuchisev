package com.daisyit.dto;

import java.io.Serializable;

public class ProdImgDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String captureDate;
	private String captureTime;
	private String workerName;
	private int wPlaceID;
	
	private String url;
	public String getCaptureDate() {
		return captureDate;
	}
	public void setCaptureDate(String captureDate) {
		this.captureDate = captureDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public String getCaptureTime() {
		return captureTime;
	}
	public void setCaptureTime(String captureTime) {
		this.captureTime = captureTime;
	}
	
	
	public String getWorkerName() {
		return workerName;
	}
	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}
	public int getwPlaceID() {
		return wPlaceID;
	}
	public void setwPlaceID(int wPlaceID) {
		this.wPlaceID = wPlaceID;
	}
	public ProdImgDTO(String captureDate, String url, String captureTime) {
		super();
		this.captureDate = captureTime + "-" + captureDate;
		this.url = url;
		this.captureTime = captureTime;
	}
	
	

}
