package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.LineProds;
import com.daisyit.backend.model.LineProdsId;

public class LineProdsDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lineProdId;
    private String lineProdCode;
	private String barcode;
    private String lineProdName;
    private Integer skuUnitID;
    private Integer processQty;
    private Integer wplaceQty;
    private Boolean disabled;
    private String skuUnitName;
    
	public int getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(int lineProdId) {
		this.lineProdId = lineProdId;
	}
	public String getLineProdCode() {
		return lineProdCode;
	}
	public void setLineProdCode(String lineProdCode) {
		this.lineProdCode = lineProdCode;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getLineProdName() {
		return lineProdName;
	}
	public void setLineProdName(String lineProdName) {
		this.lineProdName = lineProdName;
	}
	public Integer getSkuUnitID() {
		return skuUnitID;
	}
	public void setSkuUnitID(Integer skuUnitID) {
		this.skuUnitID = skuUnitID;
	}
	public Integer getProcessQty() {
		return processQty;
	}
	public void setProcessQty(Integer processQty) {
		this.processQty = processQty;
	}
	public Integer getWplaceQty() {
		return wplaceQty;
	}
	public void setWplaceQty(Integer wplaceQty) {
		this.wplaceQty = wplaceQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	
	public String getSkuUnitName() {
		return skuUnitName;
	}
	public void setSkuUnitName(String skuUnitName) {
		this.skuUnitName = skuUnitName;
	}
	public void model2DTO(LineProds lineProds) {
		this.lineProdId = lineProds.getId().getLineProdId();
		this.lineProdCode = lineProds.getId().getLineProdCode();
		this.barcode = lineProds.getBarcode();
		this.lineProdName = lineProds.getLineProdName();
		this.skuUnitID = lineProds.getSkuunitId();
		this.processQty = lineProds.getProcessQty();
		this.wplaceQty = lineProds.getWplaceQty();
		this.disabled = lineProds.getDisabled();
	}
	
	public LineProds dto2Model() {
		LineProds lineProds = new LineProds();
		LineProdsId id = new LineProdsId();
		id.setLineProdCode(this.lineProdCode);
		id.setLineProdId(this.lineProdId);
		lineProds.setId(id);
		lineProds.setBarcode(this.barcode);
		lineProds.setLineProdName(this.lineProdName);
		lineProds.setSkuunitId(this.skuUnitID);
		lineProds.setProcessQty(this.processQty);
		lineProds.setWplaceQty(this.wplaceQty);
		this.setDisabled(this.disabled);
		return lineProds;
	}
    
    
}
