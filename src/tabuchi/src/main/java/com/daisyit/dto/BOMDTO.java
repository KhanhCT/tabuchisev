package com.daisyit.dto;

public class BOMDTO {
	private String prodBarcode;
	private int processIdx;
	private String matBarcode;
	public String getProdBarcode() {
		return prodBarcode;
	}
	public void setProdBarcode(String prodBarcode) {
		this.prodBarcode = prodBarcode;
	}
	public int getProcessIdx() {
		return processIdx;
	}
	public void setProcessIdx(int processIdx) {
		this.processIdx = processIdx;
	}
	public String getMatBarcode() {
		return matBarcode;
	}
	public void setMatBarcode(String matBarcode) {
		this.matBarcode = matBarcode;
	}
	
}
