package com.daisyit.dto;

import com.daisyit.backend.model.Material;
import com.daisyit.backend.model.MaterialId;

public class MaterialDTO  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int materialId;
    private String materialCode;
    private String barcode;
    private String materialName;
    private Integer usageUnitId;
    private Integer skuunitId;
    private Double skuusageQty;
    private Boolean isLineProd;
    private String productCode;
    private Integer lineProdId;
    private Boolean disabled;
    public MaterialDTO() {}
    public MaterialDTO(String materialCode, String materialName, String barcode) {
    	this.materialCode = materialCode;
    	this.materialName = materialName;
    	this.barcode = barcode;
    	this.disabled = false;
    }
	public int getMaterialId() {
		return materialId;
	}
	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public Integer getUsageUnitId() {
		return usageUnitId;
	}
	public void setUsageUnitId(Integer usageUnitId) {
		this.usageUnitId = usageUnitId;
	}
	public Integer getSkuunitId() {
		return skuunitId;
	}
	public void setSkuunitId(Integer skuunitId) {
		this.skuunitId = skuunitId;
	}
	public Double getSkuusageQty() {
		return skuusageQty;
	}
	public void setSkuusageQty(Double skuusageQty) {
		this.skuusageQty = skuusageQty;
	}
	public Boolean getIsLineProd() {
		return isLineProd;
	}
	public void setIsLineProd(Boolean isLineProd) {
		this.isLineProd = isLineProd;
	}
	public Integer getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(Integer lineProdId) {
		this.lineProdId = lineProdId;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Material dto2Model() {
		Material material = new Material();
		MaterialId id = new MaterialId();
		id.setMaterialCode(this.materialCode);
		id.setBarcode(this.barcode);
		material.setId(id);
		material.setMaterialName(this.materialName);
		material.setUsageUnitId(this.usageUnitId);
		material.setSkuunitId(this.skuunitId);
		material.setSkuusageQty(this.skuusageQty);
		material.setIsLineProd(this.isLineProd);
		material.setLineProdId(this.lineProdId);
		material.setDisabled(false);
		return material;
	}
	public void model2DTO(Material material) {
		this.materialId = material.getId().getMaterialId();
		this.materialCode = material.getId().getMaterialCode();
		this.barcode = material.getId().getBarcode();
		this.materialName = material.getMaterialName();
		this.usageUnitId = material.getUsageUnitId();
		this.skuusageQty = material.getSkuusageQty();
		this.skuunitId = material.getSkuunitId();
		this.isLineProd = material.getIsLineProd();
		this.lineProdId = material.getLineProdId();
		this.disabled = material.getDisabled();
	}
	
	
    
}
