package com.daisyit.dto;

import com.daisyit.backend.model.Process;
import com.daisyit.backend.model.ProcessId;

public class ProcessDTO {
	private int processIdx;
	private String processCode;
	private String processName;
	private String processDoc;
	private Boolean disabled;
	public int getProcessIdx() {
		return processIdx;
	}
	public void setProcessIdx(int processIdx) {
		this.processIdx = processIdx;
	}
	public String getProcessCode() {
		return processCode;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getProcessDoc() {
		return processDoc;
	}
	public void setProcessDoc(String processDoc) {
		this.processDoc = processDoc;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public Process dto2Model() {
		Process p = new Process();
		ProcessId id = new ProcessId();
		id.setProcessId(this.getProcessIdx());
		id.setProcessCode(this.getProcessCode());
		p.setId(id);
		p.setProcessName(this.getProcessName());
		p.setProcessDoc(this.processDoc);
		p.setDisabled(this.disabled);
		return p;
	}
	public void model2DTO(Process p) {
		this.processIdx = p.getId().getProcessId();
		this.processCode = p.getId().getProcessCode();
		this.processName = p.getProcessName();
		this.processDoc = p.getProcessDoc();
		this.disabled = p.getDisabled();
	}
}
