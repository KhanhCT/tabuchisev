package com.daisyit.dto;

import java.io.Serializable;

public class BaseDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Object key;
	private Object value;
	private Object otherValue;
	public Object getKey() {
		return key;
	}
	public void setKey(Object key) {
		this.key = key;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public BaseDTO(Object key, Object value) {
		super();
		this.key = key;
		this.value = value;
	}
	
	public BaseDTO(Object key, Object value, Object otherValue) {
		super();
		this.key = key;
		this.value = value;
		this.otherValue = otherValue;
	}
	public Object getOtherValue() {
		return otherValue;
	}
	public void setOtherValue(Object otherValue) {
		this.otherValue = otherValue;
	}
	
	
	
}
