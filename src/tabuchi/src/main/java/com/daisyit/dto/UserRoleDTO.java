package com.daisyit.dto;

import com.daisyit.backend.model.UserRoles;

public class UserRoleDTO {
	private int userRoleId;
    private String name;
    private boolean status;
	public int getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public void model2DTO(UserRoles role)
	{
		this.userRoleId = role.getId().getRoleId();
		this.name = role.getRoleDesc();
		this.status = role.getDisabled();
	}
    
}
