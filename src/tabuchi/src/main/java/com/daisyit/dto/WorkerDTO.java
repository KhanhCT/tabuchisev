package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Worker;
import com.daisyit.backend.model.WorkerId;

public class WorkerDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int workerId;
    private String workerCode;
    private String workerName;
    private String barcode;
    private Boolean disabled;


	public int getWorkerId() {
		return workerId;
	}
	public void setWorkerId(int workerId) {
		this.workerId = workerId;
	}
	public String getWorkerCode() {
		return workerCode;
	}
	public void setWorkerCode(String workerCode) {
		this.workerCode = workerCode;
	}
	public String getWorkerName() {
		return workerName;
	}
	
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}
	public void model2DTO(Worker worker){
    	this.workerId = worker.getId().getWorkerId();
    	this.workerCode = worker.getId().getWorkerCode();
    	this.workerName = worker.getWorkerName();
    	this.barcode = worker.getBarcode();
    	this.disabled = worker.getDisabled();
    }
	public Worker dto2Model(){
		Worker worker = new Worker();
		WorkerId id = new WorkerId();
		id.setWorkerId(this.workerId);
		id.setWorkerCode(this.workerCode);
		worker.setId(id);
		worker.setWorkerName(this.workerName);
		worker.setBarcode(this.barcode);
		worker.setDisabled(this.disabled);
		return worker;
	}
}
