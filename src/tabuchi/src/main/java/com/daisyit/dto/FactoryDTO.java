package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.Factory;
import com.daisyit.backend.model.FactoryId;

public class FactoryDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int factoryId;
    private String factoryCode;
	private String factoryName;
    private Integer shiftQty;
    private Boolean disabled;
    private String status;
	public int getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(int factoryId) {
		this.factoryId = factoryId;
	}
	public String getFactoryCode() {
		return factoryCode;
	}
	public void setFactoryCode(String factoryCode) {
		this.factoryCode = factoryCode;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public Integer getShiftQty() {
		return shiftQty;
	}
	public void setShiftQty(Integer shiftQty) {
		this.shiftQty = shiftQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void model2DTO(Factory factory) {
		this.factoryId = factory.getId().getFactoryId();
		this.factoryCode = factory.getId().getFactoryCode();
		this.factoryName = factory.getFactoryName();
		this.shiftQty = factory.getShiftQty();
		this.disabled = factory.getDisabled();
		if(this.disabled)
			this.status = "Disabled";
		else
			this.status = "Active";
	}
	
	public Factory dto2Model() {
		Factory factory = new Factory();
		FactoryId id = new FactoryId();
		id.setFactoryCode(this.factoryCode);
		id.setFactoryId(this.factoryId);
		factory.setId(id);
		factory.setFactoryName(this.factoryName);
		factory.setShiftQty(this.shiftQty);
		factory.setDisabled(this.getDisabled());
		return factory;
	}
    
}
