package com.daisyit.dto;

import java.io.Serializable;

import com.daisyit.backend.model.ProdLine;
import com.daisyit.backend.model.ProdLineId;

public class LineDTO  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int factoryId;
	private int lineId;
	private int lineProdId;
	private String lineProdCode;
	private String lineProdName;
	private String factoryName;
	private String factoryCode;
	private String lineTagCode;
	private String lineDesc;
	private Integer wplaceQty;
	private Boolean disabled;
	private String status;
	public int getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(int factoryId) {
		this.factoryId = factoryId;
	}
	public int getLineId() {
		return lineId;
	}
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}
	public int getLineProdId() {
		return lineProdId;
	}
	public void setLineProdId(int lineProdId) {
		this.lineProdId = lineProdId;
	}
	
	public String getLineProdCode() {
		return lineProdCode;
	}
	public void setLineProdCode(String lineProdCode) {
		this.lineProdCode = lineProdCode;
	}
	public String getLineTagCode() {
		return lineTagCode;
	}
	public void setLineTagCode(String lineTagCode) {
		this.lineTagCode = lineTagCode;
	}
	public String getLineDesc() {
		return lineDesc;
	}
	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}
	public Integer getWplaceQty() {
		return wplaceQty;
	}
	public void setWplaceQty(Integer wplaceQty) {
		this.wplaceQty = wplaceQty;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public String getLineProdName() {
		return lineProdName;
	}
	public void setLineProdName(String lineProdName) {
		this.lineProdName = lineProdName;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getFactoryCode() {
		return factoryCode;
	}
	public void setFactoryCode(String factoryCode) {
		this.factoryCode = factoryCode;
	}
	public void model2DTO(ProdLine line) {
		this.factoryId = line.getId().getFactoryId();
		this.lineId = line.getId().getLineId();
		this.lineTagCode = line.getLineTagCode();
		this.lineDesc = line.getLineDesc();
		this.wplaceQty = line.getWplaceQty();
		this.disabled = line.getDisabled();
		if(this.disabled)
			this.status = "Disabled";
		else
			this.status = "Active";
	}
	
	public ProdLine dto2Model() {
		ProdLine line = new ProdLine();
		ProdLineId id = new ProdLineId();
		id.setFactoryId(this.factoryId);
		id.setLineId(this.lineId);
		line.setId(id);
		line.setLineTagCode(this.lineTagCode);
		line.setLineDesc(this.lineDesc);
		line.setWplaceQty(this.wplaceQty);
		line.setDisabled(this.disabled);
		return line;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
