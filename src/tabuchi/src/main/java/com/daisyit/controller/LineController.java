package com.daisyit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daisyit.dto.BaseDTO;
import com.daisyit.dto.FactoryDTO;
import com.daisyit.dto.LineDTO;
import com.daisyit.dto.LineProcessDTO;
import com.daisyit.dto.ProdImgDTO;
import com.daisyit.dto.ProductionLineDTO;
import com.daisyit.dto.ShiftDTO;
import com.daisyit.dto.WorkPlaceDTO;
import com.daisyit.frontend.service.LineService;


@RestController
@RequestMapping("/api/v0.1/LineController")
public class LineController {
	@Autowired
	private LineService lineService;
	/*************************************Product BOM**********************/
	@RequestMapping(value = "newProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newProcess(@RequestBody LineProcessDTO dto) {
		return lineService.newProcess(dto);
	}
	@RequestMapping(value = "updateProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateProcess(@RequestBody LineProcessDTO dto) {
		return lineService.updateProcess(dto);
	}
	@RequestMapping(value = "findProcess", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public LineProcessDTO findProcess(@RequestParam(value="lineProdCode") String lineProdCode,@RequestParam(value="processId")  int processId) {
		return lineService.findProcess(lineProdCode, processId);
	}
	@RequestMapping(value = "deleteProcess/{lineProdCode}/{processId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteProcess(@PathVariable String lineProdCode, @PathVariable int processId) {
		return lineService.deleteProcess(lineProdCode, processId);
	}
	
	@RequestMapping(value = "findAllFactory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FactoryDTO> findAllFactory() {
		return lineService.findAllFactory();
	}
	
	@RequestMapping(value = "findAllProcess", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LineProcessDTO> findAllProcess(@RequestParam(value="lineProdId") int lineProdId, @RequestParam(value="processId") int processId) {
		return lineService.findAllProcess(lineProdId, processId);
	}
	/*********************** Production Line *************************/
	@RequestMapping(value = "newLine", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newLine(@RequestBody LineDTO dto) {
		return lineService.newLine(dto);
	}
	@RequestMapping(value = "updateLine", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateLine(@RequestBody LineDTO dto) {
		return lineService.updateLine(dto);
	}
	@RequestMapping(value = "findLine", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public LineDTO findLine(@RequestParam(value="factoryId") int factoryId,@RequestParam(value="lineId")  int lineId) {
		return lineService.findLine(factoryId, lineId);
	}
	@RequestMapping(value = "findAllProdLine", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LineDTO> findAllProdLine(@RequestParam(value="factoryId") int factoryId,@RequestParam(value="lineId")  int lineId) {
		return lineService.findAllLine(factoryId, lineId);
	}
	@RequestMapping(value = "findAllLine", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LineDTO> findAllLine(@RequestParam(value="factoryId") int factoryId) {
		return lineService.findLine(factoryId);
	}
	
	@RequestMapping(value = "deleteLine/{factoryId}/{lineId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteLine(@PathVariable int factoryId, @PathVariable int lineId) {
		return lineService.deleteLine(factoryId, lineId);
	}

	@RequestMapping(value = "getMonthyLineProductivity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BaseDTO> getMonthyLineProductivity(@RequestParam(value="date") String curDate, @RequestParam(value="lineId")int lineId, @RequestParam(value="factoryId")int factoryId)
	{
		List<BaseDTO> dtos =  lineService.getMonthyLineProductivity(curDate, lineId, factoryId);
		return dtos;
	}
	@RequestMapping(value = "getMonthyFactoryProductivity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BaseDTO> getMonthyFactoryProductivity(@RequestParam(value="date") String curDate,  @RequestParam(value="factoryId")int factoryId)
	{
		List<BaseDTO> dtos =  lineService.getFactoryData(curDate, factoryId);
		return dtos;
	}
	@RequestMapping(value = "findAllPhotos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProdImgDTO> findAllPhotos(@RequestParam(value="workingDate") String workingDate,  @RequestParam(value="factoryId")int factoryId, @RequestParam(value="lineId")int lineId, @RequestParam(value="shiftId")int shiftId, @RequestParam(value="position")int position)
	{	
		return lineService.findAllPhoto(workingDate, factoryId, lineId, shiftId, position);
	}
	
	@RequestMapping(value = "findAllShifts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ShiftDTO> findAllShifts(@RequestParam(value="factoryId")int factoryId)
	{	
		return lineService.findAllShifts(factoryId);
	}
	
	@RequestMapping(value = "findAllWorkPlaces", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<WorkPlaceDTO> findAllWorkPlaces(@RequestParam(value="factoryId")int factoryId, @RequestParam(value="lineId")int lineId,@RequestParam(value="wPlaceId")int wPlaceId)
	{	
		return lineService.findAllWorkPlaces(factoryId,lineId, wPlaceId);
	}
	@RequestMapping(value = "updateWorkPlace", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateWorkPlace(@RequestBody WorkPlaceDTO dto) {
		return lineService.updateWorkPlace(dto);
	}
	@RequestMapping(value = "deleteWorkPlace/{factoryId}/{lineId}/{wPlaceId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteWorkPlace(@PathVariable int factoryId, @PathVariable int lineId, @PathVariable int wPlaceId) {
		return lineService.deleteWorkPlace(factoryId, lineId, wPlaceId);
	}
	@RequestMapping(value = "newWorkPlace", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newWorkPlace(@RequestBody WorkPlaceDTO dto) {
		return lineService.newWorkPlace(dto);
	}
	
	@RequestMapping(value = "findAllProductionPlan", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProductionLineDTO> findProductionPlan(@RequestParam(value="workingDate") String workingDate){
		return lineService.findProductionPlan(workingDate);
	}
	@RequestMapping(value = "newProductionPlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newProductionPlan(@RequestBody ProductionLineDTO dto) {
		return lineService.newProductionPlan(dto);
	}
	@RequestMapping(value = "updateProductionPlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateProductionPlan(@RequestBody ProductionLineDTO dto) {
		return lineService.updateProductionPlan(dto);
	}
	@RequestMapping(value = "deleteProductionPlan/{factoryId}/{lineId}/{shiftId}/{workingDate}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteProductionPlan(@PathVariable int factoryId,@PathVariable int lineId, @PathVariable String workingDate, @PathVariable int shiftId) {
		return lineService.deleteProductionPlan(factoryId, lineId, workingDate, shiftId);
	}
	
}
