package com.daisyit.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springlib.core.utils.CommonKey;

import com.daisyit.dto.UserDTO;
import com.daisyit.frontend.service.UserService;

@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "authentication", method = RequestMethod.POST)
	public ModelAndView doAuthentication(@RequestParam("username") String username,
			@RequestParam("password") String password, HttpServletRequest request) {
		ModelAndView model = null;
		request.getSession().removeAttribute(CommonKey.USER_ROLE_VALUE);
		request.getSession().removeAttribute(CommonKey.USER_ROLE_ID);
		request.getSession().removeAttribute(CommonKey.USER_ID);
//		String _password = Utils.Base64Encoder(password);
		UserDTO userDTO = userService.getUserByNameAndPass(username, password);
		if (userDTO != null) {
			request.getSession().setAttribute(CommonKey.USER_ID, userDTO.getUserId());
			Integer userRoleId = userDTO.getUserRoleId();
			request.getSession().setAttribute(CommonKey.USER_ROLE_ID, userRoleId);
			String userRole = userService.getUserRole(userRoleId);
			request.getSession().setAttribute(CommonKey.USER_ROLE_VALUE, userRole);
			if (userRole.equals(CommonKey.ROLE.ROLE_ADMIN)) {
				model = new ModelAndView("redirect:photoGallery");
			} else if (userRole.equals(CommonKey.ROLE.ROLE_USER)) {
				model = new ModelAndView("redirect:photoGallery");
			} else {
				model = new ModelAndView("loginPage");
				model.addObject("message", "error");
			}
		} else {
			//model = new ModelAndView("loginPage");
			model = new ModelAndView("loginPage");
			model.addObject("message", "error");
		}
		return model;
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request)
	{
		request.getSession().removeAttribute(CommonKey.USER_ROLE_VALUE);
		request.getSession().removeAttribute(CommonKey.USER_ROLE_ID);
		request.getSession().removeAttribute(CommonKey.USER_ID);
		ModelAndView model  = new ModelAndView("loginPage");
		model.addObject("message", "login");
		return model;
	}

}