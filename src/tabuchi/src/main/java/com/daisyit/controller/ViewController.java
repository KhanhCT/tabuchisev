package com.daisyit.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class ViewController {
    @RequestMapping("login")
    public ModelAndView login()
    {
        ModelAndView m = new ModelAndView("loginPage");
        return m;
    }
   
    /***************Item**************************/
    @RequestMapping("materialMaster")
    public ModelAndView itemMaster()
    {
        ModelAndView m = new ModelAndView("materialMaster");
        m.addObject("menuObj","material");
        return m;
    }
    @RequestMapping("lineProdMaster")
    public ModelAndView lineProdMaster()
    {
        ModelAndView m = new ModelAndView("lineProdMaster");
        m.addObject("menuObj","material");
        return m;
    }
    @RequestMapping("lineProdBOM")
    public ModelAndView lineProdBOM()
    {
        ModelAndView m = new ModelAndView("lineProdBOM");
        m.addObject("menuObj","material");
        return m;
    }
    @RequestMapping("processProdBOM")
    public ModelAndView processProdBOM()
    {
        ModelAndView m = new ModelAndView("processProdBOM");
        m.addObject("menuObj","material");
        return m;
    }
    
    @RequestMapping("unitMaster")
    public ModelAndView unitMaster()
    {
        ModelAndView m = new ModelAndView("unitMaster");
        m.addObject("menuObj","unitMaster");
        return m;
    }
    
    @RequestMapping("processMaster")
    public ModelAndView processMaster()
    {
        ModelAndView m = new ModelAndView("processMaster");
        m.addObject("menuObj","processMaster");
        return m;
    }
    
    /***************User**************************/
    @RequestMapping("registerUser")
    public ModelAndView registerUser()
    {
        ModelAndView m = new ModelAndView("registerUser");
        m.addObject("menuObj","user");
        return m;
    }

    @RequestMapping("userManagement")
    public ModelAndView userManagement()
    {
        ModelAndView m = new ModelAndView("userManagement");
        m.addObject("menuObj","user");
        return m;
    }
    /***************Line Master************************/
    @RequestMapping("lineProcess")
    public ModelAndView lineProcess()
    {
        ModelAndView m = new ModelAndView("lineProcess");
        m.addObject("menuObj","line");
        return m;
    }
    @RequestMapping("prodLine")
    public ModelAndView prodLine()
    {
        ModelAndView m = new ModelAndView("prodLine");
        m.addObject("menuObj","line");
        return m;
    }
    @RequestMapping("workPlace")
    public ModelAndView workPlace()
    {
        ModelAndView m = new ModelAndView("workPlace");
        m.addObject("menuObj","line");
        return m;
    }
    @RequestMapping("productionPlan")
    public ModelAndView productionPlan()
    {
        ModelAndView m = new ModelAndView("productionPlan");
        m.addObject("menuObj","line");
        return m;
    }
  
    @RequestMapping("factoryMaster")
    public ModelAndView factoryMaster()
    {
        ModelAndView m = new ModelAndView("factoryMaster");
        m.addObject("menuObj","line");
        return m;
    }
    
    @RequestMapping("fileUploader")
    public ModelAndView fileUploader()
    {
        ModelAndView m = new ModelAndView("fileUploader");
        m.addObject("menuObj","line");
        return m;
    }
    /**************************************************/
    
    /***************Customer**************************/
    @RequestMapping("workerMaster")
    public ModelAndView workerMaster()
    {
        ModelAndView m = new ModelAndView("workerMaster");
        m.addObject("menuObj","worker");
        return m;
    }
    @RequestMapping("photoGallery")
    public ModelAndView photoGallery()
    {
        ModelAndView m = new ModelAndView("photoGallery");
        return m;
    }
    
    @RequestMapping("workerProductivity")
    public ModelAndView workerProductivity()
    {
        ModelAndView m = new ModelAndView("workerProductivity");
        return m;
    }
    
	@RequestMapping("factoryStatistic")
    public ModelAndView testChart()
    {
        ModelAndView m = new ModelAndView("factoryStatistic");
        return m;
    }
    /********************************************************/
    @RequestMapping("errorPage")
    public ModelAndView errorPage()
    {
        /*
        * TODO CHECK VALID SESSION OF ALL VIEW CONTROLLER AND REDIRECT TO ERROR
        * */
        ModelAndView m = new ModelAndView("errorPage");
        return m;
    }
}
