package com.daisyit.controller;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.utils.ConstantKey;
@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter{

	@Autowired
	private SysvarDAO sysvarDAO;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String uploadDir = sysvarDAO.getValueFromName(ConstantKey.UPLOAD_FOLDER);
		uploadDir = "file:" + uploadDir + File.separator;
		registry.addResourceHandler("/uploads/**")
        .addResourceLocations(uploadDir);
	}

}
