package com.daisyit.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.daisyit.frontend.service.ItemService;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("exportData")
public class ExportController {
	@Autowired
	private ItemService itemService;
	@RequestMapping(value = "/bom", method = RequestMethod.GET)
	public ModelAndView exportBOM(HttpSession session) {
		ModelAndView model = null;
		model = new ModelAndView("csvView");
		List<Object[]> boms = itemService.exportBOM();
		model.addObject("bom", boms);
		return model;
	}

}
