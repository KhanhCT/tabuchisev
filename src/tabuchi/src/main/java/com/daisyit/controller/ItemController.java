package com.daisyit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daisyit.dto.LineProdsDTO;
import com.daisyit.dto.MaterialDTO;
import com.daisyit.dto.ProcessBOMDTO;
import com.daisyit.dto.ProcessDTO;
import com.daisyit.dto.ProductBOMDTO;
import com.daisyit.dto.UnitDTO;
import com.daisyit.frontend.service.ItemService;


@RestController
@RequestMapping("/api/v0.1/ItemController")
public class ItemController {

	/***************Material***********************/
	@Autowired
	private ItemService itemService;
	@RequestMapping(value = "getAllMaterialInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> getAllMaterialInfo()
	{
		return null;
	}
	
	@RequestMapping(value = "findAllUsageUnits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UnitDTO> findAllUsageUnits()
	{
		return itemService.findAllUsageUnits();
	}
	
	@RequestMapping(value = "findAllSkuUnits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UnitDTO> findAllSkuUnits()
	{
		return itemService.findAllSkuUnits();
	}
	
	@RequestMapping(value = "newMaterial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newMaterial(@RequestBody MaterialDTO dto)
	{
		return itemService.newMaterial(dto);
	}
	
	@RequestMapping(value = "updateMaterial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateMaterial(@RequestBody MaterialDTO dto)
	{
		return itemService.updateMaterial(dto);
	}
	
	@RequestMapping(value = "findMaterial", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public MaterialDTO findMaterial(@RequestParam(value="materialCode") String materialCode)
	{
		return itemService.findMaterial(materialCode);
	}
	
	@RequestMapping(value = "deleteMaterial/{materialCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteMaterial(@PathVariable String materialCode)
	{
		return itemService.deleteMaterial(materialCode);
	}
	
	@RequestMapping(value = "findAllMaterial", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MaterialDTO> findAllMaterial()
	{
		return itemService.findAllMaterial();
	}
	
	@RequestMapping(value = "syncMaterial", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MaterialDTO> syncMaterial()
	{
		return itemService.syncMaterial();
	}
	
	/*************************Line Product***************************/
	@RequestMapping(value = "newLineProduct", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newLineProduct(@RequestBody LineProdsDTO dto)
	{
		return itemService.newLineProduct(dto);
	}
	
	@RequestMapping(value = "updateLineProduct", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateLineProduct(@RequestBody LineProdsDTO dto)
	{
		return itemService.updateLineProduct(dto);
	}
	
	@RequestMapping(value = "findLineProduct", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public LineProdsDTO findLineProduct(@RequestParam(value="lineProdCode") String lineProdCode)
	{
		return itemService.findLineProduct(lineProdCode);
	}
	
	@RequestMapping(value = "deleteLineProduct/{lineProdCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteLineProduct(@PathVariable String lineProdCode)
	{
		return itemService.deleteLineProduct(lineProdCode);
	}
	
	@RequestMapping(value = "findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LineProdsDTO> findAll()
	{
		return itemService.findAll();
	}
	
	/*************************************Product BOM**********************/
	@RequestMapping(value = "newLineProductBOM", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newLineProductBOM(@RequestBody ProductBOMDTO dto) {
		return itemService.newLineProductBOM(dto);
	}
	@RequestMapping(value = "updateLineProducBOM", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateLineProducBOM(@RequestBody ProductBOMDTO dto) {
		return itemService.updateLineProducBOM(dto);
	}
	@RequestMapping(value = "findLineProductBOM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductBOMDTO findLineProductBOM(@RequestParam(value="lineProdCode")String lineProdCode, @RequestParam(value="materialCode")String materialCode) {
		return itemService.findLineProductBOM(lineProdCode, materialCode);
	}
	@RequestMapping(value = "findAllLineProductBOM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProductBOMDTO> findAllLineProductBOM() {
		return itemService.findAllLineProductBOM();
	}
	@RequestMapping(value = "deleteLineProductBOM/{lineProdCode}/{materialCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteLineProductBOM(@PathVariable String lineProdCode, @PathVariable String materialCode) {
		return itemService.deleteLineProductBOM(lineProdCode, materialCode);
	}

	/*************************************Process BOM**********************/
	@RequestMapping(value = "newProcessProductBOM", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newProcessProductBOM(@RequestBody ProcessBOMDTO dto) {
		return itemService.newProcessProdBOM(dto);
	}
	@RequestMapping(value = "updateProcessProducBOM", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateProcessProducBOM(@RequestBody ProcessBOMDTO dto) {
		return itemService.updateProcessProdBOM(dto);
	}
	@RequestMapping(value = "findProcessProductBOM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProcessBOMDTO> findProcessProductBOM(@RequestParam(value="lineProdCode")String lineProdCode, @RequestParam(value="processIdx") int processIdx ) {
		return itemService.findProcessProdBOM(lineProdCode, processIdx);
	}
	@RequestMapping(value = "findAllProcessProductBOM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProcessBOMDTO> findAllProcessProductBOM() {
		return itemService.findAllProcessProdBOM();
	}
	@RequestMapping(value = "deleteProcessProductBOM/{lineProdCode}/{materialCode}/{processIdx}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteProcessProductBOM(@PathVariable String lineProdCode, @PathVariable String materialCode, @PathVariable int processIdx) {
		return itemService.deleteProcessProdBOM(lineProdCode, materialCode, processIdx);
	}
	/*************************************Unit **********************/
	@RequestMapping(value = "newUnit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newUnit(@RequestBody UnitDTO dto)
	{
		return itemService.newUnit(dto);
	}
	@RequestMapping(value = "updateUnit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateUnit(@RequestBody UnitDTO dto)
	{
		return itemService.updateUnit(dto);
	}
	@RequestMapping(value = "findUnit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UnitDTO findUnit(@RequestParam(value="unitCode")String unitCode) {
		return itemService.findUnit(unitCode);
	}
	
	@RequestMapping(value = "findAllUnits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UnitDTO> findAllUnits() {
		return itemService.findAllUnits();
	}
	
	@RequestMapping(value = "deleteUnit/{unitCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteLineProductBOM(@PathVariable String unitCode) {
		return itemService.deleteUnit(unitCode);
	}
	
	/*************************************Process **********************/
	@RequestMapping(value = "newProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newProcess(@RequestBody ProcessDTO dto)
	{
		return itemService.newProcess(dto);
	}
	@RequestMapping(value = "updateProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateProcess(@RequestBody ProcessDTO dto)
	{
		return itemService.updateProcess(dto);
	}
	@RequestMapping(value = "findProcesses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProcessDTO> findProcesses(@RequestParam(value="processCode")String processCode) {
		return itemService.findProcesses(processCode);
	}
	@RequestMapping(value = "deleteProcess/{processCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int deleteProcess(@PathVariable String processCode) {
		return itemService.deleteProcess(processCode);
	}
}
