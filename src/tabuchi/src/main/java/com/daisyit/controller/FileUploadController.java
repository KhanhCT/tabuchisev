package com.daisyit.controller;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.dto.LineDTO;
import com.daisyit.dto.LineProcessDTO;
import com.daisyit.dto.LineProdsDTO;
import com.daisyit.dto.MaterialDTO;
import com.daisyit.dto.ProcessBOMDTO;
import com.daisyit.dto.ProductBOMDTO;
import com.daisyit.dto.ProductionLineDTO;
import com.daisyit.dto.WorkPlaceDTO;
import com.daisyit.dto.WorkerDTO;
import com.daisyit.frontend.service.ItemService;
import com.daisyit.frontend.service.LineService;
import com.daisyit.frontend.service.WorkerService;
import com.daisyit.utils.Constant;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v0.1/fileController")
public class FileUploadController {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
	@Autowired
	private SysvarDAO sysvarDAO;
	@Autowired
	private LineService lineService;
	@Autowired
	private WorkerService workerService;
	@Autowired
	private ItemService itemService;
	
	@RequestMapping(value = "uploadProductionLine", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<LineDTO> uploadProductionLine(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<LineDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = lineService.uploadProductionLine(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "uploadWorkerList", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<WorkerDTO> uploadWorkerList(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<WorkerDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = workerService.uploadWorkerList(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "uploadProcessList", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<LineProcessDTO> uploadProcessList(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<LineProcessDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = lineService.uploadProcessList(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "uploadWorkPlace", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<WorkPlaceDTO> uploadWorkPlace(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<WorkPlaceDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = lineService.uploadWorkPlace(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	
	@RequestMapping(value = "uploadProductionPlan", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ProductionLineDTO> uploadProductionPlan(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<ProductionLineDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = lineService.uploadProductionPlan(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	
	@RequestMapping(value = "uploadProductList", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<LineProdsDTO> uploadProduct(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<LineProdsDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = itemService.handleProductFile(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (Exception e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "uploadMaterial", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<MaterialDTO> uploadMaterial(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<MaterialDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = itemService.uploadMaterial(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (IOException e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "uploadLineProdBOM", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ProductBOMDTO> uploadLineProdBOM(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<ProductBOMDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = itemService.uploadLineProdBOM(serverFile.getAbsolutePath());
				serverFile.delete();
			} catch (IOException e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
			
			}
			return dtos;
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}

	@RequestMapping(value = "uploadProcessProdBOM", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ProcessBOMDTO> uploadProcessProdBOM(@RequestParam("file") MultipartFile file) {
		String fileName = null;
		String uploadDir = sysvarDAO.getValueFromName("m_UploadDir");
		List<ProcessBOMDTO> dtos = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());	
				dtos = itemService.uploadProcessProdBOM(serverFile.getAbsolutePath());
				serverFile.delete();
				return dtos;
			} catch (IOException e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				return dtos;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			return dtos;
		}
	}
	@RequestMapping(value = "file", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public int uploadFile(@RequestParam("file") MultipartFile file, HttpResponse res) {
		String fileName = null;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				fileName = file.getOriginalFilename();
				if(fileName== "") {
					res.setStatusCode(HttpStatus.SC_BAD_REQUEST);
				}
				String extFile = fileName.substring(fileName.length() -3, fileName.length());
				switch (extFile) {
				case "mp4":{
					String uploadDir = sysvarDAO.getValueFromName("m_UploadImageDir");
					// Creating the directory to store file
					File dir = new File(uploadDir);
					if (!dir.exists())
						dir.mkdirs();

					// Create the file on server
					
					File serverFile = new File(uploadDir + File.separator + file.getOriginalFilename());
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(bytes);
					stream.close();
					res.setStatusCode(HttpStatus.SC_OK);
				}break;
				case "jpg":{
					String[] fileArr = fileName.split("_");
					if(fileArr.length != 4) {
						String uploadDir = sysvarDAO.getValueFromName("m_UploadImageDir");
						// Creating the directory to store file
						File dir = new File(uploadDir + File.separator + fileArr[0]);
						if (!dir.exists())
						{
							dir.mkdirs();
							dir = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1]);
							dir.mkdirs();
							dir = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1] +  File.separator + fileArr[2]);
							dir.mkdirs();
						}else {
							dir = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1]);
							if (!dir.exists()) {
								dir.mkdirs();
								dir = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1] +  File.separator + fileArr[1]);
								dir.mkdirs();				
							}else {
								dir = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1] +  File.separator + fileArr[1]+  File.separator + fileArr[2]);
								if (!dir.exists()) {
									dir.mkdirs();
								}
							}
						}
						File serverFile = new File(uploadDir + File.separator + fileArr[0] +  File.separator + fileArr[1] +  File.separator + fileArr[1]+  File.separator + fileArr[2] +File.separator + fileArr[3]);
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
						stream.write(bytes);
						stream.close();
						res.setStatusCode(HttpStatus.SC_OK);
					}					
				}break;
				default:{
					res.setStatusCode(HttpStatus.SC_BAD_REQUEST);
				}break;
					
				}
				return 0;
			} catch (IOException e) {
				
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				res.setStatusCode(HttpStatus.SC_BAD_REQUEST);
				return 1;
			}
		} else {
			LOGGER.error("failed to upload " + fileName + " because the file was empty.");
			res.setStatusCode(HttpStatus.SC_BAD_REQUEST);
			return 0;
		}
	}
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public List<String> uploadMultipleFileHandler(@RequestParam("file") MultipartFile[] files) {


		String uploadDir = Constant.getInstance().findUploadFolder();
		List<String> errorFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String fileName = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				File dir = new File(uploadDir);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				LOGGER.info("Server File Location=" + serverFile.getAbsolutePath());
			} catch (IOException e) {
				LOGGER.error("You failed to upload " + fileName + " => " + e.getMessage());
				errorFiles.add(fileName);
			}
		}
		return errorFiles;
	}
}
