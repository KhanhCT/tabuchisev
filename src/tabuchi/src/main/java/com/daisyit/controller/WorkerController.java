package com.daisyit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daisyit.dto.WorkerDTO;
import com.daisyit.frontend.service.WorkerService;

@RestController
@RequestMapping("/api/v0.1/WorkerController")
public class WorkerController {
	
	@Autowired
	private WorkerService workerService;
	@RequestMapping(value = "findAllWorkers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<WorkerDTO> findAllWorkers()
	{
		return workerService.findAllWorkers();
	}
	@RequestMapping(value = "deleteWorkerId/{workerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int delete(@PathVariable int workerId) {
		return workerService.delete(workerId);
	}
	@RequestMapping(value = "deleteWorkerCode/{workerCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public int delete(@PathVariable String workerCode) {
		return workerService.delete(workerCode);
	}
	@RequestMapping(value = "newWorker", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int newWorker(@RequestBody WorkerDTO worker)
	{
		return workerService.save(worker);
	}
	
	@RequestMapping(value = "updateWorker", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int updateWorker(@RequestBody WorkerDTO worker)
	{
		return workerService.update(worker);
	}
	
	@RequestMapping(value = "findWorker", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WorkerDTO findWorker(@RequestParam String workerCode) {
		return workerService.find(workerCode);
	}
	
}
