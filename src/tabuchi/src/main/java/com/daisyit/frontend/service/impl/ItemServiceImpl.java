package com.daisyit.frontend.service.impl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springlib.core.utils.CommonKey;

import com.daisyit.backend.dao.LineProdDAO;
import com.daisyit.backend.dao.MaterialDAO;
import com.daisyit.backend.dao.ProcessBOM;
import com.daisyit.backend.dao.ProcessDAO;
import com.daisyit.backend.dao.ProductBOM;
import com.daisyit.backend.dao.SysvarDAO;
import com.daisyit.backend.dao.UnitDAO;
import com.daisyit.backend.model.LineProdBom;
import com.daisyit.backend.model.LineProds;
import com.daisyit.backend.model.Material;
import com.daisyit.backend.model.Process;
import com.daisyit.backend.model.ProcessProdBom;
import com.daisyit.backend.model.Sysvar;
import com.daisyit.backend.model.Units;
import com.daisyit.dto.LineProdsDTO;
import com.daisyit.dto.MaterialDTO;
import com.daisyit.dto.ProcessBOMDTO;
import com.daisyit.dto.ProcessDTO;
import com.daisyit.dto.ProductBOMDTO;
import com.daisyit.dto.UnitDTO;
import com.daisyit.frontend.service.ItemService;
import com.daisyit.utils.PostgresUtil;
import com.google.gson.Gson;
@Service("itemService")
public class ItemServiceImpl implements ItemService{

	@Autowired
	private UnitDAO unitDAO;
	@Autowired
	private MaterialDAO materialDAO;
	
	@Autowired
	private LineProdDAO lineProdDAO;
	
	@Autowired
	private ProductBOM productBOM;
	
	@Autowired
	private ProcessBOM processBOM;
	
	@Autowired
	private ProcessDAO processDAO;
	@Autowired
	private SysvarDAO sysvarDAO;
	
	@Override
	public List<UnitDTO> findAllUsageUnits() {
		List<Units> usageModelList = unitDAO.getAllUsageUnits(); 
		List<UnitDTO> usageList = new ArrayList<>();
				
		for(Units unit : usageModelList) {
			UnitDTO dto = new UnitDTO();
			dto.model2DTO(unit);
			usageList.add(dto);
		}
		return usageList;
	}

	@Override
	public List<UnitDTO> findAllSkuUnits() {
		List<Units> usageModelList = unitDAO.getAllSKUUnits();
		List<UnitDTO> usageList = new ArrayList<>();
				
		for(Units unit : usageModelList) {
			UnitDTO dto = new UnitDTO();
			dto.model2DTO(unit);
			usageList.add(dto);
		}
		return usageList;
	}

	@Override
	public int newMaterial(MaterialDTO dto) {	
		if(dto.getIsLineProd()) {
			LineProds lineProds = lineProdDAO.getLineProductByCode(dto.getProductCode());
			if(lineProds !=null) {
				dto.setLineProdId(lineProds.getId().getLineProdId());
				Material material = dto.dto2Model();
				return materialDAO.insert(material);
			}else
				return CommonKey.RESPONSE_CODE.ERROR;
		}else {
			Material material = dto.dto2Model();
			return materialDAO.insert(material);
		}
		
	}

	@Override
	public int updateMaterial(MaterialDTO dto) {
		LineProds lineProds = lineProdDAO.getLineProductByCode(dto.getProductCode());
		if(lineProds !=null) {
			dto.setLineProdId(lineProds.getId().getLineProdId());
			Material material = dto.dto2Model();
			return materialDAO.update(material);
		}else
			return CommonKey.RESPONSE_CODE.ERROR;
	}

	@Override
	public MaterialDTO findMaterial(String materialCode) {
		MaterialDTO dto = new MaterialDTO();
		Material material = materialDAO.find(materialCode);
		if(material == null)
			return null;
		LineProds lineProd = lineProdDAO.getLineProductById(material.getLineProdId());
		if(lineProd == null)
			return null;
		dto.model2DTO(material);;
		dto.setProductCode(lineProd.getId().getLineProdCode());
		return dto;
	}
	
	@Override
	public List<MaterialDTO> findAllMaterial() {
		List<Object[]> list = materialDAO.findAll();
		List<MaterialDTO> dtos = new ArrayList<>();
		MaterialDTO dto;
		for(Object[] obj : list) {
			dto = new MaterialDTO();
			dto.setMaterialId((int)obj[0]);
			dto.setMaterialCode(obj[1].toString());
			dto.setBarcode(obj[2].toString());
			dto.setMaterialName(obj[3].toString());

			dto.setUsageUnitId((Integer)obj[4]);
			dto.setSkuunitId((Integer)obj[5]);
			Double qty = (Double)obj[6];
			dto.setSkuusageQty((Double)qty);
			if((boolean)obj[7] == true) {
				dto.setLineProdId((int)obj[8]);
				dto.setProductCode(obj[10].toString());
			}
			dtos.add(dto);
			
		}
		return dtos;
	}

	@Override
	public int deleteMaterial(String materialCode) {
		
		return materialDAO.delete(materialCode);
	}

	@Override
	public int newLineProduct(LineProdsDTO dto) {
		LineProds lineProds = dto.dto2Model();
		if(lineProdDAO.find(dto.getLineProdCode()) != null){
			return 0;
		}
		return lineProdDAO.insert(lineProds);
	}

	@Override
	public int updateLineProduct(LineProdsDTO dto) {
		LineProds lineProds = dto.dto2Model();
		return lineProdDAO.update(lineProds);
	}

	@Override
	public LineProdsDTO findLineProduct(String productCode) {
		LineProds lineProds = lineProdDAO.find(productCode);
		LineProdsDTO  dto = new LineProdsDTO();
		if(lineProds ==null)
			return null;
		dto.model2DTO(lineProds);
		return dto;
	}

	@Override
	public int deleteLineProduct(String productCode) {	
		return lineProdDAO.delete(productCode);
	}

	@Override
	public int newLineProductBOM(ProductBOMDTO dto) {		
		LineProds lineProds = lineProdDAO.find(dto.getLineProdCode());
		if(lineProds ==null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(dto.getMaterialCode());
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		dto.setLineProdId(lineProds.getId().getLineProdId());
		dto.setMaterialId(material.getId().getMaterialId());
		LineProdBom bom = dto.dto2Model();
		int ret =0;
		try {
			ret = productBOM.insert(bom);
			return ret;
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.EXIST;
		}
	}

	@Override
	public int updateLineProducBOM(ProductBOMDTO dto) {
		LineProds lineProds = lineProdDAO.find(dto.getLineProdCode());
		if(lineProds ==null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(dto.getMaterialCode());
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		dto.setLineProdId(lineProds.getId().getLineProdId());
		dto.setMaterialId(material.getId().getMaterialId());
		LineProdBom bom = dto.dto2Model();
		return productBOM.update(bom);
	}

	@Override
	public ProductBOMDTO findLineProductBOM(String lineProdCode, String materialCode) {
		LineProds lineProds = lineProdDAO.find(lineProdCode);
		if(lineProds ==null)
			return null;
		Material material = materialDAO.find(materialCode);
		if(material == null)
			return null;
		
		LineProdBom bom = productBOM.find(lineProds.getId().getLineProdId(), material.getId().getMaterialId());
		if(bom == null)
			return null;
		ProductBOMDTO dto = new ProductBOMDTO();
		dto.mode2DTO(bom);
		dto.setLineProdCode(lineProdCode);
		dto.setMaterialCode(materialCode);
		return dto;
	}

	@Override
	public int deleteLineProductBOM(String lineProdCode, String materialCode) {
		LineProds lineProds = lineProdDAO.find(lineProdCode);
		if(lineProds == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(materialCode);
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		return productBOM.delete(lineProds.getId().getLineProdId(), material.getId().getMaterialId());
	}

	@Override
	public List<ProductBOMDTO> findAllLineProductBOM() {
		List<Object[]> list = productBOM.findAll();
		List<ProductBOMDTO> dtos = new ArrayList<>();
		ProductBOMDTO dto;
		for(Object[] obj : list) {
			dto = new ProductBOMDTO();
			dto.setLineProdId((int)obj[0]);
			dto.setMaterialId((int)obj[1]);
			dto.setLineSkuunitId((int)obj[2]);
			Double qty = (Double)obj[3];
			dto.setLineSkuQty(qty);
			dto.setMateUsageUnitId((Integer)obj[4]);
			dto.setMateUsageQty((Double)obj[5]);
			dto.setMateSkuQty((Double)obj[6]);
			dto.setLineProdCode(obj[8].toString());
			dto.setMaterialCode(obj[9].toString());
			dtos.add(dto);	
		}
		return dtos;
	}
	
	@Override
	public int newProcessProdBOM(ProcessBOMDTO dto) {
		LineProds lineProds = lineProdDAO.find(dto.getLineProdCode());
		if(lineProds ==null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(dto.getMaterialCode());
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		dto.setLineProdId(lineProds.getId().getLineProdId());
		dto.setMaterialId(material.getId().getMaterialId());
		ProcessProdBom bom = dto.dto2Model();
		int ret =0;
		try {
			ret = processBOM.insert(bom);
			return ret;
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.EXIST;
		}
	}

	@Override
	public int updateProcessProdBOM(ProcessBOMDTO dto) {
		LineProds lineProds = lineProdDAO.find(dto.getLineProdCode());
		if(lineProds ==null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(dto.getMaterialCode());
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		dto.setLineProdId(lineProds.getId().getLineProdId());
		dto.setMaterialId(material.getId().getMaterialId());
		ProcessProdBom bom = dto.dto2Model();
		return processBOM.update(bom);
	}

	@Override
	public List<ProcessBOMDTO> findProcessProdBOM(String lineProdCode, int processIdx) {
		LineProds lineProds = lineProdDAO.find(lineProdCode);
		if(lineProds ==null)
			return null;
		List<Object[]> list = processBOM.find(lineProds.getId().getLineProdId(),  processIdx);
		List<ProcessBOMDTO> dtos = new ArrayList<>();
		ProcessBOMDTO dto;
		for(Object[] obj : list) {
			dto = new ProcessBOMDTO();
			dto.setLineProdId((int)obj[0]);
			dto.setProcessIdx((int)obj[1]);
			dto.setMaterialId((int)obj[2]);
			dto.setLineSkuunitId((int)obj[3]);
			Double qty = (Double)obj[4];
			dto.setLineSkuQty(qty);
			dto.setMateUsageUnitId((Integer)obj[5]);
			dto.setMateUsageQty((Double)obj[6]);
			dto.setMateSkuQty((Double)obj[7]);
			dto.setLineProdCode(obj[9].toString());
			dto.setMaterialCode(obj[10].toString());
			dtos.add(dto);	
		}
		return dtos;
	}

	@Override
	public List<ProcessBOMDTO> findAllProcessProdBOM() {
		List<Object[]> list = processBOM.findAll();
		List<ProcessBOMDTO> dtos = new ArrayList<>();
		ProcessBOMDTO dto;
		for(Object[] obj : list) {
			dto = new ProcessBOMDTO();
			dto.setLineProdId((int)obj[0]);
			dto.setProcessIdx((int)obj[1]);
			dto.setMaterialId((int)obj[2]);
			dto.setLineSkuunitId((Integer)obj[3]);
			Double qty = (Double)obj[4];
			dto.setLineSkuQty(qty);
			dto.setMateUsageUnitId((Integer)obj[5]);
			dto.setMateUsageQty((Double)obj[6]);
			dto.setMateSkuQty((Double)obj[7]);
			dto.setLineProdCode(obj[9].toString());
			dto.setMaterialCode(obj[10].toString());
			dtos.add(dto);	
		}
		return dtos;
	}

	@Override
	public int deleteProcessProdBOM(String lineProdCode, String materialCode, int processIdx) {
		LineProds lineProds = lineProdDAO.find(lineProdCode);
		if(lineProds == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		Material material =materialDAO.find(materialCode);
		if(material == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		return processBOM.delete(lineProds.getId().getLineProdId(), material.getId().getMaterialId(), processIdx);
	}
	
	@Override
	public List<LineProdsDTO> findAll() {
		List<LineProdsDTO> dtos = new ArrayList<>();
		List<LineProds> prodList = lineProdDAO.findAll();
		for(LineProds prod : prodList)
		{
			LineProdsDTO dto = new LineProdsDTO();
			dto.model2DTO(prod);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<LineProdsDTO> handleProductFile(String filePath) {
		File productFile = new File(filePath);
		List<LineProdsDTO> dtos = new ArrayList<>();
		if(!productFile.exists())
			return dtos;
		try {
			Workbook workbook = WorkbookFactory.create(new File(filePath));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			LineProdsDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new LineProdsDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String productCode = cell.getStringCellValue();
					if (productCode == null || productCode.equals(""))
						continue;
					LineProds lineProds = lineProdDAO.find(productCode);						
					if(lineProds !=  null)
						continue;
					dto.setLineProdCode(productCode);
					
					cell = cellIterator.next();
					String barCode = cell.getStringCellValue();
					dto.setBarcode(barCode);
					
					cell = cellIterator.next();
					String productName = cell.getStringCellValue();
					dto.setLineProdName(productName);
					
					cell = cellIterator.next();	
					String skuCode = cell.getStringCellValue();
					Units unit = unitDAO.find(skuCode);
					if(unit != null) {
						dto.setSkuUnitID(unit.getId().getUnitId());
						dto.setSkuUnitName(unit.getUnitName());
					}
					
					cell = cellIterator.next();	
					try{
						int numOfProcessed = (int)cell.getNumericCellValue();
						dto.setProcessQty(numOfProcessed);
					} catch (Exception ex) {
						dto.setProcessQty(0);
					}
					
					cell = cellIterator.next();	
					try{
						int numOfWPlace = (int)cell.getNumericCellValue();
						dto.setWplaceQty(numOfWPlace);
					} catch (Exception ex) {
						dto.setWplaceQty(0);
					}
					dto.setDisabled(false);
					dtos.add(dto);
				}
			};
			for(LineProdsDTO _dto : dtos) {
				LineProds model = _dto.dto2Model();
				try{
					if(lineProdDAO.insert(model) <=0){
						lineProdDAO.update(model);
					}	
				}catch (Exception e){
					e.printStackTrace();
					dtos.remove(_dto);
				}
			}
		}catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		
		return dtos;
	}

	@Override
	public int newUnit(UnitDTO dto) {
		if(unitDAO.find(dto.getUnitCode()) !=null)
			return 0;
		Units model = dto.dto2Model();
		return unitDAO.insert(model);
	}

	@Override
	public int deleteUnit(String unitCode) {
		
		return unitDAO.delete(unitCode);
	}

	@Override
	public int updateUnit(UnitDTO dto) {
		Units model = dto.dto2Model();
		return unitDAO.update(model);
	}

	@Override
	public UnitDTO findUnit(String unitCode) {
		Units unit =  unitDAO.find(unitCode);
		UnitDTO dto = new UnitDTO();
		dto.model2DTO(unit);
		return dto;
	}
	
	@Override
	public List<MaterialDTO> uploadMaterial(String url) {
		File materialFile = new File(url);
		List<MaterialDTO> dtos = new ArrayList<>();
		List<Units> unitList = unitDAO.findAll();
		if(!materialFile.exists())
			return null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			MaterialDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new MaterialDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String materialCode = cell.getStringCellValue();
					if (materialCode == null || materialCode.equals(""))
						continue;
					Material mal = materialDAO.find(materialCode);
					if(mal != null)
						continue;
					dto.setMaterialCode(materialCode);
					cell = cellIterator.next();
					String barcode = cell.getStringCellValue();
					dto.setBarcode(barcode);
					cell = cellIterator.next();
					String name = cell.getStringCellValue();				
					dto.setMaterialName(name);
					cell = cellIterator.next();
					String skuUnitCode = cell.getStringCellValue();
					Units unit = null;
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(skuUnitCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null)
					{
						dto.setSkuunitId(null);
					}else {
						dto.setSkuunitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();
					unit = null;
					String usageCode = cell.getStringCellValue();
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(usageCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null)
					{
						dto.setUsageUnitId(null);
					}else {
						dto.setUsageUnitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();

					Double qty = null;
					try{
						qty = (Double)cell.getNumericCellValue();
					} catch (Exception ex) {
					}
					dto.setSkuusageQty(qty);
					cell = cellIterator.next();
					int isProdLine= 0;
					try{
						isProdLine = (int)cell.getNumericCellValue();	
					} catch (Exception ex) {
						
					}
					dto.setIsLineProd(false);
					if(isProdLine == 1) {
						dto.setIsLineProd(true);
						cell = cellIterator.next();
						String productCode = cell.getStringCellValue();
						LineProds prod = lineProdDAO.find(productCode);
						if(prod == null)
							continue;
						dto.setIsLineProd(true);
						dto.setLineProdId(prod.getId().getLineProdId());
					}
					dto.setDisabled(false);
					dtos.add(dto);
				}
			 }
			for(MaterialDTO _dto : dtos){
				Material _mal = _dto.dto2Model();
				try{
					materialDAO.insert(_mal);
				}catch (Exception e){
					dtos.remove(_dto);
				}
			}
		} catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return dtos;
	}

	@Override
	public List<ProductBOMDTO> uploadLineProdBOM(String url) {
		File materialFile = new File(url);
		List<ProductBOMDTO> dtos = new ArrayList<>();
		List<Units> unitList = unitDAO.findAll();
		if(!materialFile.exists())
			return null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			ProductBOMDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new ProductBOMDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String prodCode = cell.getStringCellValue();
					if (prodCode == null || prodCode.equals(""))
						continue;
					LineProds prod = lineProdDAO.find(prodCode);
					if(prod == null)
						continue;
					dto.setLineProdId(prod.getId().getLineProdId());
					dto.setLineProdCode(prod.getId().getLineProdCode());
					cell = cellIterator.next();
					String materialCode = cell.getStringCellValue();
					Material mal = materialDAO.find(materialCode);
					if(mal == null)
						continue;
					dto.setMaterialCode(materialCode);
					dto.setMaterialId(mal.getId().getMaterialId());					
					cell = cellIterator.next();
					String skuUnitCode = cell.getStringCellValue();
					Units unit = null;
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(skuUnitCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null)
					{
						dto.setLineSkuunitId(null);
					}else {
						dto.setLineSkuunitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();
					Double qty = null;
					try{
						qty = (Double)cell.getNumericCellValue();
					} catch (Exception ex) {
					}
					dto.setLineSkuQty((Double)qty);
					cell = cellIterator.next();
					unit = null;
					String usageCode = cell.getStringCellValue();
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(usageCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null) {
						dto.setMateUsageUnitId(null);
					}else {
						dto.setMateUsageUnitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();
					qty = null;
					try{
						qty = (Double)cell.getNumericCellValue();
					} catch (Exception ex) {

					}
					dto.setMateUsageQty((Double)qty);
					
					dtos.add(dto);
				}
			 }
			for(ProductBOMDTO _dto : dtos){
				LineProdBom _pBOM = _dto.dto2Model();
				try{
					productBOM.insert(_pBOM);
				}catch (Exception e){
					dtos.remove(_dto);
				}
			}
		} catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return dtos;
	}

	@Override
	public List<ProcessBOMDTO> uploadProcessProdBOM(String url) {
		File materialFile = new File(url);
		List<ProcessBOMDTO> dtos = new ArrayList<>();
		List<Units> unitList = unitDAO.findAll();
		List<Process> processes = processDAO.findAll();
		if(!materialFile.exists())
			return null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			ProcessBOMDTO dto;
			
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new ProcessBOMDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String prodCode = cell.getStringCellValue();
					if (prodCode == null || prodCode.equals(""))
						continue;
					LineProds prod = lineProdDAO.find(prodCode);
					if(prod == null)
						continue;
					dto.setLineProdId(prod.getId().getLineProdId());
					dto.setLineProdCode(prod.getId().getLineProdCode());
					cell = cellIterator.next();
					try{
						Process pr = null;
						String processCode = cell.getStringCellValue();
						for(Process p : processes) {
							if(p.getId().getProcessCode().trim().equals(processCode)) {
								pr = p;
								break;
							}
						}
						if(pr == null)
							continue;
						dto.setProcessIdx(pr.getId().getProcessId());
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					String materialCode = cell.getStringCellValue();
					Material mal = materialDAO.find(materialCode);
					if(mal == null)
						continue;
					dto.setMaterialCode(materialCode);
					dto.setMaterialId(mal.getId().getMaterialId());					
					cell = cellIterator.next();
					String skuUnitCode = cell.getStringCellValue();
					Units unit = null;
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(skuUnitCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null)
					{
						dto.setLineSkuunitId(null);
					}else {
						dto.setLineSkuunitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();
					Double qty = null;
					try{
						qty = (Double)cell.getNumericCellValue();
					} catch (Exception ex) {
					}
					dto.setLineSkuQty(qty);
					cell = cellIterator.next();
					unit = null;
					String usageCode = cell.getStringCellValue();
					for(Units u : unitList) {
						if(u.getId().getUnitCode().trim().equals(usageCode))
						{
							unit = u;
							break;
						}
					}
					if(unit ==  null) {
						dto.setMateUsageUnitId(null);
					}else {
						dto.setMateUsageUnitId(unit.getId().getUnitId());
					}
					
					cell = cellIterator.next();
					qty = null;
					try{
						qty = (Double)cell.getNumericCellValue();
					} catch (Exception ex) {
						
					}
					dto.setMateUsageQty(qty); 
					
					dtos.add(dto);
				}
			 }
			for(ProcessBOMDTO _dto : dtos){
				ProcessProdBom _pBOM = _dto.dto2Model();
				try{
					processBOM.insert(_pBOM);
				}catch (Exception e){
					dtos.remove(_dto);
				}
			}
		} catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return dtos;
	}

	@Override
	public List<ProcessDTO> findProcesses(String processCode) {
		List<Process> list = processDAO.find(processCode);
		List<ProcessDTO> dtos  =  new ArrayList<>();
		ProcessDTO dto;
		for(Process p : list) {
			dto = new ProcessDTO();
			dto.model2DTO(p);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public int newProcess(ProcessDTO dto) {
		Process p = dto.dto2Model();
		p.setDisabled(false);
		return processDAO.insert(p);
	}

	@Override
	public int deleteProcess(String processCode) {
		
		return processDAO.delete(processCode);
	}

	@Override
	public int updateProcess(ProcessDTO dto) {
		Process p = dto.dto2Model();		
		return processDAO.update(p);
	}

	@Override
	public ProcessDTO findProcess(String processCode) {
		
		return null;
	}

	@Override
	public List<Object[]> exportBOM() {
		return processBOM.exportBOM();
	}

	@Override
	public List<UnitDTO> findAllUnits() {
		List<Units> list = unitDAO.findAll();
		List<UnitDTO> dtos = new ArrayList<>();
		UnitDTO dto;
		for(Units unit :list) {
			dto= new UnitDTO();
			dto.model2DTO(unit);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<MaterialDTO> syncMaterial() {
		List<Sysvar> envs = sysvarDAO.findAll("VDB");
		Connection conn = PostgresUtil.openConnection(envs);
		ResultSet rs= null;
        ArrayList<MaterialDTO> dtos = new ArrayList<>();
        MaterialDTO dto;
        Material mal;
        try{
            String sql = "SELECT product_no, product_name from m_product_master";
            java.sql.Statement statement = conn.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next())
            {
            	dto = new MaterialDTO(rs.getString(1), rs.getString(2), rs.getString(1));
            	dto.setIsLineProd(false);
//            	mal = dto.dto2Model();
//            	if(materialDAO.update(mal) <=0) {
//            		materialDAO.insert(mal);
//            	}
//                dtos.add(dto);
            }
            String json = new Gson().toJson(dtos);
            System.out.println(json);
            int ret = materialDAO.insert(json);
            if(ret == 0)
            	dtos = null;
        }catch (Exception ex){
        	ex.printStackTrace();
        	dtos = null;
        }finally {
           
            try { 
            	if(rs !=null) {
            		rs.close();
            	}
            	if(conn!= null && !conn.isClosed())
                 	conn.close();
                
            } catch (SQLException e) {
                e.printStackTrace();
            }           
        }
        
        return dtos;
	}
}
