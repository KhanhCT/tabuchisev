package com.daisyit.frontend.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springlib.core.aop.DateTimeRange;
import org.springlib.core.utils.CommonKey;
import org.springlib.core.utils.DateTimeUtils;

import com.daisyit.backend.dao.FactoryDAO;
import com.daisyit.backend.dao.LineDAO;
import com.daisyit.backend.dao.LineProcessDAO;
import com.daisyit.backend.dao.LineProdDAO;
import com.daisyit.backend.dao.ProcessDAO;
import com.daisyit.backend.dao.ProdLineDetailDAO;
import com.daisyit.backend.dao.ProductionImgDAO;
import com.daisyit.backend.dao.ProductionLineDAO;
import com.daisyit.backend.dao.ShiftDAO;
import com.daisyit.backend.dao.WorkPlaceDAO;

import com.daisyit.backend.model.Factory;
import com.daisyit.backend.model.LineProdProcess;
import com.daisyit.backend.model.LineProds;
import com.daisyit.backend.model.Process;
import com.daisyit.backend.model.ProdLine;
import com.daisyit.backend.model.ProductionLine;
import com.daisyit.backend.model.Shift;
import com.daisyit.backend.model.WorkPlace;
import com.daisyit.dto.BaseDTO;

import com.daisyit.dto.FactoryDTO;
import com.daisyit.dto.LineDTO;
import com.daisyit.dto.LineProcessDTO;
import com.daisyit.dto.ProdImgDTO;
import com.daisyit.dto.ProductionLineDTO;
import com.daisyit.dto.ShiftDTO;
import com.daisyit.dto.WorkPlaceDTO;
import com.daisyit.frontend.service.LineService;
import com.google.gson.Gson;

@Service("lineService")
public class LineServiceImpl implements LineService{
	@Autowired
	private LineProdDAO lineProdDAO;
	
	@Autowired
	private LineProcessDAO lineProcessDAO;
	@Autowired
	private LineDAO lineDAO;
	
	@Autowired
	private FactoryDAO factoryDAO;
	
	@Autowired
	private WorkPlaceDAO workPlaceDAO;
	
	@Autowired
	private ProdLineDetailDAO prodLineDetailDAO;
	@Autowired
	private ProductionLineDAO productionLineDAO;
	
	@Autowired
	private ProductionImgDAO productionImgDAO;
	
	@Autowired
	private ProcessDAO processDAO;
	@Autowired
	private ShiftDAO shiftDAO;

	@Override
	public int newProcess(LineProcessDTO dto) {
//		LineProds lineProds = lineProdDAO.getLineProductByCode(dto.getLineProdCode());	
//		if(lineProds == null)
//			return CommonKey.RESPONSE_CODE.ERROR;
//		dto.setLineProdId(lineProds.getId().getLineProdId());
		LineProdProcess process = dto.dto2Model();
		process.setDisabled(false);
		try {
			lineProcessDAO.insert(process);
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.EXIST;
		}
		return CommonKey.RESPONSE_CODE.SUCCESS;
	}

	@Override
	public int updateProcess(LineProcessDTO dto) {
//		LineProds lineProds = lineProdDAO.getLineProductByCode(dto.getLineProdCode());	
//		if(lineProds == null)
//			return CommonKey.RESPONSE_CODE.ERROR;
//		dto.setLineProdId(lineProds.getId().getLineProdId());
		LineProdProcess process = dto.dto2Model();
		process.setDisabled(false);
		try {
			lineProcessDAO.update(process);
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.EXIST;
		}
		return CommonKey.RESPONSE_CODE.SUCCESS;
	}

	@Override
	public LineProcessDTO findProcess(String lineProdCode, int processId) {
		LineProds lineProds = lineProdDAO.getLineProductByCode(lineProdCode);	
		if(lineProds == null)
			return null;
		LineProdProcess process = lineProcessDAO.find(lineProds.getId().getLineProdId(), processId);
		if(process== null)
			return null;
		LineProcessDTO dto = new LineProcessDTO();
		dto.model2DTO(process);
		dto.setLineProdCode(lineProds.getId().getLineProdCode());
		return dto;
	}

	@Override
	public int deleteProcess(String lineProdCode, int processId) {
		LineProds lineProds = lineProdDAO.getLineProductByCode(lineProdCode);	
		if(lineProds == null)
			return CommonKey.RESPONSE_CODE.ERROR;
		
		return lineProcessDAO.delete(lineProds.getId().getLineProdId(), processId);
	}

	@Override
	public int newLine(LineDTO dto) {
		dto.setDisabled(false);
		ProdLine line = dto.dto2Model();
		try {
			return lineDAO.insert(line);
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.EXIST;
		}
	}

	@Override
	public int updateLine(LineDTO dto) {
		ProdLine line = dto.dto2Model();
		try {
			return lineDAO.update(line);
		}catch (Exception e) {
			return CommonKey.RESPONSE_CODE.ERROR;
		}
	}

	@Override
	public LineDTO findLine(int factoryId, int lineId) {
		try {
			ProdLine line = lineDAO.find(factoryId, lineId);
			if(line == null)
				return null;
			LineDTO dto = new LineDTO();
			dto.model2DTO(line);
			return dto;
		}catch (Exception e) {
			return null;
		}
	}

	@Override
	public int deleteLine(int factoryId, int lineId) {	
		try {	
			return lineDAO.delete(factoryId, lineId);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonKey.RESPONSE_CODE.ERROR;
		}
		
	}

	@Override
	public List<FactoryDTO> findAllFactory() {
		List<FactoryDTO> dtos = new ArrayList<>();
		List<Factory> fList = factoryDAO.findAll();
		for(Factory f : fList) {
			FactoryDTO dto = new FactoryDTO();
			dto.model2DTO(f);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<LineDTO> findLine(int factoryId) {
		List<LineDTO>  dtos = new ArrayList<>();
		try {
			List<ProdLine> lines = lineDAO.findAll(factoryId);
			for(int i=0; i < lines.size(); i++) {
				LineDTO dto = new LineDTO();
				dto.model2DTO(lines.get(i));
				dtos.add(dto);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return dtos;
	}

	

	@Override
	public List<LineProcessDTO> findAllProcess(int lineProdId, int processId) {
		List<Object[]> list = lineProcessDAO.findAll(lineProdId, processId);
		List<LineProcessDTO> dtos = new ArrayList<>();
		for(Object[] obj : list) {
			LineProcessDTO dto = new LineProcessDTO();
			dto.setLineProdId((int)obj[0]);
			dto.setProcessId((int)obj[1]);
			dto.setProcessName((String)obj[2]);
			dto.setMinSt((int)obj[3]);
			dto.setMaxSt((int)obj[4]);
			dto.setStandardVideoLink((String)obj[5]);
			dto.setWorkVideoLink((String)obj[6]);
			dto.setWplaceQty((int)obj[7]);
			dto.setLineProdCode((String)obj[8]);
			dto.setLineProdName((String)obj[9]);
			dto.setDisabled((Boolean)obj[10]);
			dtos.add(dto);
		}
		return dtos;
	}

	

	@Override
	public List<BaseDTO> getMonthyLineProductivity(String curDate, int lineId, int factoryId) {
		List<BaseDTO> dtos = new ArrayList<>();
		Date date = DateTimeUtils.convertDateByFormatLocal(curDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		//String[] dateRange = DateTimeUtils.getStringMonthRange(date);
		DateTimeRange dateRange = DateTimeUtils.getMonthRange(date);
		List<Object[]> data = prodLineDetailDAO.getMonthlyLineData(factoryId, lineId,dateRange.getBegTime() ,dateRange.getEndTime(), 1);
		for(Object[] obj : data) {		
			Date tmp = (Date)obj[0];
			dtos.add(new BaseDTO((Long)tmp.getTime() + 24 * 60 * 60 * 1000, (int)obj[1]));
		}
		return dtos;
	}

	@Override
	public List<BaseDTO> getFactoryData(String curDate, int factoryId) {
		List<BaseDTO> dtos = new ArrayList<>();
		Date date = DateTimeUtils.convertDateByFormatLocal(curDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		//String _curDate = DateTimeUtils.convertDateStringByFormatLocal(date, CommonKey.DATE_FORMAT.DATE_TIME_1);
		List<Object[]> data = prodLineDetailDAO.getFactoryData(factoryId, date);
		for(Object[] obj : data) {
			ProdLine line = lineDAO.find((int)obj[0], factoryId);
			if(line == null)
				continue;
			dtos.add(new BaseDTO(line.getLineDesc(), (int)obj[1], (int)obj[2]));
		}
		return dtos;
		
	}

	@Override
	public List<ProdImgDTO> findAllPhoto(String workingDate, int factoryId, int lineId, int shiftId, int position) {
		Date date = DateTimeUtils.convertDateByFormatLocal(workingDate, CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT);
		List<ProdImgDTO> pathList = new ArrayList<>();
		List<Object[]> imgList = productionImgDAO.find(date, factoryId, lineId, shiftId, position);
		for(Object[] img : imgList) {
			String time = DateTimeUtils.convertDateStringByFormatLocal((Date)img[1], CommonKey.DATE_FORMAT.TIME_FORMAT_2);
			pathList.add(new ProdImgDTO(DateTimeUtils.convertDateStringByFormatLocal((Date)img[0], CommonKey.DATE_FORMAT.DATE_SLASH_FORMAT ), (String)img[2], time));
		}
		return pathList;
	}

	@Override
	public List<ShiftDTO> findAllShifts(int factoryId) {
		List<Shift> shiftList = shiftDAO.findAll(factoryId);
		List<ShiftDTO> dtos = new ArrayList<>();
		for(Shift shift: shiftList) {
			ShiftDTO dto = new ShiftDTO();
			dto.model2DTO(shift);
			dtos.add(dto);
		}
		return dtos;
	}

	
	@Override
	public List<LineDTO> findAllLine(int factoryId, int lineId) {
		List<LineDTO> dtos = new ArrayList<>();
		try{
			List<Object[]> prodLines = lineDAO.findAll(factoryId, lineId);
			for(Object[] obj : prodLines){
				LineDTO dto = new LineDTO();
				dto.setFactoryId((int)obj[0]);
				dto.setLineId((int)obj[1]);
				dto.setLineTagCode((String)obj[2]);
				dto.setLineDesc((String)obj[3]);
				dto.setWplaceQty((int)obj[4]);
				dto.setDisabled((Boolean)obj[5]);
				dto.setFactoryCode((String)obj[6]);
				dto.setFactoryName((String)obj[7]);	
				dtos.add(dto);
			}
		}catch(Exception ex){
		}
		return dtos;
	}

	@Override
	public List<LineProcessDTO> uploadProcessList(String url) {
		File prodPlanFile = new File(url);
		List<LineProcessDTO> dtos = new ArrayList<>();
		if(!prodPlanFile.exists())
			return null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			LineProcessDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new LineProcessDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String productCode = cell.getStringCellValue();
					if (productCode == null || productCode.equals(""))
						continue;
					LineProds product = lineProdDAO.find(productCode);
					if(product == null)
						continue;
					dto.setLineProdCode(productCode);
					dto.setLineProdId(product.getId().getLineProdId());
					dto.setLineProdName(product.getLineProdName());
					cell = cellIterator.next();
					try{
						String processCode = cell.getStringCellValue();
						List<Process> pList = processDAO.find(processCode);
						if(pList.size() >0)
							dto.setProcessId(pList.get(0).getId().getProcessId());
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						String processName = cell.getStringCellValue();
						dto.setProcessName(processName);
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						int wplaceQty = (int)cell.getNumericCellValue();
						dto.setWplaceQty(wplaceQty);
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						int minSt = (int)cell.getNumericCellValue();
						dto.setMinSt(minSt);
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						int maxSt = (int)cell.getNumericCellValue();
						dto.setMaxSt(maxSt);
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						String stVideo = cell.getStringCellValue();
						dto.setStandardVideoLink(stVideo);
					} catch (Exception ex) {
						continue;
					}
					cell = cellIterator.next();
					try{
						String workVideo = cell.getStringCellValue();
						dto.setWorkVideoLink(workVideo);
					} catch (Exception ex) {
						continue;
					}
					dto.setDisabled(false);
					dtos.add(dto);
				}
			 }
			for(LineProcessDTO _dto : dtos){
				
				LineProdProcess epc = _dto.dto2Model();
				try{
					lineProcessDAO.saveOrUpdate(epc);
				}catch (Exception e){
					dtos.remove(_dto);
				}
			}
		} catch (EncryptedDocumentException e) {
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return dtos;
	}

	@Override
	public List<LineDTO> uploadProductionLine(String url) {
		File prodPlanFile = new File(url);
		List<LineDTO> dtos = new ArrayList<>();
		if(!prodPlanFile.exists())
			return dtos;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			LineDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new LineDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String factoryCode = cell.getStringCellValue();
					if (factoryCode == null || factoryCode.equals(""))
						continue;
					Factory factory = factoryDAO.find(factoryCode);
					if(factory == null)
						continue;
					dto.setFactoryCode(factoryCode);
					dto.setFactoryId(factory.getId().getFactoryId());
					dto.setFactoryName(factory.getFactoryName());
					
					cell = cellIterator.next();
					dto.setLineDesc(cell.getStringCellValue());
					cell = cellIterator.next();
					dto.setLineTagCode(cell.getStringCellValue());
					cell = cellIterator.next();
					int wPlaceQty = (int)cell.getNumericCellValue();
					if(wPlaceQty>16)
						wPlaceQty = 16;
					dto.setWplaceQty(wPlaceQty);
					dto.setDisabled(false);
					dtos.add(dto);
				}
			}
			String json = new Gson().toJson(dtos);
			int ret = lineDAO.insert(json);
			if(ret == 0) dtos = null;
		} catch (Exception e) {
			dtos = null;
		}
		return dtos;
	}

	@Override
	public List<WorkPlaceDTO> findAllWorkPlaces(int factoryId, int lineId, int wPlaceId) {
		List<WorkPlaceDTO> dtos = new ArrayList<>();
		List<Object[]> objs = workPlaceDAO.find(factoryId, lineId, wPlaceId);
		WorkPlaceDTO dto;
		for(Object[] obj : objs) {
			dto = new WorkPlaceDTO();
			dto.setFactoryId((int)obj[0]);
			dto.setLineId((int)obj[1]);
			dto.setwPlaceId((int)obj[2]);
			dto.setDescription(obj[3].toString());
			dto.setProcessId((int)obj[4]);
			dto.setIpv4Cam(obj[5].toString());
			dto.setIpv4Tab(obj[6].toString());
			dto.setDisabled((Boolean)obj[7]);
			dto.setFactoryCode(obj[8].toString());
			dto.setFactoryName(obj[9].toString());
			dto.setLineDesc(obj[10].toString());
			dto.setProcessName(obj[11].toString());	
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public int updateWorkPlace(WorkPlaceDTO dto) {
		WorkPlace w = dto.dto2Model();
		return workPlaceDAO.insert(w);
	}

	@Override
	public int deleteWorkPlace(int factoryId, int lineId, int wPlaceId) {
		
		return workPlaceDAO.delete(factoryId, lineId, wPlaceId);
	}

	@Override
	public int newWorkPlace(WorkPlaceDTO dto) {
		WorkPlace w = dto.dto2Model();	
		return workPlaceDAO.insert(w);
	}

	@Override
	public List<WorkPlaceDTO> uploadWorkPlace(String url) {
		File wPlaceFile = new File(url);
		List<WorkPlaceDTO> dtos = new ArrayList<>();
		if(!wPlaceFile.exists())
			return dtos;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			WorkPlaceDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new WorkPlaceDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String factoryCode = cell.getStringCellValue();
					if (factoryCode == null || factoryCode.equals(""))
						continue;
					Factory factory = factoryDAO.find(factoryCode);
					if(factory == null)
						continue;
					dto.setFactoryCode(factoryCode);
					dto.setFactoryId(factory.getId().getFactoryId());
					dto.setFactoryName(factory.getFactoryName());
					cell = cellIterator.next();
					String lineCode = cell.getStringCellValue();
					ProdLine line = lineDAO.find(lineCode, factory.getId().getFactoryId());
					if(line == null)
						continue;
					dto.setLineId(line.getId().getLineId());
					dto.setLineTagCode(line.getLineTagCode());
					dto.setLineDesc(line.getLineDesc());
					cell = cellIterator.next();
					int wPlaceId = (int)cell.getNumericCellValue();
					dto.setwPlaceId(wPlaceId);
					
					cell = cellIterator.next();
					dto.setDescription(cell.getStringCellValue());
					
					cell = cellIterator.next();
					String processCode = cell.getStringCellValue();
					List<Process> pList= processDAO.find(processCode);
					dto.setProcessId(pList.get(0).getId().getProcessId());
					dto.setProcessCode(pList.get(0).getId().getProcessCode());
					dto.setProcessName(pList.get(0).getProcessName());
					cell = cellIterator.next();
					dto.setIpv4Cam(cell.getStringCellValue());
					cell = cellIterator.next();
					dto.setIpv4Tab(cell.getStringCellValue());
					dto.setDisabled(false);
					dtos.add(dto);					
				}
			 }
			String json = new Gson().toJson(dtos);
			System.out.println(json);
			int ret = workPlaceDAO.insert(json);
			if(ret == 0) {
				dtos =  null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			dtos = null;
		}
		return dtos;
	}
	@Override
	public List<ProductionLineDTO> uploadProductionPlan(String url) {
		File prodPlanFile = new File(url);
		List<ProductionLineDTO> dtos = new ArrayList<>();
		if(!prodPlanFile.exists())
			return dtos;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			ProductionLineDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new ProductionLineDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String factoryCode = cell.getStringCellValue();
					if (factoryCode == null || factoryCode.equals(""))
						continue;
					Factory factory = factoryDAO.find(factoryCode);
					if(factory == null)
						continue;
					dto.setFactoryId(factory.getId().getFactoryId());
					dto.setFactoryName(factory.getFactoryName());
					
					cell = cellIterator.next();
					ProdLine line  = lineDAO.find(cell.getStringCellValue(), factory.getId().getFactoryId());
					if(line == null)
						continue;
					dto.setLineDesc(line.getLineDesc());
					dto.setLineId(line.getId().getLineId());
					
					cell = cellIterator.next();
					Shift shift = shiftDAO.find(factory.getId().getFactoryId(),cell.getStringCellValue());
					if(shift == null)
						continue;
					dto.setShiftName(shift.getShiftName());
					dto.setShiftId(shift.getId().getShiftId());
					
					cell = cellIterator.next();
					dto.setWorkingDate(cell.getStringCellValue());
					cell = cellIterator.next();
					LineProds product = lineProdDAO.find(cell.getStringCellValue());
					if(product == null)
						continue;
					dto.setLineProdId(product.getId().getLineProdId());
					dto.setLineProdName(product.getLineProdName());
					cell = cellIterator.next();
					int ordered = (int)cell.getNumericCellValue();
					dto.setOrderedQty(ordered);
					dto.setDisabled(false);
					dtos.add(dto);
				}
			}
			String json = new Gson().toJson(dtos);
			System.out.println(json);
			int ret = productionLineDAO.insert(json);
			if(ret == 0)
				dtos = null;
		} catch (Exception e) {
			dtos = null;
		}
		return dtos;
	}
	@Override
	public int newProductionPlan(ProductionLineDTO dto) {
		ProductionLine p = dto.dto2Model();
		return productionLineDAO.save(p);
	}

	@Override
	public int updateProductionPlan(ProductionLineDTO dto) {
		ProductionLine p = dto.dto2Model();
		return productionLineDAO.update(p);
	}

	@Override
	public List<ProductionLineDTO> findProductionPlan(String workingDate) {
		List<Object[]> list = productionLineDAO.find(workingDate);
		List<ProductionLineDTO> dtos = new ArrayList<>();
		ProductionLineDTO dto;
		for(Object[] obj: list) {
			dto = new ProductionLineDTO();
			dto.setFactoryId((int)obj[0]);
			dto.setLineId((int)obj[1]);
			dto.setWorkingDate(obj[2].toString());
			dto.setShiftId((int)obj[3]);
			dto.setLineProdId((int)obj[8]);
			dto.setOrderedQty((int)obj[9]);
			dto.setFactoryName(obj[14].toString());
			dto.setLineDesc(obj[15].toString());
			dto.setShiftName(obj[16].toString());
			dto.setLineProdName(obj[17].toString());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public int deleteProductionPlan(int factoryId, int lineId, String workingDate, int shiftId) {
		
		return productionLineDAO.delete(factoryId, lineId, workingDate, shiftId);
	}
	
	
}
