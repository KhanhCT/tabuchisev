package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.LineProdsDTO;
import com.daisyit.dto.MaterialDTO;
import com.daisyit.dto.ProcessBOMDTO;
import com.daisyit.dto.ProcessDTO;
import com.daisyit.dto.ProductBOMDTO;
import com.daisyit.dto.UnitDTO;

public interface ItemService {

	public int newMaterial(MaterialDTO dto);
	public int updateMaterial(MaterialDTO dto);
	public MaterialDTO findMaterial(String materialCode);
	public List<MaterialDTO> findAllMaterial();
	public int deleteMaterial(String materialCode);
	public List<MaterialDTO> uploadMaterial(String url);
	public List<MaterialDTO> syncMaterial();
	
	/*********Line Product ******************/
	public int newLineProduct(LineProdsDTO dto);
	public int updateLineProduct(LineProdsDTO dto);
	public LineProdsDTO findLineProduct(String productCode);
	public int deleteLineProduct(String productCode);
	public List<LineProdsDTO> findAll();
	public List<LineProdsDTO> handleProductFile(String filePath);
	/*****************************************/
	
	/************BOM**********************/
	public int newLineProductBOM(ProductBOMDTO dto);
	public int updateLineProducBOM(ProductBOMDTO dto);
	public ProductBOMDTO findLineProductBOM(String lineProdCode, String materialCode);
	public List<ProductBOMDTO> findAllLineProductBOM();
	public int deleteLineProductBOM(String lineProdCode, String materialCode);
	public List<ProductBOMDTO> uploadLineProdBOM(String url);
	
	public int newProcessProdBOM(ProcessBOMDTO dto);
	public int updateProcessProdBOM(ProcessBOMDTO dto);
	public List<ProcessBOMDTO> findProcessProdBOM(String lineProdCode,int processIdx);
	public List<ProcessBOMDTO> findAllProcessProdBOM();
	public int deleteProcessProdBOM(String lineProdCode, String materialCode, int processIdx);
	public List<ProcessBOMDTO> uploadProcessProdBOM(String url);
	public List<Object[]> exportBOM();
	/************************************/
	
	/************Unit**********************/
	public List<UnitDTO> findAllUsageUnits();
	public List<UnitDTO> findAllSkuUnits();
	public List<UnitDTO> findAllUnits();
	public int newUnit(UnitDTO dto);
	public int deleteUnit(String unitCode);
	public int updateUnit(UnitDTO dto);
	public UnitDTO findUnit(String unitCode);
	
	/************Process**********************/
	public List<ProcessDTO> findProcesses(String processCode);
	public int newProcess(ProcessDTO dto);
	public int deleteProcess(String processCode);
	public int updateProcess(ProcessDTO dto);
	public ProcessDTO findProcess(String processCode);	
}
