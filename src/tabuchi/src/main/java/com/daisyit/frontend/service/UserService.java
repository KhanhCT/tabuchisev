package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.UserDTO;
import com.daisyit.dto.UserRoleDTO;
import com.springlib.core.dto.TokenInfo;

public interface UserService {
	 List<UserDTO> findAll();
	 UserDTO findById(int id);
	 int newUser(UserDTO user);
	 boolean delete(int id) ;
	 UserDTO loadUserByUsername(String username);
	 boolean checkLogin(UserDTO user);
	 String getUserRole(int userRoldeId);
	 UserDTO getUserByNameAndPass (String userName, String passWord);
	 public TokenInfo getTokenInfo(String userName, String password);
	 List<UserRoleDTO> getAllUserRoles();
	 
}
