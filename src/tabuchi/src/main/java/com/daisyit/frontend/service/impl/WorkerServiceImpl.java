package com.daisyit.frontend.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daisyit.backend.dao.WorkerDAO;

import com.daisyit.backend.model.Worker;
import com.daisyit.dto.WorkerDTO;
import com.daisyit.frontend.service.WorkerService;
import com.google.gson.Gson;
@Service("workerService")
public class WorkerServiceImpl implements WorkerService{
	@Autowired
	private WorkerDAO workerDAO;

	@Override
	public List<WorkerDTO> findAllWorkers() {
		List<Worker> workerList = workerDAO.findAll();
		List<WorkerDTO> dtos = new ArrayList<>();
		for(Worker worker : workerList)
		{
			WorkerDTO dto = new WorkerDTO();
			dto.model2DTO(worker);
			dtos.add(dto);
		}
		return dtos;
	}
	@Override
	public int save(WorkerDTO dto) {
		Worker worker = dto.dto2Model();
		if(worker!=null) {
			worker.setDisabled(false);
			return workerDAO.insert(worker);
		}
		else
			return 0;
	}
	@Override
	public int update(WorkerDTO dto) {
		Worker worker = dto.dto2Model();
		return workerDAO.update(worker);
	}
	@Override
	public List<WorkerDTO> uploadWorkerList(String url) {
		File prodPlanFile = new File(url);
		List<WorkerDTO> dtos = new ArrayList<>();
		if(!prodPlanFile.exists())
			return null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(url));
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext()) {
				rowIterator.next();
				rowIterator.next();
				rowIterator.next();
			}else{
				return dtos;
			}
			WorkerDTO dto;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				dto = new WorkerDTO();
				Iterator<Cell> cellIterator = row.cellIterator();
				if (cellIterator.hasNext()) {
					cellIterator.next();
					Cell cell = cellIterator.next();
					String workerCode = cell.getStringCellValue();
					if (workerCode == null || workerCode.equals(""))
						continue;
					dto.setWorkerCode(workerCode);
					cell = cellIterator.next();
					String workerName = cell.getStringCellValue();
					if (workerName == null || workerName.equals(""))
						continue;
					dto.setWorkerName(workerName);
					cell = cellIterator.next();
		
					String barcode = cell.getStringCellValue();
					dto.setBarcode(barcode);
					
					dto.setDisabled(false);
					dtos.add(dto);
				}
			 }
			String json = new Gson().toJson(dtos);
			int ret=  workerDAO.insert(json);
			if(ret == 0)
				dtos = null;
		}catch (Exception e) {
			dtos = null;
		}
		return dtos;
	}
	@Override
	public int delete(int workerId) {
		return workerDAO.delete(workerId);
	}
	@Override
	public int delete(String workerCode) {
		return workerDAO.delete(workerCode);
	}
	@Override
	public WorkerDTO find(String workerCode) {
		Worker worker = workerDAO.find(workerCode);
		WorkerDTO dto = null;
		if(worker !=null) {
			dto = new WorkerDTO();
			dto.model2DTO(worker);
		}
		return dto;
	}
	
	
}
