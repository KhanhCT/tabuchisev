package com.daisyit.frontend.service;
import java.util.List;

import com.daisyit.dto.BaseDTO;

import com.daisyit.dto.FactoryDTO;
import com.daisyit.dto.LineDTO;
import com.daisyit.dto.LineProcessDTO;
import com.daisyit.dto.ProdImgDTO;
import com.daisyit.dto.ProductionLineDTO;
import com.daisyit.dto.ShiftDTO;
import com.daisyit.dto.WorkPlaceDTO;

public interface LineService {
	/*********Factory******************/
	List<FactoryDTO> findAllFactory();
	/*********Line Process ******************/
	public int newProcess(LineProcessDTO dto);
	public int updateProcess(LineProcessDTO dto);
	public LineProcessDTO findProcess(String lineProdCode, int processId);
	public int deleteProcess(String lineProdCode, int processId);	
	public List<LineProcessDTO> findAllProcess(int lineProdId, int processId);
	public List<LineProcessDTO>  uploadProcessList(String url);
	/*****************************************/
	
	/*********Line ******************/
	public int newLine(LineDTO dto);
	public int updateLine(LineDTO dto);
	public LineDTO findLine(int factoryId, int lineId);
	public int deleteLine(int factoryId, int lineId);
	public List<LineDTO> findLine(int factoryId);
	public List<LineDTO> findAllLine(int factoryId, int lineId);
	public List<LineDTO>  uploadProductionLine(String url);
	
	/*****************************************/
	/*****************Workplace*******************/
	public List<WorkPlaceDTO> findAllWorkPlaces(int factoryId, int lineId, int wPlaceId);
	public int updateWorkPlace(WorkPlaceDTO dto);
	public int deleteWorkPlace(int factoryId, int lineId, int wPlaceId);
	public int newWorkPlace(WorkPlaceDTO dto);
	public List<WorkPlaceDTO>  uploadWorkPlace(String url);
	
	/***************Statistic*********************/
	/*********Plan ******************/
	public int newProductionPlan(ProductionLineDTO dto);
	public int updateProductionPlan(ProductionLineDTO dto);
	public List<ProductionLineDTO> findProductionPlan(String workingDate);
	public List<ProductionLineDTO> uploadProductionPlan(String url);
	public int deleteProductionPlan(int factoryId, int lineId, String workingDate, int shiftId);
	
	/*****************************************/
	
	public List<BaseDTO> getMonthyLineProductivity(String curDate, int lineId, int factoryId);
	public List<BaseDTO> getFactoryData(String curDate,  int factoryId);
	
	/***************Photo Gallery ************************/
	public List<ProdImgDTO> findAllPhoto(String workingDate, int factoryId, int lineId, int shiftId, int position);
	
	public List<ShiftDTO> findAllShifts(int factoryId);

}
