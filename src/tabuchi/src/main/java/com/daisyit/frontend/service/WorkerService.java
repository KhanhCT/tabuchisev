package com.daisyit.frontend.service;

import java.util.List;

import com.daisyit.dto.WorkerDTO;

public interface WorkerService {
	List<WorkerDTO> findAllWorkers();
	int save(WorkerDTO worker);
	int update(WorkerDTO worker);
	int delete(int workerId);
	int delete(String workerCode);
	WorkerDTO find(String workerCode);
	List<WorkerDTO> uploadWorkerList(String url);
}
