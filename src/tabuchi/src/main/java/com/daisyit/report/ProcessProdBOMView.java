package com.daisyit.report;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.daisyit.dto.BOMDTO;

public class ProcessProdBOMView  extends AbstractCsvView {

	@SuppressWarnings("unchecked")
	@Override
	protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.setHeader("Content-Disposition", "attachment; filename=\"BOM.txt\"");
	    List<Object[]> boms = (List<Object[]>) model.get("bom");
	    String[] header = {"prodBarcode","processIdx","matBarcode"};
	    ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
	            CsvPreference.STANDARD_PREFERENCE);
	    csvWriter.writeHeader(header);
	    BOMDTO dto;
	    for(Object[] bom : boms){
	        dto = new BOMDTO();
	        dto.setProdBarcode(bom[0].toString());
	        dto.setMatBarcode(bom[2].toString());
	        dto.setProcessIdx((int)bom[1]);
	        csvWriter.write(dto,  header);
	    }
	    csvWriter.close();
		
	}
	
	
}