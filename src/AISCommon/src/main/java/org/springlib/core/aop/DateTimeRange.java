package org.springlib.core.aop;

import java.util.Date;

public class DateTimeRange{
	private Date begTime;
	private Date endTime;
	public Date getBegTime() {
		return begTime;
	}
	public void setBegTime(Date begTime) {
		this.begTime = begTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
