package org.springlib.core.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

@Aspect
public class AopMonitor {
	private static final Logger LOGGER = LoggerFactory.getLogger(AopMonitor.class);

	public Object profile(ProceedingJoinPoint pjp) throws Throwable {
		// log input parameters
		StringBuilder argSb = new StringBuilder();
		Object[] signatureArgs = pjp.getArgs();
		Object signatureArg = null;

		for (int i = 0; i < signatureArgs.length; i++) {
			signatureArg = signatureArgs[i];
			argSb.append("param ").append(i + 1).append(" = ").append(signatureArg).append("; ");
		}

		int length = argSb.length() > 2 ? argSb.length() - 2 : 0;
		LOGGER.info(pjp.getSignature().getName() + "[" + argSb.substring(0, length) + "]");

		// log elapsed time
		long start = System.currentTimeMillis();
		Object output = pjp.proceed();
		long elapsedTime = System.currentTimeMillis() - start;

		MDC.put("package", pjp.getSignature().getDeclaringTypeName());
		MDC.put("method", pjp.getSignature().getName());
		MDC.put("duration", String.valueOf(elapsedTime));

		LOGGER.info("Method [" + pjp.getSignature().getDeclaringTypeName() + "." + pjp.getSignature().getName()
				+ "] execution time: " + elapsedTime + " milliseconds.");
		return output;
	}
}
