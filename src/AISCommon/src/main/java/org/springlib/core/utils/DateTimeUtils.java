package org.springlib.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springlib.core.aop.DateTimeRange;

public class DateTimeUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtils.class);
	private static DateTimeUtils instance;
	private  DateTimeUtils()
	{
		
	}
	public static DateTimeUtils getInstance() {
		if(instance == null)
			instance = new DateTimeUtils();
		return instance;
	}
	public static Date convertDateByFormatLocal(String date, String format) {
		if (date == null || date.isEmpty() || format == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			LOGGER.error("Content: "+ date + "Exception: " + e.getMessage());
			return null;
		}
	}

	public static String convertDateStringByFormatLocal(Date date, String format) {
		if (date == null || format == null) {
			return null;
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date).toString();
		}catch(Exception ex){
			LOGGER.error("Content: "+ date + "Exception: " + ex.getMessage());
			return null;
		}
	}

	public static Date getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
	private static Calendar getCalendar(Date date) {
	    Calendar calendar = GregorianCalendar.getInstance();
	    calendar.setTime(date);
	    return calendar;
	}
	public static DateTimeRange getMonthRange(Date date) {
		DateTimeRange month = new DateTimeRange();
		Calendar calendar = getCalendar(date);
		calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		//setTimeToBeginningOfDay(calendar);
		month.setBegTime(calendar.getTime());
		calendar = getCalendar(date);
		calendar.set(Calendar.DAY_OF_MONTH,
	            calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//setTimeToEndofDay(calendar);
		month.setEndTime(calendar.getTime());
		return month;
	}
	public static String[] getStringMonthRange(Date date) {
		DateTimeRange month = getMonthRange(date);
		String[] range = new String[2];
		String begin = DateTimeUtils.convertDateStringByFormatLocal(month.getBegTime(), CommonKey.DATE_FORMAT.DATE_TIME_1);
		range[0] = begin;
		String end = DateTimeUtils.convertDateStringByFormatLocal(month.getEndTime(), CommonKey.DATE_FORMAT.DATE_TIME_1);
		range[1] = end;
		return range;
	}
	private static void setTimeToBeginningOfDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	}
}
