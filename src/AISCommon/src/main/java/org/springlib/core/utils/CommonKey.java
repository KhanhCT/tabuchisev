package org.springlib.core.utils;

public interface CommonKey {
	public static final String USER_ID="UserId";
	public static final String USER_ROLE_ID="UserRoleId";
	public static final String USER_ROLE_VALUE="UserRoleValue";
	public interface ROLE {
		public static final String ROLE_ADMIN = "Admin";
		public static final String ROLE_USER = "User";
	}

	public interface DATE_FORMAT {
		public static final String DATE_DATA_FORMAT = "dd/MM/yyyy HH:mm:ss";
		public static final String DATE_SLASH_FORMAT = "dd/MM/yyyy";
		public static final String DATE_OTHER_FORMAT = "MMddyyyy";
		public static final String DATE_TIME_FORMAT = "ddMMyyyy";
		public static final String DATE_TIME_1 = "yyyy-MM-dd";
		public static final String TIME_FORMAT_1 = "HH:mm:ss";
		public static final String TIME_FORMAT_2 = "HH:mm";
	}

	public interface CURRENCY {
		public static final String VND = "VND";
		public static final String USD = "USD";
	}

	public interface RESPONSE_CODE {
		public static final int ERROR =0;
		public static final int SUCCESS=1;
		public static final int EXIST =2;
	}
	public enum ResponseCode{
		ERROR(0),
		SUCCESS(1),
		EXITS(2);
		public final int Value;
		private ResponseCode(int value)
	    {
	        Value = value;
	    }
	}
	public interface TRANSACTION_TYPE {
		public static final String CASH_IN = "00";
		public static final String CASH_OUT = "01";
		public static final String CASH_TRANSFER = "02";
		public static final String CASH_WITHDRAW = "03";
	}

	public interface REPORT_KEY {
		public static final String TITLE = "title";
		public static final String FROM_DATE = "fromDate";
		public static final String TO_DATE = "toDate";
		public static final String NAME = "reportName";
		public static final String REPORT_DATE = "reportDate";
		
		public static final String TRANSACTION_LIST = "TRANSACTION LIST";
		public static final String CARD_BALANCE = "CARD BALANCE";
		public static final String CARD_TRANSACTION_HISTORY = "CARD TRANSACTION HISTORY";

		public interface TYPE {
			public static final String PDF = "pdf";
			public static final String CSV = "csv";
			public static final String EXCEL = "xls";
		}
		
		public interface CARD_TRAN_HISTORY {
			public static final String DATE_TIME = "Ngày\nDate-Time";
			public static final String TRANS_NUM = "GD số\nNumber";
			public static final String TYPE = "Loại\nType";
			public static final String TRANS_TYPE = "Loại\nTransaction type";
			public static final String CARD_ID = "Thẻ số/Card ID: ";
		}
	}

	public interface REQUEST_METHOD {
		public static final String GET = "GET";
		public static final String POST = "POST";
		public static final String PUT = "PUT";
	}
	
	public interface ALL_CODE {
		public static final String PRICE_UNIT = "PRICE_UNIT";
	}
}
