package org.springlib.core.http;

import java.util.Collections;
import java.util.HashMap;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import com.springlib.core.dto.TokenInfo;

public class RestClient {
	@SuppressWarnings("rawtypes")
	// HttpEntity Map
	private HashMap<Class, ParameterizedTypeReference> typesManaged;
	private RestTemplate restTemplate;
	private HttpHeaders headers;
	private String apiUrl;
	private String accessToken;

	
	public RestClient(RestTemplate restTemplate, String apiUrl, String accessToken) {
		this.restTemplate = restTemplate;
		this.restTemplate.setErrorHandler(new RestClientErrorHandler());
		this.apiUrl = apiUrl;
		this.accessToken = accessToken;

		// Set default headers and types to handle
		setHeaders();
		populateManagedTypes();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> deleteRequestWithArgs(Class<T> valueType, String resourceUrl,
			@SuppressWarnings("rawtypes") HashMap args) {
		HttpEntity<String> httpEntity = new HttpEntity<>("parameters", headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.DELETE, httpEntity,
					typesManaged.get(valueType), args);
		} catch (RestClientException e) {
			// return new ResponseEntity<T>(HttpStatus.EXPECTATION_FAILED);
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	public <T> void setTypeReference(Class<T> cls, @SuppressWarnings("rawtypes") ParameterizedTypeReference e)
	{
		this.typesManaged.put(cls, e);
	}
	/**
	 * 
	 * @param valueType: Entity object
	 * @param resourceUrl: specific Url
	 * @param parts: 
	 * @return
	 * For instance:
	 * MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
	 *	parts.add("name", hostname);
	 *	return restClient.postRequest(Object.class, "/rest/?op=allocate", parts).getBody();
	 */
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> postRequest(Class<T> valueType, String resourceUrl, MultiValueMap<String, String> parts) {
		//headers.setContentType(MediaType.APPLICATION_JSON);
		//headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(parts, headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.POST, httpEntity,
					typesManaged.get(valueType));

		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> postRequest(Class<T> valueType, String resourceUrl,String jsonBody)
	{
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.POST, new HttpEntity<>(jsonBody, headers),
					typesManaged.get(valueType));
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	/**
	 * 
	 * @param valueType
	 * @param resourceUrl
	 * @param parts
	 * @param args
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> postRequestWithArgs(Class<T> valueType, String resourceUrl,
			MultiValueMap<String, Object> parts, @SuppressWarnings("rawtypes") HashMap args) {
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(parts,
				headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.POST, httpEntity, typesManaged.get(valueType),
					args);
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	/**
	 * 
	 * @param valueType
	 * @param resourceUrl
	 * @param parts
	 * @return
	 * MultiValueMap<String, String> parts = new LinkedMultiValueMap<>();
		for (String id : systemIds) {
			parts.add("machines", id);
		}
		// ResponseEntity<?> response = restClient.postRequest(String[].class,
		// "/rest/?op=release", parts);
		ResponseEntity<String[]> response = restClient.postRequestFormURL(String[].class, "/rest/?op=release",
				parts);
	 */
	
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> postRequestFormURL(Class<T> valueType, String resourceUrl,
			MultiValueMap<String, String> parts) {
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<MultiValueMap<String, String>>(parts,
				headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.POST, httpEntity,
					typesManaged.get(valueType));
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> putRequest(Class<T> valueType, String resourceUrl,
			MultiValueMap<String, Object> parts) {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.PUT, httpEntity, typesManaged.get(valueType));
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> putRequest(Class<T> valueType, String resourceUrl, String jsonBody)
	{
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.PUT, new HttpEntity<>(jsonBody, headers), valueType);
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * 
	 * @param valueType
	 * @param resourceUrl
	 * @param parts
	 * @param args
	 * @return
	 * For exmaple
	 * HashMap<String, String> args = new HashMap<>();
		args.put("system_id", systemId);
		args.put("id", id);
		return restClient.putRequestWithArgs(String.class, "/nodes/{system_id}/interfaces/{id}/", builder.buildAsArgs(), args)
	 */
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> putRequestWithArgs(Class<T> valueType, String resourceUrl,
			MultiValueMap<String, Object> parts, @SuppressWarnings("rawtypes") HashMap args) {
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(parts,
				headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.PUT, httpEntity, typesManaged.get(valueType),
					args);
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	/**
	 * 
	 * @param valueType
	 * @param resourceUrl
	 * @return
	 * ResponseEntity<String[]> response = restClient.getRequest(String[].class, "/rest/?op=list_allocated");
	 */
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> getRequest(Class<T> valueType, String resourceUrl) {
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization","Bearer " + this.accessToken);
		HttpEntity<String> httpEntity = new HttpEntity<>("parameters", headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.GET, httpEntity, typesManaged.get(valueType));
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	/**
	 * 
	 * @param valueType
	 * @param resourceUrl
	 * @param args
	 * @return
	 * #For Example:
	 * HashMap<String, String> args = new HashMap<>();
     * args.put("name", name);
	 * return restClient.getRequestWithArgs(ByteArrayResource.class, "/rest/{name}", args).getBody();
	 */
	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<T> getRequestWithArgs(Class<T> valueType, String resourceUrl,
			@SuppressWarnings("rawtypes") HashMap args) {
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization","Bearer " + this.accessToken);
		HttpEntity<String> httpEntity = new HttpEntity<>("parameters", headers);
		try {
			return restTemplate.exchange(apiUrl + resourceUrl, HttpMethod.GET, httpEntity, typesManaged.get(valueType),
					args);
		} catch (RestClientException e) {
			return new ResponseEntity<T>((T) e.getMostSpecificCause().getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	/**
	 * Set Header for RestAPI
	 */
	private void setHeaders() {
		headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		//headers.setContentType(MediaType.APPLICATION_JSON);
	}

	/**
	 * Add map for HttpEntity
	 */
	private void populateManagedTypes() {
		typesManaged = new HashMap<>();
		
		typesManaged.put(String[].class, new ParameterizedTypeReference<String[]>() {
		});
		
		typesManaged.put(String.class, new ParameterizedTypeReference<String>() {
		});
		
		typesManaged.put(TokenInfo.class, new ParameterizedTypeReference<TokenInfo>() {
		});
	}

}
